---
title: "Automedias : Le Médiactivisme Disrupté ?"

locale: fr

#
# Programme
#

programme:

  # Jour 1
  - date: 2024-01-30
    title: Automedias et Affects Politiques. Selon quelle stratégie politique le modèle automédiatique peut-il se distinguer du modèle médiactiviste?
    schedule:
      - moderator: Vincent Puig       
        guests: 
        - Igor Galligo
        - Viviana Lipuma   
        start_time: 17:00
        end_time: 19:30
        intro: Dans leur ouvrage Médiactivistes , les sociologues F. Granjon et D. Cardon rappellent que la tradition médiactiviste, née de l'élan du laboratoire social qu'ont été les années 1970, s’est érigée contre l’hégémonie médiatique télévisuelle pour donner à voir et à entendre une partie invisibilisée de la réalité et des vécus. Le médiactivisme s’est construit en opposition à une organisation médiatique perçue comme étant l’expression et le rouage d’une domination politique. A l’aube des années 2000, cette organisation médiatique et cette opposition se renouvellent et se mondialisent avec l’arrivée et le développement des médias numériques. L’hégémonie médiatique reformulée par la Silicon Valley se développe alors sous la forme de réseaux sociaux et de plateformes de communication globalisées qui permettent à tout un chacun.e de produire et de communiquer une information. En réponse, les héritiers de la tradition médiactiviste investissent le champ des médias numériques avec le désir de se réapproprier les supports d’information et de communication des Big Tech, afin de délivrer des informations et des analyses alternatives à celles diffusées par les médias dominants. Il n’en demeure pas moins que, dans ce paysage médiatique renouvelé, les postures adoptées par les médiactivistes sont parfois discutables. Les contributions en ligne, sous forme de texte ou de vidéo, se présentent comme le fait d’individus ou de groupes d’activistes exceptionnels qui visent à «  retourner le système contre lui-même », avec l’espoir d’être suivis par le plus grand nombre. Or, c’est là conserver non seulement les infrastructures technologiques du capitalisme numérique, mais également une structure de l’action politique fondée sur la séparation entre meneurs et suiveurs, que les réseaux sociaux reproduisent à travers l’abonnement numérique, d’un côté et les abonnés, de l’autre. Dans cette séance introductive, nous souhaitons analyser les mécanismes qui conduisent à cette posture et pour cela interroger le médiactivisme au prisme de la configuration bipartite de ses « affects politiques ». Dans Vivre Sans ?  le philosophe Frédéric Lordon se livre à une critique démocratique de « la politique du/de la virtuose » accusée de sacrifier la puissance politique de la multitude devant la fascination pour l’héroïsme de quelques dissident.e.s, dont Lordon qualifie l’éloge d'aristocratisme. A la suite de sa critique, nous voudrions repenser l’action de la multitude à travers un modèle d’action capable non seulement de dépasser l’opposition fonctionnelle entre producteur et consommateur d’information, mais aussi l’opposition affective, qui permet à cette première de perdurer. Il s'agira, ainsi, de dégager quelques linéaments susceptibles de résorber le hiatus entre avant-garde et peuple/masse qui a structuré les échecs de toute politique révolutionnaire au 20ème siècle.

    # Jour 2
  - date: 2024-04-12
    title: AI and Automation of Mediactivm
    schedule:
      - moderator: Igor Galligo         
        guests:
          - Geert Lovink
          - David Chavalarias
          - Hadrien Pelissier      
        start_time: 17:00
        end_time: 19:30
        language: Session en anglais
        intro: In contemporary activism there is a growing realization that there is an urgent need to distinguish between tools for (save, decentralised) organization and (broad) mobilization that scale up. Social media platforms have mixed the two up (news & personal messages), up to the point of becoming unworkable, exhausting, unleashing mental health crises, on top of creating dangerous situations in terms surveillance. The social media hegemony is not yet broken but there are many cracks in its architecture—and appeal. While the Internet Question seems stagnant and unresolved, the hype caravan has moved on. From VR, crypto and Web3 where now all in the grip of ‘AI' as the latest marketing term for machine learning and large language models that feeds its generic ’summary’ content back into the Web. While the ‘infuencer’ persona is in decline, the mass obsession with likes, views, comments, swiping from one TikTok video to the next, retweets funny memes continues. What will disrupt the 'bored billions’ is the automation of mediactivism (already visible in the current waves of fake news and deep fakes). While officials raise the ethics issue of AI for Good, asking the impossible question how to buid ‘responsible’ extraction software, hackers, designers and other artists are coding subversive tools that disrupt and undermine the totalitarian system of control, ‘polluting’ data bases, turning fake news and monstrous images upside down. The message is clear:the world prepares for planetary cyberwarfare. Are you ready?

    # Jour 3
  - date: 2024-04-26
    title: "Autonomie esthétique des usager.è.s des plateformes numériques: processus de subjectivation et création d’imaginaires collectifs"
    schedule:
      - moderator: Viviana Lipuma           
        guests:
          - Laurence Allard
          - Anaïs Nony       
        start_time: 17:30
        end_time: 19:30
        intro: Les défis sociaux contemporains nous invitent à élaborer un concept plus exigeant de ce qu’on entend par “domaine public”, et donc à aller au-delà de sa définition purement négative d'un domaine qui n'est pas (encore) dans les mains des grands acteurs du capitalisme. L'exigence démocratique de protéger et d’étendre ce qui nous appartient collectivement doit porter sur notre santé, nos manières d'habiter la ville, nos manières de communiquer, mais aussi sur nos imaginaires et nos mises en récit. Or, à l’encontre de l'espoir d'un “espace public parallèle” (Mattelart) en opposition au dispositif télévisuel, à ses clichés et à son fonctionnement vertical aux débuts de l’internet, depuis les années 2000 la tendance est au contraire à l'infotainement, à la consommation passive de signes et à la production par tout un.e chacun.e de contenus convenus en des formats convenus.  La page de l’aventure collective du médiactivisme semble définitivement tournée:en même temps qu'une dépossession des moyens de production et une perte de savoirs techniques, on assiste à l'affaiblissement des capacités collectives à façonner des visions du monde. Le concept de “sémiocapitalisme” de Franco Berardi décrit cette main-basse sur les signes de la part des instances techno-économiques du capitalisme numérique qui en contrôlent la diffusion algorithmique, mais aussi la production à travers les icônes du web (youtubeurs et influenceurs) soumis à leurs contraintes de valorisation financière (audience, likes). Dans ce nouveau contexte infrastructurel, comment peut-on activer et cultiver les capacités esthétiques de l'intelligence collective, qui a montré savoir s'autodéterminer politiquement et utiliser tactiquement des outils du web pour mener à bien ses luttes? Cette question nous invite à envisager une fabrique autonome des imaginaires, susceptible de renouer avec les pratiques de subjectivation sémio-technique du médiactivisme, mais sans les figures tutélaires d’un savoir et d’une compétence esthétiques qui les encadraient. Nous estimons que cette fabrique autonome des imaginaires nous permettrait de nous orienter vers des choix d'avenir et de complexifier la compréhension qu'on a du social. L'objectif de cette séance sera de détailler les conditions matérielles et mentales d'une autonomie esthétique des usager.è.s des NTIC, en se montrant attentifs à son impossible assimilation à un “imaginaire révolutionnaire” propre au vidéoactivisme.

    # Jour 4
  - date: 2024-05-13
    title: Digitalisation du journalisme, la disruption du journalisme professionnel
    schedule:
      - moderator: Igor Galligo       
        guests:
          - Aurélie Aubert
          - Jérôme Valluy     
        start_time: 18:00
        end_time: 20:30 (séance en salle Claude Pompidou. 5ème étage)
        intro: Le néologisme « collution », inventé par Franck Rebillard et Nikos Smyrnaios décrit de manière convaincante un des effets du processus de disruption du journalisme professionnel, à la fois premier partenaire et première victime du tournant numérique. La collution désigne à la fois une collusion socioéconomique et une dilution éditoriale mêlées. Du côté de la collision, le « capitalisme de surveillance » (Shoshana Zuboff, 2018) s’est développé sur la spoliation financière du journalisme, son discrédit par un « journalisme citoyen » qui tend vers l'automédiation, le renforcement de sa surveillance policière (Pegasus, Predator…) et sa dilution dans le « web synthétique » (O.Ertzscheid). Du côté de la collusion, les journalistes sont les premiers à rediffuser leurs articles, à utiliser les sources et ressources numériques, à ignorer les atteintes à la vie privée et à partager des revenus de publicités individualisées plutôt que de les combattre et défendre des conditions autonomes et démocratiques de leur travail. Comment alors expliquer que la profession s’empresse dans une collaboration autodestructrice ? Tandis que le médiactivisme revendiquait une rupture claire et nette avec le journalisme, il semblerait au contraire que la distinction entre automédiation et journalisme présente aujourd'hui une porosité produite par la pression des nouvelles plateformes de communication du capitalisme numérique qui affecte la profession journalistique ainsi que ses productions.  Pour comprendre ce phénomène, nous suivrons l’hypothèse - aujourd'hui consolidée par de nombreuses recherches - d’une nouvelle dépendance journalistique, considérée comme un « fait social » au sens de Durkheim dans « Le suicide » (1897).

    # Jour 5
  - date: 2024-05-27
    title: "Automedias: Repenser le design des plateformes"
    schedule:
      - moderator: Ludovic Duhem          
        guests:
          - Michael Bourgatte
          - Samuel Huron
          - Tallulah Frappier       
        start_time: 18:00
        end_time: 20:30  (séance en salle Claude Pompidou. 5ème étage)
        intro: L’idée d’automédiation suppose une émancipation de la logique médiatique dominante et de son discours officiel. Mais si le contenu est revendiqué comme une alternative nécessaire, légitime et crédible au nom de la vérité, de la liberté et de la démocratie, les plateformes de diffusion de cet activisme alter-médiatique ou contre-médiatique interroge rarement les conditions de sa conception et de sa diffusion. Cette séance cherchera à mettre en évidence d’une part les questionnements incontournables sur le design des plateformes numériques de création de contenu produites par le capitalisme des GAFAM et les BATX et sur les alternatives open source ainsi que les possibilités et intérêts d’un co-design des plateformes par les utilisateurs. Elle mettra également en question la tradition médiactiviste et sa fabrique de l'information (oppositionnelle et révolutionnaire) pour définir les conditions d'une bifurcation pharmaco-politique où l'enjeu est moins l'appropriation et le détournement, la résistance et la confrontation que l'invention du collectif démocratique par la pratique automédiatique.

    # Jour 6
  - date: 2024-06-20
    title: L’Automédia au prisme des minorités de genre
    schedule:
      - moderator: Viviana Lipuma       
        guests:
          - Mona Gérardin-Laverge
          - Hélène Fleckinger
          - Claire Blandin         
        start_time: 17:00
        end_time: 19:30 (salle Atelier 1 de la Bibliothèque Publique d'Information, Accès Centre Pompidou)
        intro: L'invisibilisation des minorités politiques est une constante des médias dominants:réduites à des objets dont on parle, à qui on ne donne la parole et l'image que pour confirmer des stéréotypes de genre, de race et de classe qui circulent à leur propos, cette relégation en coulisses coïncide dans les faits avec leur exclusion de l'arène citoyenne. Le féminisme a historiquement essayé d'évincer cet état de fait médiatique en faisant des technologies d'information disponibles les alliés de leurs combats. Après le tract et la presse écrite au tournant du 20ème siècle, le magazine et la vidéo expérimentale dans les années 1970, on assiste depuis les années 1990 à une « troisième vague » qui investit activement les nouveaux médias d'information et de communication. Le cyberféminisme, terme qui désigne un ensemble de pratiques hétérogènes menées par les militantes féministes sur internet, inaugure un nouvel « espace oppositionnel » (Mattelart). Mais la création de cet espace n’a pas seulement servi à rendre audibles les revendications sur la parité de genre, la fin des violences sexistes et sexuelles, la prise en compte du travail domestique gratuit, la remise en question des normes esthétiques et sociales. De nombreux.ses sociologues insistent, en effet, sur ce que l'architecture du web fait au mouvement:les blogs et les vlogs font éclore un expressivisme digital où les agencements textuels, visuels et sonores des profils donnent lieu à « une identité par bricolage » (Allard) ; le web apparaît en outre comme un espace inédit de convergence des luttes - à l’instar du mouvement metoo; enfin, le journalisme citoyen et l'édition participative rendent possible un élargissement sans précédents d'une « base » gagnée à la cause. Mais quel est le prix de cette révolution médiatique? On sait que le fonctionnement de l’espace numérique favorise une prise de parole et une prise individuelle, focalisée sur l’expérience d’oppression vécue en première personne et à une hyper-exposition de la manière dont on la vainc individuellement. Ces opérations sémiotiques se réalisent au détriment d’une saisie structurelle des dominations de genre et de la relation avec d’autres dominations. Il en résulte souvent, bien que non systématiquement, une simplification des débats et des engagements politiques. En interrogeant le devenir historique des pratiques médiatiques des minorités de genre (féministe, transféministe, queer et homosexuelles) à l’âge des plateformes numériques, cette séance a ainsi comme objectif de dresser un diagnostic politique des réussites et des échecs liés à ce changement de paradigme technologique.

    # Jour 7
  - date: 2024-07-10
    title: "Generative AI: a new kind of Automedia ?"
    schedule:
      - moderator: Nina Begus (en anglais)  
        guests:
          - Gasper Begus
          - Agnieszka Kurant       
        start_time: 17:00
        end_time: 19:30 (séance en salle PS du Centre Pompidou. Accès - 1)
        intro: Every technology is molded by our cultural understanding of it, including its fictional representations. The very creation of AI systems and our interactions with them are influenced by how we conceptualize AI. How do fictional representations limit our view, and in what ways are they beneficial? The talk will focus on AI and speech. While AI is imitating human speech, it is also producing its artificial speech in a unique manner. Generative AI presents a yet unprecedented, new kind of automedia:self-productive, automatic, and autonomous but also tied to human design and prompts. No AI system is isolated from its means of production, including broader social and cultural aspects.  We will demonstrate how the cultural implications of language learning have shaped AI products as we recognize them today. Nina Begus will introduce a fresh approach to designing AI systems that veers away from the traditional anthropomorphic trajectory. Avoiding external projections can only succeed with enhanced interpretability of automatic and highly generative AI. Engaging in a dialogue with this talk, we are honored to invite UC Berkeley professor Gašper Beguš, an expert on GANs and speech, and Agnieszka Kurant, a conceptual artist investigating collective and nonhuman intelligences and the exploitations present in digital capitalism.


#
# Lien Inscription
#
#
inscription: 
  method: inscription
  howto: "
  L'évènement est ouvert à toutes et tous sur place ou en ligne. Inscription demandée:

    [Inscription](https://docs.google.com/forms/d/e/1FAIpQLScoC1G4qs1mBFISVIJ_qB8_-ajIl_5WpSl00xRyVvyXcFTnHg/viewform){.link .xxl}
  "

#
# Info Pratique
#
practical_info: 

  - description: "L’événement se tiendra dans la salle Triangle du Centre Pompidou, de 17h à 19h30, les 30 janvier et 12 avril. En salle Claude Pompidou le 26 avril (17h30 à 19h30), le 13 mai et le 27 mai, situé au 5ème étage du Centre Pompidou. En salle Atelier 1 de la BPI le 20 juin. Et en salle PS du Centre Pompidou (accès - 1) le 10 juillet.

      <br>
      [Salle Triangle](https://www.iri.centrepompidou.fr/pied/contact/){.link .l} 

      *Place George Pompidou, 
      
      75004 Paris*   
      <br><br>
      Métro :   
      Chatelet, Ligne 1 ou Rambuteau, ligne 11"


---

# Presentation

« Que ce soit le geste auto-médiatique produit à la volée par un seul individu avec un smartphone, ou l’entreprise collective automédiatique déjà aperçue dans le champ des luttes démocratiques, qui réinvente les formes des médias tactiques et des médiactivistes, l’automédiation désigne l’autoproduction et l’autodiffusion de l’information à caractère politique par l’usage ou la réinvention des appareils et circuits de communication numériques. »  Telle est la définition donnée de l’Automedia en introduction du dossier de recherche collectif publié par l’organisation du même nom dans le numéro 6 des Cahiers Costech.  

Pourtant si l’automedia et l’automediation semblent hériter de l’avant-garde des médias tactiques ou plus largement de la tradition médiactiviste, leurs réinventions à l’époque des technologies, des plateformes numériques et de l’Intelligence Artificielle apparaît – encore aujourd’hui – davantage comme un enjeu à conquérir qu’une réalité socio-technique établie. Si une transformation médiatique s’opère bien sous l’effet des conditionnements provoqués par la numérisation des supports de communication, les enjeux politiques visés par la tradition médiactiviste ne sont pas assurés d’être renouvelés. 

Au contraire, le contexte infrastructurel dans lequel ces nouvelles pratiques s’élaborent semble davantage témoigner des mutations culturelles du capitalisme de l’information que d’un renouvellement de la posture contre-hégémonique  propre au médiactivisme qui nécessiterait – entre autres – une souveraineté technologique et une nouvelle organisation économique médiatique. Exceptions faites de quelques programmes numériques tels que Discord, Mastodon, Telegram, Mobilizon l’écrasante majorité de la communication sur Internet se fait aujourd’hui sur les grandes plateformes du capitalisme numérique, qu’il soit américain (Facebook, X, YouTube, Instagram, Twitch, Linkedln, etc.) ou chinois (TikTok), quelle que soit la sensibilité ou l’intention politique des émetteurs.

Plus précisément, le tournant numérique du médiactivisme semble vider le mouvement de sa radicalité comme pour tenter d’en désamorcer l’ambition politique, tout en capitalisant sur les volontés et les expressions numériques de ses participants. Si les technologiques numériques et celles de l’Intelligence Artificielle sont pourtant susceptibles d’apporter des puissances politiques nouvelles au médiactivisme , nous faisons ici l’hypothèse d’une corruption de ses finalités politiques par le contexte techno-économique auquel celles-ci sont désormais soumises. Un processus que Bernard Stiegler nomme : disruption.

Dans son ouvrage États de choc, Bêtise et savoir au 21ème siècle , Stiegler développe les analyses de Naomi Klein publiées dans La Stratégie du choc : la montée d’un capitalisme du désastre  pour conceptualiser une « stratégie du choc technologique où ce sont les conditions d’autonomie et d’hétéronomie des institutions académiques au sens large […] qui se trouvent radicalement modifiées  » par un contexte techno-économique propre à engendrer des innovations destructrices. Une analyse que Stiegler redéveloppera quelques années plus tard dans son ouvrage Dans la Disruption, Comment ne pas devenir fou ?  pour en généraliser le procédé, au-delà de l’assaut porté contre les institutions étatiques par un techno-capitalisme libertarien. 

Cependant, l’intérêt majeur de l’analyse stieglerienne consiste dans l’apport de concepts visant à déconstruire la posture d’opposition politique afin de renouveler les formes du combat (devenant techno-politique) et faire émerger des réponses propres à son devenir technologique. Ainsi, à la suite de Jacques Derrida , Stiegler rappelle la nature pharmacologique de la technique (toute technique est à la fois poison et remède) et prône la bifurcation plutôt que la révolution pour proposer une stratégie de combat contre la disruption. Si « la question (pharmaco-politique) est toujours de renverser le poison en remède », alors l’enjeu politique n’est plus tant de s’opposer à ce devenir pour revenir à la tradition médiactiviste que de faire bifurquer ce devenir vers des formes politico-médiatiques numériques qui peuvent résoudre certaines apories de la tradition médiactiviste.

Ainsi, nous proposons de nommer automedia le concept médiatique qui a résulté d’une disruption du médiactivisme par le capitalisme numérique pour redéfinir le/la médiactiviste en YouTuber ou en Tiktoker . L’enjeu de ce séminaire est double. Il consistera d’abord à approfondir l’hypothèse de cette disruption constituant l’automedia en innovation destructrice du mouvement médiactiviste. Il consistera ensuite à s’interroger sur les devenirs et les formes potentielles d’une pharmaco-politique de l’automedia pour penser et former son devenir positif.

Plusieurs évolutions nous permettent de caractériser cette disruption et de distinguer le modèle automediatique en train d’émerger du modèle médiactiviste dont il est issu. Nous remarquons deux évolutions majeures par rapport au médiactivisme : auto-media désigne des principes d’auto-production et des principes d’auto-matisation technologiques au sein d’entreprises de productions médiatiques. Automedias synthétise ainsi une déclinaison sémantique à partir du préfixe « auto- » pour produire au moins deux significations.

Dans l’histoire technique du médiactivisme, les dynamiques d’auto-production étaient réservées à quelques initié.e.s issu.e.s du monde de la production audiovisuelle et/ou cinématographique, qui se réunissaient en groupe pour produire une fabrique collective. A l’inverse, la fabrique automédiatique veut pouvoir être pratiquée individuellement et être incarnée par tout un chacun.e sur des appareils de communication et plateformes numériques, sans requérir des compétences techniques (audiovisuelles) ou technologiques spécialisées. D’autre part, les processus d’auto-matisation technologique se trouvent aujourd’hui renouvelés par les technologies numériques et les technologies de l’Intelligence Artificielle et font entrer la circulation de l’information dans une nouvelle ère de production médiatique qui n’existait pas durant les années soixante-dix, à la naissance du médiactivisme. 

Les séances mensuelles de ce séminaire qui s’étaleront jusqu’à l’été 2024 proposeront chaque fois d’étudier des points saillants de cette disruption. Elles tenteront également d’en proposer des bifurcations afin de constituer le concept d’automedia et la pratique de l’automediation comme une transformation positive du médiactivisme à l’époque numérique et de l’Intelligence Artificielle.

