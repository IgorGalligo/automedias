---
title: "Automedias : Mediactivism Disrupted ?"

locale: en

#
# Programme
#

programme:

  # Jour 1
  - date: 2024-01-30
    title: Automedia and Political Affects. According to what political strategy can the automedia model be distinguished from the media-activist model?
    schedule:
      - moderator: Vincent Puig       
        guests: 
        - Igor Galligo
        - Viviana Lipuma   
        start_time: 17:00
        end_time: 19:30
        intro: "Dans leur ouvrage Médiactivistes , les sociologues F. Granjon et D. Cardon rappellent que la tradition médiactiviste, née de l'élan du laboratoire social qu'ont été les années 1970, s’est érigée contre l’hégémonie médiatique télévisuelle pour donner à voir et à entendre une partie invisibilisée de la réalité et des vécus. Le médiactivisme s’est construit en opposition à une organisation médiatique perçue comme étant l’expression et le rouage d’une domination politique. A l’aube des années 2000, cette organisation médiatique et cette opposition se renouvellent et se mondialisent avec l’arrivée et le développement des médias numériques. L’hégémonie médiatique reformulée par la Silicon Valley se développe alors sous la forme de réseaux sociaux et de plateformes de communication globalisées qui permettent à tout un chacun.e de produire et de communiquer une information. En réponse, les héritiers de la tradition médiactiviste investissent le champ des médias numériques avec le désir de se réapproprier les supports d’information et de communication des Big Tech, afin de délivrer des informations et des analyses alternatives à celles diffusées par les médias dominants. Il n’en demeure pas moins que, dans ce paysage médiatique renouvelé, les postures adoptées par les médiactivistes sont parfois discutables : les contributions en ligne, sous forme de texte ou de vidéo, se présentent comme le fait d’individus ou de groupes d’activistes exceptionnels qui visent à «  retourner le système contre lui-même », avec l’espoir d’être suivis par le plus grand nombre. Or, c’est là conserver non seulement les infrastructures technologiques du capitalisme numérique, mais également une structure de l’action politique fondée sur la séparation entre meneurs et suiveurs, que les réseaux sociaux reproduisent à travers l’abonnement numérique, d’un côté et les abonnés, de l’autre. Dans cette séance introductive, nous souhaitons analyser les mécanismes qui conduisent à cette posture et pour cela interroger le médiactivisme au prisme de la configuration bipartite de ses « affects politiques ». Dans Vivre Sans ?  le philosophe Frédéric Lordon se livre à une critique démocratique de « la politique du/de la virtuose » accusée de sacrifier la puissance politique de la multitude devant la fascination pour l’héroïsme de quelques dissident.e.s, dont Lordon qualifie l’éloge d'aristocratisme. A la suite de sa critique, nous voudrions repenser l’action de la multitude à travers un modèle d’action capable non seulement de dépasser l’opposition fonctionnelle entre producteur et consommateur d’information, mais aussi l’opposition affective, qui permet à cette première de perdurer. Il s'agira, ainsi, de dégager quelques linéaments susceptibles de résorber le hiatus entre avant-garde et peuple/masse qui a structuré les échecs de toute politique révolutionnaire au 20ème siècle."



    # Jour 2
  - date: 2024-04-12
    title: AI and Automation of Mediactivm
    schedule:
      - moderator: Igor Galligo (in english)         
        guests:
          - Geert Lovink
          - David Chavalarias
          - Hadrien Pelissier      
        start_time: 17:00
        end_time: 19:30
        intro: In contemporary activism there is a growing realization that there is an urgent need to distinguish between tools for (save, decentralised) organization and (broad) mobilization that scale up. Social media platforms have mixed the two up (news & personal messages), up to the point of becoming unworkable, exhausting, unleashing mental health crises, on top of creating dangerous situations in terms surveillance. The social media hegemony is not yet broken but there are many cracks in its architecture—and appeal. While the Internet Question seems stagnant and unresolved, the hype caravan has moved on. From VR, crypto and Web3 where now all in the grip of ‘AI' as the latest marketing term for machine learning and large language models that feeds its generic ’summary’ content back into the Web. While the ‘infuencer’ persona is in decline, the mass obsession with likes, views, comments, swiping from one TikTok video to the next, retweets funny memes continues. What will disrupt the 'bored billions’ is the automation of mediactivism (already visible in the current waves of fake news and deep fakes). While officials raise the ethics issue of AI for Good, asking the impossible question how to buid ‘responsible’ extraction software, hackers, designers and other artists are coding subversive tools that disrupt and undermine the totalitarian system of control, ‘polluting’ data bases, turning fake news and monstrous images upside down. The message is clear:the world prepares for planetary cyberwarfare. Are you ready?
  

    # Jour 3
  - date: 2024-04-26
    title: "Aesthetic autonomy of users of digital platforms: process of subjectification and creation of collective imaginations"
    schedule:
      - moderator: Viviana Lipuma          
        guests:
          - Laurence Allard
          - Anaïs Nony       
        start_time: 17:30
        end_time: 19:30
        intro: "Les défis sociaux contemporains nous invitent à élaborer un concept plus exigeant de ce qu’on entend par “domaine public”, et donc à aller au-delà de sa définition purement négative d'un domaine qui n'est pas (encore) dans les mains des grands acteurs du capitalisme. L'exigence démocratique de protéger et d’étendre ce qui nous appartient collectivement doit porter sur notre santé, nos manières d'habiter la ville, nos manières de communiquer, mais aussi sur nos imaginaires et nos mises en récit. Or, à l’encontre de l'espoir d'un “espace public parallèle” (Mattelart) en opposition au dispositif télévisuel, à ses clichés et à son fonctionnement vertical aux débuts de l’internet, depuis les années 2000 la tendance est au contraire à l'infotainement, à la consommation passive de signes et à la production par tout un.e chacun.e de contenus convenus en des formats convenus.  La page de l’aventure collective du médiactivisme semble définitivement tournée:en même temps qu'une dépossession des moyens de production et une perte de savoirs techniques, on assiste à l'affaiblissement des capacités collectives à façonner des visions du monde. Le concept de “sémiocapitalisme” de Franco Berardi décrit cette main-basse sur les signes de la part des instances techno-économiques du capitalisme numérique qui en contrôlent la diffusion algorithmique, mais aussi la production à travers les icônes du web (youtubeurs et influenceurs) soumis à leurs contraintes de valorisation financière (audience, likes). Dans ce nouveau contexte infrastructurel, comment peut-on activer et cultiver les capacités esthétiques de l'intelligence collective, qui a montré savoir s'autodéterminer politiquement et utiliser tactiquement des outils du web pour mener à bien ses luttes? Cette question nous invite à envisager une fabrique autonome des imaginaires, susceptible de renouer avec les pratiques de subjectivation sémio-technique du médiactivisme, mais sans les figures tutélaires d’un savoir et d’une compétence esthétiques qui les encadraient. Nous estimons que cette fabrique autonome des imaginaires nous permettrait de nous orienter vers des choix d'avenir et de complexifier la compréhension qu'on a du social. L'objectif de cette séance sera de détailler les conditions matérielles et mentales d'une autonomie esthétique des usager.è.s des NTIC, en se montrant attentifs à son impossible assimilation à un “imaginaire révolutionnaire” propre au vidéoactivisme."

    # Jour 4
  - date: 2024-05-13
    title: Digitalization of journalism, the disruption of professional journalism
    schedule:
      - moderator: Igor Galligo       
        guests:
          - Aurélie Aubert
          - Jérôme Valluy     
        start_time: 18:00
        end_time: 20:30 (Claude Pompidou room. 5th floor)
        intro: Le néologisme « collution », inventé par Franck Rebillard et Nikos Smyrnaios décrit de manière convaincante un des effets du processus de disruption du journalisme professionnel, à la fois premier partenaire et première victime du tournant numérique. La collution désigne à la fois une collusion socioéconomique et une dilution éditoriale mêlées. Du côté de la collision, le « capitalisme de surveillance » (Shoshana Zuboff, 2018) s’est développé sur la spoliation financière du journalisme, son discrédit par un « journalisme citoyen » qui tend vers l'automédiation, le renforcement de sa surveillance policière (Pegasus, Predator…) et sa dilution dans le « web synthétique » (O.Ertzscheid). Du côté de la collusion, les journalistes sont les premiers à rediffuser leurs articles, à utiliser les sources et ressources numériques, à ignorer les atteintes à la vie privée et à partager des revenus de publicités individualisées plutôt que de les combattre et défendre des conditions autonomes et démocratiques de leur travail. Comment alors expliquer que la profession s’empresse dans une collaboration autodestructrice ? Tandis que le médiactivisme revendiquait une rupture claire et nette avec le journalisme, il semblerait au contraire que la distinction entre automédiation et journalisme présente aujourd'hui une porosité produite par la pression des nouvelles plateformes de communication du capitalisme numérique qui affecte la profession journalistique ainsi que ses productions.  Pour comprendre ce phénomène, nous suivrons l’hypothèse - aujourd'hui consolidée par de nombreuses recherches - d’une nouvelle dépendance journalistique, considérée comme un « fait social » au sens de Durkheim dans « Le suicide » (1897).

    # Jour 5
  - date: 2024-05-27
    title: "Automedias: Rethinking platform design"
    schedule:
      - moderator: Ludovic Duhem          
        guests:
          - Michael Bourgatte
          - Samuel Huron
          - Tallulah Frappier       
        start_time: 18:00
        end_time: 20:30 (Claude Pompidou room. 5th floor)
        intro: L’idée d’automédiation suppose une émancipation de la logique médiatique dominante et de son discours officiel. Mais si le contenu est revendiqué comme une alternative nécessaire, légitime et crédible au nom de la vérité, de la liberté et de la démocratie, les plateformes de diffusion de cet activisme alter-médiatique ou contre-médiatique interroge rarement les conditions de sa conception et de sa diffusion. Cette séance cherchera à mettre en évidence d’une part les questionnements incontournables sur le design des plateformes numériques de création de contenu produites par le capitalisme des GAFAM et les BATX et sur les alternatives open source ainsi que les possibilités et intérêts d’un co-design des plateformes par les utilisateurs. Elle mettra également en question la tradition médiactiviste et sa fabrique de l'information (oppositionnelle et révolutionnaire) pour définir les conditions d'une bifurcation pharmaco-politique où l'enjeu est moins l'appropriation et le détournement, la résistance et la confrontation que l'invention du collectif démocratique par la pratique automédiatique.

    # Jour 6
  - date: 2024-06-20
    title: Automedia through the prism of gender minorities
    schedule:
      - moderator: Lipuma Viviana         
        guests:
          - Mona Gérardin-Laverge
          - Hélène Fleckinger
          - Claire Blandin         
        start_time: 17:00
        end_time: 19:30 (room Atelier 1 of the Public Information Library, Access Center Pompidou)
        intro: "L'invisibilisation des minorités politiques est une constante des médias dominants : réduites à des objets dont on parle, à qui on ne donne la parole et l'image que pour confirmer des stéréotypes de genre, de race et de classe qui circulent à leur propos, cette relégation en coulisses coïncide dans les faits avec leur exclusion de l'arène citoyenne. Le féminisme a historiquement essayé d'évincer cet état de fait médiatique en faisant des technologies d'information disponibles les alliés de leurs combats. Après le tract et la presse écrite au tournant du 20ème siècle, le magazine et la vidéo expérimentale dans les années 1970, on assiste depuis les années 1990 à une « troisième vague » qui investit activement les nouveaux médias d'information et de communication. Le cyberféminisme, terme qui désigne un ensemble de pratiques hétérogènes menées par les militantes féministes sur internet, inaugure un nouvel « espace oppositionnel » (Mattelart). Mais la création de cet espace n’a pas seulement servi à rendre audibles les revendications sur la parité de genre, la fin des violences sexistes et sexuelles, la prise en compte du travail domestique gratuit, la remise en question des normes esthétiques et sociales. De nombreux.ses sociologues insistent, en effet, sur ce que l'architecture du web fait au mouvement : les blogs et les vlogs font éclore un expressivisme digital où les agencements textuels, visuels et sonores des profils donnent lieu à « une identité par bricolage » (Allard) ; le web apparaît en outre comme un espace inédit de convergence des luttes - à l’instar du mouvement metoo; enfin, le journalisme citoyen et l'édition participative rendent possible un élargissement sans précédents d'une « base » gagnée à la cause. Mais quel est le prix de cette révolution médiatique? On sait que le fonctionnement de l’espace numérique favorise une prise de parole et une prise individuelle, focalisée sur l’expérience d’oppression vécue en première personne et à une hyper-exposition de la manière dont on la vainc individuellement. Ces opérations sémiotiques se réalisent au détriment d’une saisie structurelle des dominations de genre et de la relation avec d’autres dominations. Il en résulte souvent, bien que non systématiquement, une simplification des débats et des engagements politiques. En interrogeant le devenir historique des pratiques médiatiques des minorités de genre (féministe, transféministe, queer et homosexuelles) à l’âge des plateformes numériques, cette séance a ainsi comme objectif de dresser un diagnostic politique des réussites et des échecs liés à ce changement de paradigme technologique."

    # Jour 7
  - date: 2024-07-10
    title: "Generative AI: a new kind of Automedia ?"
    schedule:
      - moderator: Nina Begus (in english)         
        guests:
          - Gasper Begus 
          - Agnieszka Kurant      
        start_time: 17:00
        end_time: 19:30 (session in the PS room at the Center Pompidou. Access - 1)
        intro: Every technology is molded by our cultural understanding of it, including its fictional representations. The very creation of AI systems and our interactions with them are influenced by how we conceptualize AI. How do fictional representations limit our view, and in what ways are they beneficial? The talk will focus on AI and speech. While AI is imitating human speech, it is also producing its artificial speech in a unique manner. Generative AI presents a yet unprecedented, new kind of automedia:self-productive, automatic, and autonomous but also tied to human design and prompts. No AI system is isolated from its means of production, including broader social and cultural aspects.  We will demonstrate how the cultural implications of language learning have shaped AI products as we recognize them today. Nina Begus will introduce a fresh approach to designing AI systems that veers away from the traditional anthropomorphic trajectory. Avoiding external projections can only succeed with enhanced interpretability of automatic and highly generative AI. Engaging in a dialogue with this talk, we are honored to invite UC Berkeley professor Gašper Beguš, an expert on GANs and speech, and Agnieszka Kurant, a conceptual artist investigating collective and nonhuman intelligences and the exploitations present in digital capitalism.

---

# Presentation

"Whether it is the self-media gesture produced on the fly by a single individual with a smartphone, or the collective auto-media enterprise already seen in the field of democratic struggles and experiments, which reinvents the forms of tactical media and of mediactivists, automediation refers to the self-production and self-communication of political information through the use or reinvention of digital communication devices and circuits."  This is the definition of Automedia given in the introduction to the collective research dossier published by the organization of the same name in issue 6 of Les Cahiers Costech.  

Yet if automedia and automediation seem to have inherited the avant-garde of tactical media — or, more broadly, of the mediactivist tradition — their reinvention in the age of technology, digital platforms, and Artificial Intelligence appears, even today, rather an issue to be conquered than an established socio-technical reality. While a media transformation is indeed underway as a result of the conditioning brought about by the digitization of communication media, this does not guarantee a renewal of the political issues targeted by the mediactivist tradition. 

On the contrary, the infrastructural context in which these new practices are developing seems to bear witness more to the cultural mutations of information capitalism than to a renewal of the counter-hegemonic posture  proper to mediactivism, which would require — among other things — technological sovereignty and a new media economic organization. With the exception of a few digital programs (such as Discord, Mastodon, Telegram or Mobilizon), communication on the Internet today takes place primarily on the major platforms of digital capitalism, whether American (Facebook, X, YouTube, Instagram, Twitch, Linkedln, etc.) or Chinese (TikTok). This applies to the vast majority of information disseminated online, irrespective of the political leaning or intent of its source.

More precisely, the digital turn of mediactivism seems to empty the movement of its radicality, as if in an attempt to defuse its political ambition, while capitalizing on the digital wills and expressions of its participants. While mediactivism are likely to glean new political potential from digital technologies and Artificial Intelligence , we hypothesize here that its political aims are being corrupted by the techno-economic context to which they are now subject — a process that Bernard Stiegler calls disruption.

In his book États de choc, Bêtise et savoir au 21ème siècle , Stiegler develops Naomi Klein's analyses published in La Stratégie du choc:la montée d'un capitalisme du désastre  to conceptualize a "strategy of technological shock in which it is the conditions of autonomy and heteronomy of academic institutions in the broadest sense [...] that are radically modified " by a techno-economic context conducive to destructive innovation. Stiegler later redeveloped this analysis in Dans la Disruption, Comment ne pas devenir fou?  to generalize this process beyond the assault on state institutions by libertarian techno-capitalism. 

However, the major interest of Stieglerian analysis lies in concepts aimed at deconstructing the posture of political opposition in order to renew forms of combat (becoming techno-political) and to elicit responses specific to its technological devenir. Following in the footsteps of Jacques Derrida , Stiegler recalls the pharmacological nature of technology (that all technology is both poison and remedy) and advocates bifurcation rather than revolution as a strategy for combating disruption. If "the [pharmaco-political] question is always to turn poison into remedy", then the political challenge is no longer so much to oppose this process of transformation, in order to return to the mediactivist tradition, as to bifurcate it towards digital politico-media forms that can resolve certain aporias of the mediactivist tradition.

Thus, we propose the term automedia to describe the media concept that has emerged      from the disruption of mediactivism by digital capitalism, redefining the mediactivist as YouTuber or TikToker . The stakes of this seminar are twofold. Firstly, it will explore the hypothesis of this disruption, which constitutes automedia as a destructive innovation of the mediactivist movement. Secondly, it will examine the devenir and potential forms of a pharmaco-politics of automedia, in order to conceptualize and shape its positive devenir .

Several developments allow us to characterize this disruption and to distinguish the automedia model that is emerging from the mediactivist model from which it originated. We note two major evolutions in relation to mediactivism:auto-media implicates principles of both self-production and technological auto-mation within media production companies. The term thus synthesizes a semantic declension from the prefix "auto-" to produce at least two meanings.

In the technical history of mediactivism, the dynamics of self-production were reserved for a few initiates from the world of audiovisual and/or film production, who came together in groups to form a collective factory. In contrast, the automediated factory lends itself to individual practice and embodiment by any user of digital communication devices and platforms, without requiring specialized technical (audiovisual) or technological skills. Furthermore, the processes of technological auto-mation are today being renewed by the digital technologies and Artificial Intelligence of today are now  bringing the circulation of information into a new era of media production that did not exist at the birth of mediactivism half a century ago. 

The monthly sessions of this seminar, which will run until the summer of 2024, will each examine some of the salient points of this disruption. They will also attempt to propose bifurcations in order to constitute the concept of automedia and the practice of automediation as a positive transformation of mediactivism in the digital age and Artificial Intelligence.
