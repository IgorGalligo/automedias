---
title: Deepfake
subtitle: 'A Rhetorical and Economic Alternative to Address the&nbsp;So—Called "Post-Truth Era"'
date: 2023-05-10
event_date: 
  - 2023-05-10
place: University of California, Berkeley

authors: Igor Galligo

organizers: 
  - Automedias.org

locale: en

linked_publications:
  - 007

#
# Info Pratique
#
practical_info: 


  - title: Joins us
    description: '
    The event will be held at UC Berkeley, in Matrix Conference Room, and online with Zoom. 

  - May 10, 2023, from 9a.m. to 7 p.m.

      <br>
      [Matrix Conference Room](https://matrix.berkeley.edu/using-space-matrix/){.link .l} 

      Please be sure to register

      [Register link](https://docs.google.com/forms/d/e/1FAIpQLSe89O_QyIEXNxiSVK09wQIoHXhpbkUMlYkitPdJNrTPs8YqOA/viewform?usp=pp_url ){.link .l} 

      *University of California, Berkeley
       820 Social Sciences Building
       Berkeley, CA 94720-1922*   
      <br><br>
      
      '
    

  - title: Participating online
    description: "
      A Zoom link will be provided after registration.

      Please be sure to register early.

      [Register link](https://docs.google.com/forms/d/e/1FAIpQLSe89O_QyIEXNxiSVK09wQIoHXhpbkUMlYkitPdJNrTPs8YqOA/viewform?usp=pp_url ){.link .l} 
    
      "


aside:
  - title: Organization
    content: "
    ## Project owners:

     - Igor Galligo   <br>
       *Researcher in Media Sciences and founder of the Automedias.org project*

     ## Co-organizers:

     - David Bates   <br>
       *Professor of Philosophy at UC Berkeley*

     - Marion Fourcade   <br>
       *Professor of Sociology at UC Berkeley*

     - James Porter   <br>
       *Professor of Rhetoric at UC Berkeley*

     - Morgan Ames   <br>
       *Professor of Science of Information and Communication at UC Berkeley*

     ## Production Help:

     Faith Enemark, Heather Reilly, Davinderjeet Sidhu, Lupita Rodriguez, Eva Y Seto, Julia Sizek, Chuck Kapelke, Oscar Calva, Warren Neidich and Andres Sandoval.
    "


#
# Programme
#
programme:
  
  # Jour 1
  - date: 2023-05-10
    title: Deepfake
    schedule:
      - session: Opening
        format: opening
        description: 
        start_time: 9:00
        end_time: 9:15
        speakers: Igor Galligo
        
      - session: "Rhetoric, Democracy and “Post-Truth”"
        intro: "How are rhetoric and fakeness consubstantial with democracy? To what conception of truth does the notion of \"post-truth\" correspond? And why is Post-Truth a problematic notion for the rhetorical tradition?"
        speakers: James Porter
        start_time: 9:20
        end_time: 9:45

      - session: "Rhetoric, Democracy and “Post-Truth”"
        speakers: Linda Kinstler
        start_time: 9:50
        end_time: 10:15

      - session: "Rhetoric, Democracy and “Post-Truth”"
        speakers: Chiara Cappelletto
        format: Discussant
        start_time: 10:20
        end_time: 10:30

      - session: "Rhetoric, Democracy and “Post-Truth”"
        description: collective discussion with the audience
        start_time: 10:30
        end_time: 10:50
        
      - break: true
        start_time: 10:50
        end_time: 11:00  

      - session: "Subjectivity, Digital Computationalism and Artificial Intelligence"
        intro: "How does the theorization of contemporary computing, which gave birth to the Internet and artificial intelligence, and which is based on computationalism, constitute a problematic conception of subjectivity? How is this conception opposed to the rhetorical and hermeneutic tradition? What conceptions of truth are discarded by computationalism?"
        speakers: David Bates
        start_time: 11:00
        end_time: 11:25 

      - session: "Subjectivity, Digital Computationalism and Artificial Intelligence"
        speakers: Warren Neidich
        start_time: 11:30
        end_time: 11:55 

      - session: "Subjectivity, Digital Computationalism and Artificial Intelligence"
        speakers: Morgan Ames
        start_time: 12:00
        end_time: 12:10
      
      - session: "Subjectivity, Digital Computationalism and Artificial Intelligence"
        description: collective discussion with the audience
        start_time: 12:10
        end_time: 12:30

      - break: true
        start_time: 12:30
        end_time: 14:00 
        
      - session: "Critical Digital Rhetoric"
        intro: "What renewals can be made within the rhetorical tradition to adapt it to the digital political and Artificial Intelligence contexts? What critical political powers can digital rhetoric retain in the face of computational digital media, fed by data sciences in the new social spaces that are the Internet and social networks? "
        speakers: Nina Begus
        start_time: 14:00
        end_time: 14:25

      - session: "Critical Digital Rhetoric"
        speakers: Justin Hodgson
        start_time: 14:30
        end_time: 14:55

      - session: "Critical Digital Rhetoric"
        speakers: Nathan Atkinson
        start_time: 15:00
        end_time: 15:10

      - session: "Critical Digital Rhetoric"
        description: collective discussion with the audience
        start_time: 15:10
        end_time: 15:30

      - break: true
        start_time: 15:30
        end_time: 15:40 

      - session: "Computational Capitalism and Surveillance Capitalism in light of the Deepfake"
        intro: "What conceptions and productions of truth do computational capitalism and surveillance capitalism promote? And against what conceptions or practices of producing truth do they discriminate? To which social groups, does this discrimination pose problems of expression and individuation today?"
        speakers: Marion Fourcade 
        start_time: 15:40
        end_time: 16:05

      - session: "Computational Capitalism and Surveillance Capitalism in light of the Deepfake"
        speakers: Igor Galligo 
        start_time: 16:10
        end_time: 16:35

      - session: "Computational Capitalism and Surveillance Capitalism in light of the Deepfake"
        speakers: Konrad Posch 
        start_time: 16:40
        end_time: 16:50

      - session: "Computational Capitalism and Surveillance Capitalism in light of the Deepfake"
        description: collective discussion with the audience
        start_time: 16:50
        end_time: 17:10

      - break: true
        start_time: 17:10
        end_time: 17:20 

      - session: "For a New Digital Political Economy of Deepfake"
        intro: "How to extend the digital political economy to the symbolic and iconic economy? What new rhetorical and hermeneutic economy of truth can political economy invent? What circuits of collective truth production can political economy develop to grant the deepfake political meaning and value? "
        speakers: Martin Kenney 
        start_time: 17:20
        end_time: 17:45

      - session: "For a New Digital Political Economy of Deepfake"
        speakers: Mark Nitzberg 
        start_time: 17:50
        end_time: 18:15

      - session: "For a New Digital Political Economy of Deepfake"
        speakers: John Zysman 
        start_time: 18:20
        end_time: 18:30

      - session: "For a New Digital Political Economy of Deepfake"
        description: collective discussion with the audience
        start_time: 18:35
        end_time: 18:55


#
# Partenaires
#
partners:

  - name: European Union
    logo: eu.webp
    category: Funding and Scientific Partners
  - name: UPL
    logo: UPL.webp
    category: Funding and Scientific Partners 
  - name: Nest
    logo: NEST.webp
    category: Funding and Scientific Partners
  - name: Townsend Center for the Humanities, UC Berkeley
    logo: townsend.webp
    category: Funding and Scientific Partners

  - name: Department of Rhetoric, UC Berkeley
    logo: rhetoric.webp
    category: Scientifics Partners of UC Berkeley
  - name: Social Sciences Matrix 
    logo: matrix_berkley.webp
    category: Scientifics Partners of UC Berkeley
  - name: Center for Science, Technology, Medicine & Society 
    logo: cstms.webp
    category: Scientifics Partners of UC Berkeley
  - name: Network for New Political Economy 
    logo: n2pe.webp
    category: Scientifics Partners of UC Berkeley
  - name: Berkeley Economy and Society Initiative
    logo: berkeley.webp
    category: Scientifics Partners of UC Berkeley

  - name: Berkeley Economy and Society Initiative
    logo: saas_fee.webp
    category: Other Scientific Partner

legals: "This project has received funding   from the MSCA-RISE program under grant agreement No 101007915"

    
    
#
# Printed Matter
#
print: 
  cover_fallback: events/004-Deepfake.png
  credit: Illustration Lyes Hammadouche
  prefered_color: 65
  average_day_duration: "9am<br> → 7pm"  

#
# Pinned Document
#
pinned:
  - image: events/004-Deepfake.png
    link: ""

---
# Presentation

Since Greek antiquity, the rhetorical tradition has proposed to conceive and apprehend the search for truth differently from the Western philosophical tradition that was born with Plato. Platonic politics wished to control the city by subjecting political expression to the philosophical concept, whereas rhetoric opposed the logocratic and universal claim of philosophy, in the name of the diversity of subjectivities and forms of life that composed the demos, and justified democratic deliberation as a form and process of agreement and democratic agency. 

This symposium aims to develop a critique of the current debates against Post-Truth and fakeness, led today by Big Tech in an effort to ensure its hegemony on the process of subjectivation  and to control the political expression of the demos through the control of the digital economy, which today includes the economy of creation and economy of imagination. In addition to the critical force of the rhetoric that we wish to rehabilitate, in order to denounce the illusion of a digital democracy through the current platforms of digital capitalism, this colloquium would like to suggest a different approach to the problems related to the deepfake by proposing an articulation between a Critical Digital Rhetoric and a Digital Political Economy. 

Rather than censoring the new combination of Fakeness and Artificial Intelligence, called the deepfake, as Big Tech is doing today, we wish to reintegrate the production of deepfake in a new digital political economy, which would exploit the rhetorical potential of deepfake in a new economy of digital democracy. The latter would face the challenge of revealing the democratic value of the deepfake through the possibility of a circulation and a reappropriation of symbolic images as well as a digital hermeneutic. It is thus a new rhetorical paradigm of digital democracy that we wish to promote as an alternative to the alienating alliance of surveillance capitalism, computational capitalism, computational sciences, and data sciences.

[Read the argument](https://automedias.org/en/publication/007-A/)

