---
title: Automedias
subtitle: Une Révolution Médiatique <span>Comment chacun.e peut-iel contribuer à la production de l'information&thinsp;?</span>
locale: fr

place: Maison des Sciences de l’Homme Paris-Nord
postal_adress: 20, avenue George Sand, La Plaine St-Denis


#
# teaser
#
teasers:
  - title : Automedias - teaser<br>Igor Galligo et Cemil Sanli *(organisateurs du projet)*<br>Julien Letourneur *(aide à la réalisation)*. 
    iframe: <iframe src="https://player.vimeo.com/video/717220173?h=fa32a4e575&autoplay=0&title=0&byline=0&portrait=0" width="640" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
    url: https://vimeo.com/717220173
    cover: https://i.vimeocdn.com/video/1447818732-de39acb5274192ea8e84f37751c4a3cdc7b610bfcc150043a3aa5300c935b276-d?mw=1500&mh=844&q=70

#
#
# Vidéo
#
videos:
  - title : "Automédias : Description des automédias"
    url:  https://video.lemediatv.fr/w/aeYocTUXnQKNtAM8Jc2LfU?start=0s
    iframe: '<iframe title="AUTOMÉDIAS : QUAND LES CITOYENS PRENNENT LE POUVOIR 2022-06-22 10:00" src="https://video.lemediatv.fr/videos/embed/4ad5aa03-5852-421b-ba8c-cb6cb03dcfe8?title=0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups" width="560" height="315" frameborder="0"></iframe>'
    cover: https://video.lemediatv.fr/lazy-static/previews/b9b12241-fe68-4f6b-a42f-2613d329d1dd.jpg

  - title : "Automédias : Valeurs et normes des automedias"
    url: https://video.lemediatv.fr/w/knC8UZfGHeKpEtD9aFJ9AZ?
    iframe: '<iframe title="AUTOMÉDIAS : QUAND LES CITOYENS PRENNENT LE POUVOIR #2 2022-06-23 10:00" src="https://video.lemediatv.fr/videos/embed/9ce28609-84dd-41a9-8448-f21ae88b8d2d?title=0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups" width="560" height="315" frameborder="0"></iframe>'
    cover: https://video.lemediatv.fr/lazy-static/previews/692af10f-9d2c-45ba-8953-eb87665cddde.jpg 

  - title : "Automédias : Nouveaux enjeux des médias contributifs"
    url: https://video.lemediatv.fr/w/6qhGZoXQqv9bBX2Fvg7Cfb?start=6s
    iframe: '<iframe title="AUTO-MÉDIAS: QUAND LES CITOYENS PRENNENT LE POUVOIR #3 2022-06-24 10:00" src="https://video.lemediatv.fr/videos/embed/2be1d6f3-f6d3-4432-b0e5-ec7c83ffab86?title=0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups" width="560" height="315" frameborder="0"></iframe>'
    cover: https://video.lemediatv.fr/lazy-static/previews/f5b5a78f-db2c-4fbd-ad73-830805edb85d.jpg


#
# Info Pratique
#
practical_info: 

  - title: Venir sur place
    description: '
    L’événement se tiendra à la Maison des Sciences de l’Homme Paris-Nord les 22, 23 et 24 juin 2022 de 9h30 à 19h30.

      <br>
      [Maison des Sciences de l’Homme Paris Nord](https://www.mshparisnord.fr/){.link .l} 

      *20 avenue George Sand,   
      
      93210 La Plaine St-Denis*   
      <br><br>
      Métro :   
      Front Populaire *(Ligne 12)*


      <figure class="justPrint"><img data-src="/../../assets/media/events/mdsh_paris.jpg" class="lazy" aria-hidden="true"></figure>


      '
  - title: Suivre en ligne
    description: "
      L’évènement, réalisé en partenariat avec la chaîne d’info Le Media, sera retransmis en direct sur le site web de la chaîne.

      [Le Media](https://www.lemediatv.fr){.link .l}
      "

  - title: Télécharger le programme
    description: "
      Le programme de l'évenement est disponible sur cette page ou disponible en PDF
      
      [Programme de l'evenement](/../../assets/media/events/003-Automedias-22→24_juin_2022-programme.pdf){.link .l}
      "

aside:

  - title: Organisation
    content: "
    ## Porteurs du projet&thinsp;:

    - Igor Galligo   <br>
      *Chercheur en sciences des médias et fondateur du projet automedias.org*

    - Cemil Sanli   <br>
      *Journaliste indépendant et fondateur de Cemil Choses à te dire*

    ## Coorganisateurs&thinsp;:

    - Ludovic Duhem   <br>
      *Chercheur en philosophie et directeur de la recherche de l’ESAD Valenciennes*

    - Zakaria Bendali   <br>
      *Chercheur en sociologie politique*

    - Edouard Bouté   <br>
      *Chercheur en sciences de l’information et de la communication*

    - Camille Lizop   <br>
      *Chercheuse en philosophie*

    ## Aide à la production&thinsp;:

    Anne Sedes, Yves Citton, Victor Chaix, Catherine Partin, Julien&nbsp;Letourneur, Arnaud Prêtre (ISIT) et l'équipe de Le Media. 
    "

#
# Lien Inscription
# 
inscription: 
  method: inscription
  howto: "
    L'évènement est ouvert à toutes et tous. Inscription demandée.

    [Inscription](https://www.helloasso.com/associations/noodesign/evenements/inscription-evenement-automedia ){.link .xxl}


    Pour soutenir notre projet&#8239;:   

    [okpal.com/automedias](https://www.okpal.com/automedias/ ){.link .l .collecte}


    Pour tous renseignements complémentaires&#8239;:  

    [contact@automedias.org](mailto:contact@automedias.org){.link .l .mail}
  "

#
# Programme
#

programme:

  # Jour 1
  - date: 2022-06-22
    title: Journée 1 __Description des automédias__
    schedule:
      - session: Ouverture
        format: ouverture
        description: 
        start_time: 9:30
        end_time: 9:45
        speakers:
          - Cemil Sanli
          - Igor Galligo
          - Yves Citton

      - session: Affects et esthétique de l'automédiation
        moderator: Ludovic Duhem
        isit: 
          - Nancy Pratt (ISIT)
          - Geoffrey Stücklin (ISIT)
        guests: 
          - Eric Kluitenberg
          - André Gunthert
          - David Garcia
          - David Bates
        start_time: 9:45
        end_time: 11:25
        debate: 
          start_time: 11:00
          duration: 25



      - session: Populisme et Automédia
        description: 
        moderator: Ludovic Duhem
        isit: 
          - Nancy Pratt (ISIT)
          - Geoffrey Stücklin (ISIT)
        guests: 
          - Paolo Gerbaudo
          - Dominique Cardon
          - Jen Schradie
          - Harry Halpin
        start_time: 11:30
        end_time: 13:00
        debate: 
          start_time: 12:35
          duration: 25


      - break: true
        start_time: 13:00
        end_time: 14:30

      - session: Présentation du projet AUTOMEDIAS.ORG
        description: 
        start_time: 14:30
        end_time: 15:10
        speakers: Igor Galligo
        debate: 
          start_time: 15:00
          duration: 10        

      - session: Les automédias Gilets Jaunes, naissance d'un mouvement.
        moderator: Igor Galligo
        guests: 
          - Jérôme Rodrigues
          - Faouzi Lellouche
          - Canal Concorde
          - Louis Paccalin
          - Luc Gwiazdzinski
          - Ulrike Lune Riboni
          - Mélanie Lecha
          - Louison Suberbie
        start_time: 15:15
        end_time: 17:00
        debate: 
          start_time: 16:30
          duration: 30

      - session: Ecologie et Automédias
        description: 
        moderator: 
          - Cemil Sanli
          - Édouard Bouté
        guests: 
          - Le Biais Vert
          - Humeco
          - Mr. Mondialisation (sous réserve)
          - Laurence Allard
          - Pauline Chasseray-Peraldi
        start_time: 17:05
        end_time: 18:35            
        debate: 
          start_time: 18:05
          duration: 30

      - break: true
        start_time: 18:35
        end_time: 18:50          

      - session: Atelier avec le public 1
        format: Atelier
        start_time: 18:50
        end_time: 19:50

  # Jour 2
  - date: 2022-06-23
    title: Journée 2 __Valeurs et normes des automedias__
    schedule:

      - session: Les Arts automédiatiques
        moderator: Zakaria Bendali
        guests: 
          - Lucas Roxo
          - Wael Sghaier
          - Pauline Desgrandchamp
          - Nawal Hafed
          - Louis Moreau-Avila
          - Victor Chaix
          - Igor Galligo
        start_time: 9:30
        end_time: 11:00
        debate: 
          start_time: 10:30
          duration: 30

      - session: Complots et vérités à l'ère du capitalisme numérique
        moderator: Igor Galligo
        guests: 
          - Henri Boullier
          - Baptiste Kotras
          - Louis Morelle
          - Yves Citton
          - Le Canard Réfractaire
          - Le Biais Vert          
        start_time: 11:05
        end_time: 12:35
        debate: 
          start_time: 10:30
          duration: 30

      - break: true
        start_time: 12:45
        end_time: 14:15

      - session: Dépasser la religion de l’audience. Quelles nouvelles valeurs pour les automédias&#8239;?
        moderator: Igor Galligo
        guests: 
          - Cerveaux Non Disponibles
          - Dany Caligula
          - Osons Causer
          - Cemil Sanli
          - Laurence Allard
          - Anne Alombert
        start_time: 14:15
        end_time: 16:00
        debate: 
          start_time: 15:30
          duration: 30

      - session: Confiance et crédibilité dans les médias dominants. Où en est la critique du journalisme&#8239;?
        moderator: Cemil Sanli
        guests: 
          - ACRIMED
          - Arrêt sur Images
          - Le Monde Diplomatique
          - François Jost
          - Jean-Marie Charon
          - Raphaël Lupovici
        start_time: 16:05
        end_time: 17:45
        debate: 
          start_time: 17:15
          duration: 30  

      - break: true
        start_time: 17:35
        end_time: 17:50          

      - session: Atelier avec le public 2
        format: Atelier
        start_time: 18:00
        end_time: 19:30


  # Jour 3
  - date: 2022-06-24
    title: Journée 3 __Nouveaux enjeux des&nbsp;médias contributifs__
    schedule:
      - session: Économie de la contribution&thinsp;&#58; Après le crowdfunding, quelle économie pour les automédias&#8239;?
        moderator: Igor Galligo
        guests: 
          - Franck Rebillard
          - Michel Bauwens
          - Théophile Kouamouo
        start_time: 9:30
        end_time: 11:00
        debate: 
          start_time: 10:30
          duration: 30

      - session: Philosophie de la contribution et Design de l'information
        moderator: Igor Galligo
        guests: 
          - Sylvia Fredriksson
          - Samuel Huron
          - Michelle Christensen
          - Florian Conradi
        start_time: 11:05
        end_time: 12:45
        debate: 
          start_time: 12:15
          duration: 30

      - break: true
        start_time: 12:45
        end_time: 14:15

      - session: Comment acquérir une souveraineté numérique sans perdre une puissance médiatique&#8239;?
        moderator: 
          - Cemil Sanli
          - Edouard Bouté
        guests: 
          - Octopuce
          - Assemblée Virtuelle
          - French Data Network
          - Yves Citton
          - Chloé Gence
        start_time: 14:25
        end_time: 15:55
        debate: 
          start_time: 15:25
          duration: 30

      - session: Incarnation ou structures décentralisées. Comment dépasser l’opposition verticalité&thinsp;/&thinsp;horizontalité&#8239;?
        moderator: 
        - Zakaria Bendali
        - Édouard Bouté
        guests: 
          - La Mule
          - Le Canard Réfractaire
          - Dany Caligula
          - Canal Concorde
          - Cemil Sanli
          - Ulrike Lune Riboni
        start_time: 16:00
        end_time: 17:45
        debate: 
          start_time: 17:15
          duration: 30

      - break: true
        start_time: 17:45
        end_time: 18:00

      - session: Atelier avec le public 3
        format: Atelier
        start_time: 18:00
        end_time: 19:30


legals: "This project has received funding from the MSCA-RISE program under grant agreement No 101007915"

---

# Présentation

Comme vous le savez, nous n’avons pas tous le même accès aux médias. Certaines classes sociales et opinions sont moins privilégiées que d’autres, voire invisibilisées, et cela nous le remarquons chaque jour sur les plateaux télé et dans la presse. Une majorité mise sous silence, qui s’est manifestée autrement avec le mouvement des Gilets Jaunes, et notamment grâce à un nouveau phénomène socio-technique : l’AUTOMEDIA&thinsp;!

L’AUTOMEDIA est né de l’association de la fonction vidéo des smartphones et de la fonction partage des réseaux et applications numériques. Il ou elle est un.e citoyen.ne, militant.e ou simple amateur.rice, qui décide par lui-même ou elle-même, de contribuer à la production et à la diffusion d’une information d’intérêt public par des moyens de captation, d’enregistrement et de communication numériques.
En échangeant collectivement des images, des paroles, ou des histoires, l’automedia contribue aussi à la constitution de valeurs, de rêves et de combats communs, qui font émerger de nouvelles communautés politiques. 

En France, le mouvement des Gilets Jaunes s’est propagé à une vitesse extraordinaire en s’appropriant des technologies automédiatiques. Il a pu tisser un réseau médiatique alternatif face aux chaînes d’information dominantes, qui lui a conféré une puissance de socialisation sans précédent depuis 50 ans.

Mais dans leur écrasante majorité, les automédias n’existent aujourd’hui qu’à travers les réseaux sociaux et supports d’un capitalisme numérique. Facebook, Twitter, YouTube, Instagram, TikTok, et d’autres, sont des entreprises privées qui répondent aux valeurs et aux logiques d’un modèle économique aliénant et discriminant. L’opacité des algorithmes de recommandation, les contraintes en matière de publication, la censure ou l'autocensure, la suppression arbitraire de certains groupes, l’existence souterraine d’une économie des médias consumériste et productiviste fondée sur le narcissisme, la publicité, le clash, l’émotion, la pulsion, etc. limitent aujourd’hui les ambitions politiques des automédias. Ceux-ci sont en réalité conditionnés par des designs numériques qui capitalisent sur les expressions de leurs excitations et de leurs effets sociaux, positifs ou négatifs.

Dans ce contexte, il nous apparaît aujourd’hui indispensable, non seulement de faire reconnaître l’avant-garde automédiatique de cette nouvelle démocratie numérique en train de naître, mais aussi d’inventer de nouvelles infrastructures numériques de l’information qui permettront à leurs usagers d’acquérir une souveraineté sur leurs outils de médiation, ainsi qu’une autonomie (plus qu’une indépendance) sur les valeurs qui sous-tendent les normes et les protocoles de production de leurs informations. Il faut donc que les automédias entrent dans une nouvelle phase d’auto-conception de leurs médias afin de produire des outils cohérents avec les horizons politiques des valeurs et des idéaux qu’ils défendent ou promeuvent. 
C’est là le sens du terme «&#8239;automédia&#8239;» et c’est le double enjeu de notre évènement. 

Les 22, 23 et 24 juin 2022, nous vous invitons à venir écouter et discuter avec des automédias, des journalistes, des développeurs informatiques et des chercheurs pour participer à une réflexion sur les fondements économiques, politiques et technologiques d’un nouveau système contributif automédiatique


Cet événement gratuit de trois jours aura lieu à La Plaine St-Denis, dans l’auditorium de la Maison des Sciences de l’Homme Paris-Nord, tout près du métro Front Populaire (Terminus ligne 12).


Il sera aussi retransmis en direct par la chaîne Le Media, partenaire de notre événement.

Pour participer, venez nombreux&thinsp;!
