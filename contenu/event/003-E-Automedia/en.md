---
title: Automedias
subtitle: A Media Revolution <span>How can everyone contribute to the production of information?</span>
locale: en

place: Maison des Sciences de l’Homme Paris-Nord
postal_adress: 20, avenue George Sand, La Plaine St-Denis


#
# teaser
#
teasers:
  - title : Automedias - teaser<br>Igor Galligo et Cemil Sanli *(project managers)*<br>Julien Letourneur *(assistance)*. 
    iframe: <iframe src="https://player.vimeo.com/video/717220173?h=fa32a4e575&autoplay=0&title=0&byline=0&portrait=0" width="640" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
    url: https://vimeo.com/717220173
    cover: https://i.vimeocdn.com/video/1447818732-de39acb5274192ea8e84f37751c4a3cdc7b610bfcc150043a3aa5300c935b276-d?mw=1500&mh=844&q=70


#
#
# Vidéo
#
videos:
  - title : "Automedia: Description of automedia"
    url:  https://video.lemediatv.fr/w/aeYocTUXnQKNtAM8Jc2LfU?start=0s
    iframe: '<iframe title="AUTOMÉDIAS : QUAND LES CITOYENS PRENNENT LE POUVOIR 2022-06-22 10:00" src="https://video.lemediatv.fr/videos/embed/4ad5aa03-5852-421b-ba8c-cb6cb03dcfe8?title=0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups" width="560" height="315" frameborder="0"></iframe>'
    cover: https://video.lemediatv.fr/lazy-static/previews/b9b12241-fe68-4f6b-a42f-2613d329d1dd.jpg


    
  - title : "Automedias : Automedia values and standards"
    url: https://video.lemediatv.fr/w/knC8UZfGHeKpEtD9aFJ9AZ?
    iframe: '<iframe title="AUTOMÉDIAS : QUAND LES CITOYENS PRENNENT LE POUVOIR #2 2022-06-23 10:00" src="https://video.lemediatv.fr/videos/embed/9ce28609-84dd-41a9-8448-f21ae88b8d2d?title=0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups" width="560" height="315" frameborder="0"></iframe>'
    cover: https://video.lemediatv.fr/lazy-static/previews/692af10f-9d2c-45ba-8953-eb87665cddde.jpg 



  - title : "Automedias : Emerging Issues for Contributed Media"
    url: https://video.lemediatv.fr/w/6qhGZoXQqv9bBX2Fvg7Cfb?start=6s
    iframe: '<iframe title="AUTO-MÉDIAS: QUAND LES CITOYENS PRENNENT LE POUVOIR #3 2022-06-24 10:00" src="https://video.lemediatv.fr/videos/embed/2be1d6f3-f6d3-4432-b0e5-ec7c83ffab86?title=0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups" width="560" height="315" frameborder="0"></iframe>'
    cover: https://video.lemediatv.fr/lazy-static/previews/f5b5a78f-db2c-4fbd-ad73-830805edb85d.jpg


#
# Info Pratique
#
practical_info: 


  - title: Join us
    description: '
    The event will be held at the Maison des Sciences de l’Homme Paris-Nord on June 22, 23 and 24, 2022 from 9:30 a.m. to 7:30 p.m.

      <br>
      [Maison des Sciences de l’Homme Paris Nord](https://www.mshparisnord.fr/){.link .l} 

      *20 avenue George Sand,   
      
      93210 La Plaine St-Denis*   
      <br><br>
      Métro :   
      Front Populaire *(Ligne 12)*

      <figure class="justPrint"><img data-src="/../../assets/media/events/mdsh_paris.jpg" class="lazy" aria-hidden="true"></figure>
      '
  - title: Watching online
    description: "
      The event, produced in partnership with the news channel Le Media, will be broadcast live on the channel's website.

      [Le Media](https://www.lemediatv.fr){.link .l}
      "

  - title: Download program
    description: "
      The event program is available on this page or available in PDF
      
      [PDF event program](/../../assets/media/events/003-Automedias-22→24_juin_2022-programme.pdf){.link .l}
      "

aside:
  - title: Organisation
    content: "
    ## Project owners:

     - Igor Galligo   <br>
       *Researcher in media sciences and founder of the automedias.org project*

     - Cemil Sanli   <br>
       *Freelance journalist and founder of Cemil Choses à te dire*

     ## Co-organizers:

     - Ludovic Duhem   <br>
       *Researcher in philosophy and director of research at ESAD Valenciennes*

     - Zakaria Bendali   <br>
       *Researcher in political sociology*

     - Edouard Bouté   <br>
       *Researcher in information and communication sciences*

     - Camille Lizop   <br>
       *Philosophy researcher*

     ## Production Help:

     Anne Sedes, Yves Citton, Victor Chaix, Catherine Partin, Julien&nbsp;Letourneur, Arnaud Prêtre (ISIT) and the Le Media team.
    "


#
# Lien Inscription
# 
inscription: 
  method: inscription
  howto: "
      The event is open to everyone. Registration requested.

     [Registration](https://www.helloasso.com/associations/noodesign/evenements/inscription-evenement-automedia ){.link .xxl}


     To support our project:

     [okpal.com/automedias](https://www.okpal.com/automedias/ ){.link .l .collection}


     For further information:

    [contact@automedias.org](mailto:contact@automedias.org){.link .l .mail}
  "

#
# Programme
#

programme:
  
  # Jour 1
  - date: 2022-06-22
    title: Day 1 __Automedia Description__
    schedule:
      - session: Opening
        format: opening
        description: 
        start_time: 9:30
        end_time: 9:45
        speakers:
          - Cemil Sanli
          - Igor Galligo
          - Yves Citton

      - session: Affects and aesthetics of self-mediation
        moderator: Ludovic Duhem
        isit: 
          - Nancy Pratt (ISIT)
          - Geoffrey Stücklin (ISIT)
        guests: 
          - Eric Kluitenberg
          - André Gunthert
          - David Garcia
          - David Bates
        start_time: 9:45
        end_time: 11:25
        debate: 
          start_time: 11:00
          duration: 25



      - session: Populism and Automedia
        description: 
        moderator: Ludovic Duhem
        isit: 
          - Nancy Pratt (ISIT)
          - Geoffrey Stücklin (ISIT)
        guests: 
          - Paolo Gerbaudo
          - Dominique Cardon
          - Jen Schradie
          - Harry Halpin
        start_time: 11:30
        end_time: 13:00
        debate: 
          start_time: 12:35
          duration: 25


      - break: true
        start_time: 13:00
        end_time: 14:30

      - session: Presentation of the AUTOMEDIAS.ORG project
        description: 
        start_time: 14:30
        end_time: 15:10
        speakers: Igor Galligo
        debate: 
          start_time: 15:00
          duration: 10        

      - session: Yellow Vests automedia, birth of a movement.
        moderator: Igor Galligo
        guests: 
          - Jérôme Rodrigues
          - Faouzi Lellouche
          - Canal Concorde
          - Louis Paccalin
          - Luc Gwiazdzinski
          - Ulrike Lune Riboni
          - Mélanie Lecha
          - Louison Suberbie
        start_time: 15:15
        end_time: 17:00
        debate: 
          start_time: 16:30
          duration: 30

      - session: Ecology and Automedia
        description: 
        moderator: 
          - Cemil Sanli
          - Édouard Bouté
        guests: 
          - Le Biais Vert
          - Humeco
          - Mr. Mondialisation (sous réserve)
          - Laurence Allard
          - Pauline Chasseray-Peraldi
        start_time: 17:05
        end_time: 18:35            
        debate: 
          start_time: 18:05
          duration: 30

      - break: true
        start_time: 18:35
        end_time: 18:50          

      - session: Workshop with the public 1
        format: Workshop
        start_time: 18:50
        end_time: 19:50

  # Jour 2
  - date: 2022-06-23
    title: Day 2 __Automedia values and standards__
    schedule:

      - session: Automedia Arts
        moderator: Zakaria Bendali
        guests: 
          - Lucas Roxo
          - Wael Sghaier
          - Pauline Desgrandchamp
          - Nawal Hafed
          - Louis Moreau-Avila
          - Victor Chaix
          - Igor Galligo
        start_time: 9:30
        end_time: 11:00
        debate: 
          start_time: 10:30
          duration: 30

      - session: Conspiracies and Truths in the Age of Digital Capitalism
        moderator: Igor Galligo
        guests: 
          - Henri Boullier
          - Baptiste Kotras
          - Louis Morelle
          - Yves Citton
          - Le Canard Réfractaire
          - Le Biais Vert          
        start_time: 11:05
        end_time: 12:35
        debate: 
          start_time: 10:30
          duration: 30

      - break: true
        start_time: 12:45
        end_time: 14:15

      - session: Go beyond the religion of the audience. What new values for automedia?
        moderator: Igor Galligo
        guests: 
          - Cerveaux Non Disponibles
          - Dany Caligula
          - Osons Causer
          - Cemil Sanli
          - Laurence Allard
          - Anne Alombert
        start_time: 14:15
        end_time: 16:00
        debate: 
          start_time: 15:30
          duration: 30

      - session: Trust and credibility in mainstream media. Where is the criticism of journalism?
        moderator: Cemil Sanli
        guests: 
          - ACRIMED
          - Arrêt sur Images
          - Le Monde Diplomatique
          - François Jost
          - Jean-Marie Charon
          - Raphaël Lupovici
        start_time: 16:05
        end_time: 17:45
        debate: 
          start_time: 17:15
          duration: 30  

      - break: true
        start_time: 17:35
        end_time: 17:50          

      - session: Workshop with the public 2
        format: Workshop
        start_time: 18:00
        end_time: 19:30


  # Jour 3
  - date: 2022-06-24
    title: Day 3 __Emerging Issues in Contributory Media__
    schedule:
      - session: Economy of contribution&thinsp;&#58; After crowdfunding, what economy for automedia?
        moderator: Igor Galligo
        guests: 
          - Franck Rebillard
          - Michel Bauwens
          - Théophile Kouamouo
        start_time: 9:30
        end_time: 11:00
        debate: 
          start_time: 10:30
          duration: 30

      - session: Contribution Philosophy and Information Design
        moderator: Igor Galligo
        guests: 
          - Sylvia Fredriksson
          - Samuel Huron
          - Michelle Christensen
          - Florian Conradi
        start_time: 11:05
        end_time: 12:45
        debate: 
          start_time: 12:15
          duration: 30

      - break: true
        start_time: 12:45
        end_time: 14:15

      - session: Comment acquérir une souveraineté numérique sans perdre une puissance médiatique?
        moderator: 
          - Cemil Sanli
          - Edouard Bouté
        guests: 
          - Octopuce
          - Assemblée Virtuelle
          - French Data Network
          - Yves Citton
          - Chloé Gence
        start_time: 14:25
        end_time: 15:55
        debate: 
          start_time: 15:25
          duration: 30

      - session: How to acquire digital sovereignty without losing media power?
        moderator: 
        - Zakaria Bendali
        - Édouard Bouté
        guests: 
          - La Mule
          - Le Canard Réfractaire
          - Dany Caligula
          - Canal Concorde
          - Cemil Sanli
          - Ulrike Lune Riboni
        start_time: 16:00
        end_time: 17:45
        debate: 
          start_time: 17:15
          duration: 30

      - break: true
        start_time: 17:45
        end_time: 18:00

      - session: Workshop with the public 3
        format: Workshop
        start_time: 18:00
        end_time: 19:30



legals: "This project has received funding from the MSCA-RISE program under grant agreement No 101007915"

---

# Présentation

Comme vous le savez, nous n’avons pas tous le même accès aux médias. Certaines classes sociales et opinions sont moins privilégiées que d’autres, voire invisibilisées, et cela nous le remarquons chaque jour sur les plateaux télé et dans la presse. Une majorité mise sous silence, qui s’est manifestée autrement avec le mouvement des Gilets Jaunes, et notamment grâce à un nouveau phénomène socio-technique : l’AUTOMEDIA&thinsp;!

L’AUTOMEDIA est né de l’association de la fonction vidéo des smartphones et de la fonction partage des réseaux et applications numériques. Il ou elle est un.e citoyen.ne, militant.e ou simple amateur.rice, qui décide par lui-même ou elle-même, de contribuer à la production et à la diffusion d’une information d’intérêt public par des moyens de captation, d’enregistrement et de communication numériques.
En échangeant collectivement des images, des paroles, ou des histoires, l’automedia contribue aussi à la constitution de valeurs, de rêves et de combats communs, qui font émerger de nouvelles communautés politiques. 

En France, le mouvement des Gilets Jaunes s’est propagé à une vitesse extraordinaire en s’appropriant des technologies automédiatiques. Il a pu tisser un réseau médiatique alternatif face aux chaînes d’information dominantes, qui lui a conféré une puissance de socialisation sans précédent depuis 50 ans.

Mais dans leur écrasante majorité, les automédias n’existent aujourd’hui qu’à travers les réseaux sociaux et supports d’un capitalisme numérique. Facebook, Twitter, YouTube, Instagram, TikTok, et d’autres, sont des entreprises privées qui répondent aux valeurs et aux logiques d’un modèle économique aliénant et discriminant. L’opacité des algorithmes de recommandation, les contraintes en matière de publication, la censure ou l'autocensure, la suppression arbitraire de certains groupes, l’existence souterraine d’une économie des médias consumériste et productiviste fondée sur le narcissisme, la publicité, le clash, l’émotion, la pulsion, etc. limitent aujourd’hui les ambitions politiques des automédias. Ceux-ci sont en réalité conditionnés par des designs numériques qui capitalisent sur les expressions de leurs excitations et de leurs effets sociaux, positifs ou négatifs.

Dans ce contexte, il nous apparaît aujourd’hui indispensable, non seulement de faire reconnaître l’avant-garde automédiatique de cette nouvelle démocratie numérique en train de naître, mais aussi d’inventer de nouvelles infrastructures numériques de l’information qui permettront à leurs usagers d’acquérir une souveraineté sur leurs outils de médiation, ainsi qu’une autonomie (plus qu’une indépendance) sur les valeurs qui sous-tendent les normes et les protocoles de production de leurs informations. Il faut donc que les automédias entrent dans une nouvelle phase d’auto-conception de leurs médias afin de produire des outils cohérents avec les horizons politiques des valeurs et des idéaux qu’ils défendent ou promeuvent. 
C’est là le sens du terme «&#8239;automédia&#8239;» et c’est le double enjeu de notre évènement. 

Les 22, 23 et 24 juin 2022, nous vous invitons à venir écouter et discuter avec des automédias, des journalistes, des développeurs informatiques et des chercheurs pour participer à une réflexion sur les fondements économiques, politiques et technologiques d’un nouveau système contributif automédiatique


Cet événement gratuit de trois jours aura lieu à La Plaine St-Denis, dans l’auditorium de la Maison des Sciences de l’Homme Paris-Nord, tout près du métro Front Populaire (Terminus ligne 12).


Il sera aussi retransmis en direct par la chaîne Le Media, partenaire de notre événement.

Pour participer, venez nombreux&thinsp;!
