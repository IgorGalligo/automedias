---
# La mise à jour des membres se fait directement depuis le fichier _data/people_database.yaml

title: Les membres
layout: member_list
type: members
permalink: /{{ locale }}/members/
locale: fr
eleventyExcludeFromCollections: true
---

L'organisation Automédias rassemble des chercheurs de différentes disciplines scientifiques, des citoyens, des média-activistes, des designers et des ingénieurs numériques.