---
# Updating members is done directly from the file _data/people_database.yaml

title: Members
layout: member_list
type: members
permalink: /{{ locale }}/members/
locale: en
eleventyExcludeFromCollections: true
---

The Automedia organization brings together researchers from different scientific disciplines, citizens, media-activists, designers and digital engineers.