---
title: The reception of automedia by the Yellow Vests
subtitle: Between empowerment of practices and denunciation of hegemonies
type: article
locale: en
draft: false
author: Raphaël Lupovici

description: 
date: 2022-06-08

linked_publications:
  - 001
flag:

bibliography:
  - BOCK Mary Angela, 2016, «&#8239;Film the Police\! Cop-Watching and Its Embodied Narratives&#8239;», *Journal of Communication*, vol.&#8239;66, n<sup>o</sup>&#8239;1, p. 13‑34.

  - BOCZKOWSKI Pablo J, MITCHELSTEIN Eugenia et MATASSI Mora, 2018, «&#8239;“News comes across when I’m in a moment of leisure”&thinsp;&#58; Understanding the practices of incidental news consumption on social media&#8239;», *New Media & Society*, vol.&#8239;20, n<sup>o</sup>&#8239;10, p. 3523‑3539.

  - BOURDIEU Pierre, 1996, *Sur la télévision*, Paris, Raisons d’agir (coll.&#8239;«&#8239;Raisons d’agir&#8239;»), 95&#8239;p.

  - BOUTÉ Édouard, 2021, «&#8239;La mise en visibilité des forces de l’ordre sur Twitter pendant le mouvement des Gilets jaunes&#8239;», *Questions de communication*, vol.&#8239;1, n<sup>o</sup>&#8239;39, p. 185‑208.

  - BOUTÉ Édouard et MABI Clément, 2020, «&#8239;Des images en débat &thinsp;&#58; de la blessure de Geneviève

  - Legay à la répression des Gilets jaunes&#8239;», *Études de communication*, vol.&#8239;1, n<sup>o</sup>&#8239;54, p. 29‑52.

  - BOYER Pierre C., DELEMOTTE Thomas, GAUTHIER Germain, ROLLET Vincent et SCHMUTZ Benoît, 2020, «&#8239;Les déterminants de la mobilisation des Gilets jaunes&#8239;», *Revue économique*, vol.&#8239;71, n<sup>o</sup>&#8239;1, p. 109‑138.

  - CARDON Dominique et GRANJON Fabien, 2013, *Médiactivistes*, 2<sup>e</sup> éd., Paris, Presses de Sciences Po

  - (coll.&#8239;«&#8239;Contester&#8239;»), 200&#8239;p.

  - CHAMPAGNE Patrick, 1984, «&#8239;La manifestation. La production de l’événement politique&#8239;», *Actes de la recherche en sciences sociales*, n<sup>o</sup>&#8239;52, p. 19‑41.

  - Collectif d’enquête sur les Gilets JAUNES, 2019, «&#8239;Enquêter in situ par questionnaire sur une mobilisation. Une étude sur les Gilets jaunes&#8239;», *Revue française de science politique*, vol.&#8239;69, n<sup>o</sup>&#8239;5‑6, p. 869‑892.

  - DAHLBERG Lincoln, 2007, «&#8239;The Internet and Discursive Exclusion&thinsp;&#58; From Deliberative to Agonistic Public Sphere Theory&#8239;» dans Lincoln Dahlberg et Eugenia Siapera&#8239;(eds.), *Radical Democracy and the Internet. Interrogating Theory and Practice*, Londres, Palgrave Macmillan, p.&#8239;128‑147.

  - DAHLGREN Peter, 2009, *Media and political engagement. Citizens, communication, and democracy*, Cambridge, Cambridge University Press (coll.&#8239;«&#8239;Communication, Society and Politics&#8239;»), 246&#8239;p.

  - DAHLGREN Peter, 2003, «&#8239;Reconfigurer la culture civique dans un milieu médiatique en évolution&#8239;», *Questions de communication*, traduit par Catherine Loneux, n<sup>o</sup>&#8239;3, p. 151‑168.

  - Della PORTA Donatella et MOSCA Lorenzo, 2005, «&#8239;Global-net for Global Movements? A Network of Networks for a Movement of Movements&#8239;», *Journal of Public Policy*, vol.&#8239;25, n<sup>o</sup>&#8239;1, p. 165‑190.

  - FERRON Benjamin, 2016, «&#8239;Professionnaliser les “médias alternatifs” ? Enjeux sociaux et politiques d’une mobilisation (1999-2016)&#8239;», *Savoir/Agir*, vol.&#8239;4, n<sup>o</sup>&#8239;38, p. 21‑28.

  - FRASER Nancy, 1993, «&#8239;Rethinking the Public Sphere&thinsp;&#58; A Contribution to the Critique of Actually Existing Democracy&#8239;» dans Craig Calhoun&#8239;(ed.), *Habermas and the Public Sphere*, Cambridge, Mass., The MIT Press, p.&#8239;109‑142.

  - GRANJON Fabien, 2022, *Classes populaires et usages de l’informatique connectée. Des inégalités sociales-numériques*, Paris, Presses des Mines, 360&#8239;p.

  - GRANJON Fabien, 2020, «&#8239;Médias&#8239;» dans Olivier Fillieule, Lilian Mathieu et Cécile Péchu&#8239;(eds.), *Dictionnaire des mouvements sociaux*, 2<sup>e</sup> éd., Paris, Presses de Sciences Po (coll.&#8239;«&#8239;Références&#8239;»), p.&#8239;378‑385.

  - GUNTHERT André, 2020, «&#8239;L’image virale comme preuve. Les vidéos des violences policières dans la crise des Gilets jaunes&#8239;», *Communications*, vol.&#8239;1, n<sup>o</sup>&#8239;106, p. 187‑207.

  - HERMIDA Alberto et Hernández-SANTAOLALLA Víctor, 2018, «&#8239;Twitter and video activism as tools for counter-surveillance&thinsp;&#58; the case of social protests in Spain&#8239;», *Information, Communication & Society*, vol.&#8239;21, n<sup>o</sup>&#8239;3, p. 416‑433.

  - JEANPIERRE Laurent, 2019, *In Girum. Les leçons politiques des ronds-points*, Paris, La Découverte (coll.&#8239;«&#8239;Cahiers libres&#8239;»), 192&#8239;p.

  - KIDD Dorothy, 2021, «&#8239;Indymedia and Standing Rock&thinsp;&#58; Media-historic moments&#8239;», *Journal of Alternative & Community Media*, vol.&#8239;6, n<sup>o</sup>&#8239;1, p. 9‑27.

  - Le BART Christian, 2020, *Petite sociologie des Gilets jaunes. La contestation en mode post-institutionnel*, Rennes, Presses universitaires de Rennes, 212&#8239;p.

  - LEFEBVRE Rémi, 2019, «&#8239;Les Gilets jaunes et les exigences de la représentation politique&#8239;», *La Vie des idées*, \[En ligne\].

  - LIEVROUW Leah A., 2011, *Alternative and Activist New Media*, Cambridge, Polity (coll.&#8239;«&#8239;Digital media and society series&#8239;»), 294&#8239;p.

  - LOU Chen, Tandoc JR. Edson C., HONG Li Xuan, PONG Xiang Yuan (Brenda), LYE Wan Xin (Rachelle) et SNG Ngiag Gya (Trisha), 2021, «&#8239;When Motivations Meet Affordances&thinsp;&#58; News Consumption on Telegram&#8239;», *Journalism Studies*, vol.&#8239;22, n<sup>o</sup>&#8239;7, p. 934‑952.

  - MABI Clément et THEVIOT Anaïs, 2014, «&#8239;S’engager sur Internet. Mobilisations et pratiques politiques&#8239;», *Politiques de communication*, vol.&#8239;2, n<sup>o</sup>&#8239;3, p. 5‑24.

  - MOUALEK Jérémie, 2022, «&#8239;L’image disqualifiante de la “violence populaire” en démocratie. Le cas des Gilets jaunes et de leurs “clichés”&#8239;», *Socio. La nouvelle revue des sciences sociales*, n<sup>o</sup>&#8239;16, p. 139‑158.

  - NEVEU Érik, 2019, *Sociologie des mouvements sociaux*, 7<sup>e</sup> éd., Paris, La Découverte (coll.&#8239;«&#8239;Repères&#8239;»), 128&#8239;p.

  - OSER Jennifer, HOOGHE Marc et MARIEN Sofie, 2013, «&#8239;Is Online Participation Distinct from Offline Participation? A Latent Class Analysis of Participation Types and Their Stratification&#8239;», *Political Research Quarterly*, vol.&#8239;66, n<sup>o</sup>&#8239;1, p. 91‑101.

  - OUARDI Samira, 2010, «&#8239;La critique des médias à l’ère de leur industrialisation. Contours d’une problématique et traces d’une tradition&#8239;», *Mouvements*, vol.&#8239;1, n<sup>o</sup>&#8239;61, p. 11‑22.

  - PASQUIER Dominique, 2018, *L’Internet des familles modestes. Enquête dans la France rurale*, Paris, Presses des Mines (coll.&#8239;«&#8239;Sciences sociales&#8239;»), 220&#8239;p.

  - SEBBAH Brigitte, LOUBÈRE Lucie, SOUILLARD Natacha, Thiong-KAY Laurent et SMYRNAIOS Nikos, 2018, *Les Gilets jaunes se font une place dans les médias et l’agenda politique*, Toulouse,

  - LERASS - Université de Toulouse.

  - SIROUX Jean-Louis, 2020, *Qu’ils se servent de leurs armes. Le traitement médiatique des Gilets jaunes*, Vulaines-sur-Seine, Éditions du Croquant, 164&#8239;p.

  - SOUILLARD Natacha, SEBBAH Brigitte, LOUBÈRE Lucie, Thiong-KAY Laurent et SMYRNAIOS Nikos, 2020, «&#8239;Les Gilets jaunes, étude d’un mouvement social au prisme de ses arènes médiatiques&#8239;», *Terminal*, n<sup>o</sup>&#8239;127, p. \[En ligne\].

  - Thiong-KAY Laurent, 2020, «&#8239;L’automédia, objet de luttes symboliques et figure controversée. Le cas de la médiatisation de la lutte contre le barrage de Sivens (2012-2015)&#8239;», *Le Temps des médias*, vol.&#8239;2, n<sup>o</sup>&#8239;35, p. 105‑120.


---
Le mouvement des Gilets jaunes s’est illustré par son rapport conflictuel aux institutions (Le Bart 2020), parmi lesquelles les médias dominants (Sebbah et al. 2018), l’inscrivant du coup dans la tradition du médiactivisme (Cardon et Granjon 2013). Les Gilets jaunes ont en effet investi ce terrain de lutte aussi bien de manière *contre-hégémonique* en dénonçant l’activité des médias dominants, qu’*expressiviste* par une autonomisation de ses pratiques médiatiques, notamment sous la forme d’automédias (Thiong-Kay 2020). C’est ainsi que de nombreuses contributions de formes variées se sont développées sur les réseaux socionumériques (RSN) allant du témoignage individuel via l’utilisation de smartphones jusqu’à la constitution de collectifs tendant à une professionnalisation accrue de leur activité (Ferron 2016). C’est surtout sur ces plateformes que ces initiatives se sont manifestées et ont rencontré leurs publics&#8239;: ce sont ces derniers que notre recherche vise à mieux connaître.

Notre entrée se fera à travers leur réception des automédias, et la place qu’ils prennent à la fois dans leur engagement citoyen et dans leurs manières de s’informer. Nous cherchons à comprendre comment les Gilets jaunes ont pu se retrouver dans ces productions en nous intéressant à la contribution des contenus partagés et des réseaux socionumériques (RSN) à différentes dimensions de la citoyenneté. En mobilisant les travaux de Peter Dahlgren (2009) nous sommes en mesure d’identifier six dimensions de l’agentivité civique promues dans l’univers automédiatique des Gilets jaunes&#8239;: les connaissances, les valeurs, les pratiques, la confiance, les espaces et les identités.

Nous nous appuyons sur des entretiens menés dans le cadre d’un travail de thèse en cours où nous avons interrogé des participants au mouvement sur leurs pratiques médiatiques et leur rapport aux médias et à la politique. Ces entretiens ont été menés dans sept régions[^1] et révèlent des profils plutôt âges (52 ans d’âge en moyenne), ainsi qu’une diversité des parcours socio-professionnels, où se retrouvent aussi bien une précarité financière qu’un niveau de vie relativement élevé, conforme aux observations soulignant le caractère bigarré du mouvement (Collectif d’enquête sur les Gilets jaunes 2019). Cet article propose ainsi une réflexion sur les conditions sous lesquelles un public contestataire peut s’appuyer sur des initiatives médiatiques autonomes pour s’engager politiquement.

#  Les automédias dans l’environnement médiatique des Gilets jaunes

La place qu’occupent les automédias dans les habitudes informationnelles des Gilets jaunes va dépendre de l’environnement médiatique dans lequel ceux-ci évoluent. Notre enquête révèle une large variété de titres consultés sur des supports différents. Quoique certains continuent à lire la presse papier, quasiment tous les enquêtés s’informent désormais en ligne, que ce soit sur leur ordinateur ou sur leur smartphone. Les médias dominants ne sont pas absents des habitudes informationnelles des Gilets jaunes. Mais leur consultation se fait sous des modalités généralement critiques, comme dans le cas de Patrick&#8239;qui regarde LCI dans un souci de surveillance de leurs propos&#8239;:

> «&#8239;Oui je regarde beaucoup plus LCI \[…\] c’est depuis les Gilets jaunes ils ont tellement menti, tellement raconté des choses, que j’ai dit&#8239;: «&#8239;on va voir ce qu’ils vont nous mettre&#8239;» et je me suis habitué aux chroniqueurs qui y étaient. Et je suis certains, je regarde leurs points de vue mais toujours en contre balance vu qu’on a un point de vue qui est plus objectif sur les Gilets jaunes, eux ils font une généralité.&#8239;» <figcaption>Patrick, 60 ans, chauffagiste, Montescou (Pyrénées Orientales)</figcaption>

Concernant la presse, il est intéressant de noter que les journaux consultés au format papier appartiennent fréquemment à la Presse quotidienne régionale (PQR) plutôt qu’aux grands titres nationaux tels que *Le Monde* ou *Libération*. Quand des articles issus de la presse nationale parviennent sous les yeux des Gilets jaunes, c’est surtout le fait d’un partage au sein des espaces d’échanges que sont les différents groupes Facebook ou fils WhatsApp et Telegram auxquels ils sont abonnés. Si la PQR bénéficie de sa proximité avec ses lecteurs et de son ancrage territorial, elle n’est nullement épargnée par la défiance des Gilets jaunes envers les médias&#8239;:

  > **Réponse**&#8239;: c’est pas parce que je le lis \[Le Télégramme\] que j’adhère à ce qu’ils me disent&#8239;!

  > **Question**&#8239;: qu’est-ce que tu n’aimes pas&#8239;?

  > **Réponse**&#8239;: je n’aime pas parce que ça m’est arrivé de témoigner, et sur mon témoignage qui faisait peut-être 5 lignes il en est ressorti une demi-ligne qui était horrible.

  > **Question**&#8239;: quand est-ce que c’était&#8239;?

  > **Réponse**&#8239;: c’était dans le cadre des Gilets jaunes.&#8239;»

  >   <figcaption>Jacqueline, 64 ans, infirmière, Lannilis (Finistère)</figcaption>

Cette expérience d’un décalage entre le vécu et le discours médiatique s’avère être plus généralement le vecteur de la critique des médias des Gilets jaunes, qui vont s’appuyer sur cet écart pour ensuite s’engager dans une dénonciation du système médiatique. C’est donc un public défiant envers les médias que les automédias ont rencontré et qui, comme on le verra ensuite, ont pu alimenter cette critique.

On trouve d’autre part des médias alternatifs et engagés, avant tout à gauche, comme Le Média, Le Monde diplomatique, Basta&#8239;! ou Là-bas si j’y suis. Ces productions se situent à mi-chemin entre les médias traditionnels et les automédias. Des premiers, ces médias conservent une tendance à la professionnalisation de leur activité qui va se structurer en fonction des moyens dont ils disposent, de leur ancienneté et de l’adoption de pratiques professionnelles similaires aux médias dominants (Ferron 2016). Des seconds, ils conserveront un engagement politique assumé et une activité critique de dénonciation des hégémonies (Cardon et Granjon 2013). Ces productions se révèlent différemment populaires selon le profil sociologique des enquêtés. Le Média TV fait quasiment l’unanimité auprès des personnes rencontrées tandis que d’autres comme Le Monde diplomatique seront davantage appréciées par des personnes ayant fait des études supérieures ou ayant été politisées plus tôt. Certains sont par ailleurs abonnés à ces médias en ligne, signe d’un intérêt notable&#8239;:

> «&#8239;C’est parce que j’achète plus de papier j’ai réservé un budget pour les médias indépendants. Donc Blast, Le Média, QG, Arrêt sur images, Là-bas si j’y suis, je vais m’abonner à Hors-Série où il y a des longs entretiens avec des intellectuels.&#8239;»<figcaption>Thomas, 39 ans, illustrateur, Brest (Finistère)</figcaption>

Cet écosystème médiatique se distingue donc par des contenus jugés plus amples et plus fouillés que les médias traditionnels, ce qui peut renseigner sur les normes implicites de qualité journalistique en vigueur au sein du mouvement des Gilets jaunes.

Enfin, les automédias sont effectivement bien présents dans cet environnement médiatique. Les plus cités sont Cerveaux non disponibles, Vécu, Le Canard réfractaire ou Civicio qui reviennent très fréquemment durant les entretiens, conformément à leurs audiences plus importantes que d’autres initiatives de taille plus modeste. Ces automédias ont en commun avec les médias alternatifs leur point de vue engagé et critique, mais s’inscrivent davantage dans une logique d’accompagnement d’un mouvement social, celui des Gilets jaunes ayant été à l’origine de leur création. Plus généralement, l’automédiatisation prise au sens large se retrouve fréquemment dans les contenus publiés sur Facebook, sous la forme d’images capturées directement par des manifestants qui circuleront de partage en partage dans les différents espaces d’expression du mouvement (Bouté et Mabi 2020).

La diversité des canaux de diffusion mérite également d’être notée. Si Facebook a fait partie des réseaux emblématiques du mouvement (Jeanpierre 2019; Boyer et al. 2020), les Gilets jaunes utilisent également des applications de discussion telles que WhatsApp, Signal ou Telegram dans des groupes qui leur permettent aussi bien de se coordonner que de partager des informations. Cette dernière offre la possibilité d’avoir accès à des fils de discussions publics qui vont pleinement fonctionner comme des automédias puisqu’ils sont animés par une personne ou un groupe particulier qui peuvent ainsi fixer une politique éditoriale tout en répondant à un besoin d’information, d’efficacité et de socialisation (Lou et al. 2021). Ces comptes semblent avoir profité des mesures prises par Facebook pour lutter contre les fausses informations lors de la pandémie de COVID-19 pour contourner ces restrictions et fidéliser leur public.

Enfin, et en continuité avec notre observation sur la PQR, la dimension locale de l’environnement médiatique des Gilets jaunes dépasse le cadre des médias traditionnels. C’est ainsi que les habitudes médiatiques se ressemblent à l’intérieur des petits groupes que nous avons pu rencontrer. À titre d’exemple, les Gilets jaunes de Haute-Savoie que nous avons rencontrés étaient pour beaucoup lecteurs du journal *Informations ouvrières*, proche de la Quatrième Internationale, et qui s’est diffusé sous l’impulsion d’un des membres qui y milite de longue date.

Deux cas de figure se présentent dans l’introduction des enquêtés aux automédias s’étant développés autour du mouvement des Gilets jaunes. Une partie des Gilets jaunes a découvert l’univers des médias alternatifs avec le mouvement tandis que l’autre était déjà familière de ce genre de productions. Concernant la première catégorie, la découverte des automédias a pu se produire de manière concomitante à la politisation déclenchée par leurs premières manifestations. Comme cela avait été noté au début du mouvement, les premiers rassemblements comptaient une moitié de primo-manifestants (Collectif d’enquête sur les Gilets jaunes 2019) qui ont donc pu découvrir les différents répertoires d’action politique, parmi lesquels le médiactivisme, dimension récurrente des mouvements sociaux contemporains. Le besoin de s’informer va donc prendre de l’importance chez certains Gilets jaunes qui se mettent à fréquenter les RSN de manière plus suivie, comme dans le cas de Béatrice qui a commencé à s’informer à partir du mouvement&#8239;:

> «&#8239;je n’étais pas trop sur FB, sur les réseaux à l’époque je n’avais pas tout ça, je connaissais pas WhatsApp, il y avait que FB où il y avait ma sœur \[…\] C’est au moment des Gilets jaunes en fait, il y a un certain Éric Drouet, ça passe sur notre profil, mon mari le voit aussi, ça passe sur le réseau et on se dit «&#8239;c’est quoi cette histoire&#8239;», on écoute et on se dit «&#8239;oh lala&#8239;», on s’est dit «&#8239;ça y’est c’est le moment, enfin, on va changer le monde&#8239;!&#8239;», enfin bref, on est sortis le 17 novembre et voilà. Après on a commencé à vraiment suivre les réseaux, on a partagé, repartagé, on s’est invectivés.&#8239;»<figcaption>Béatrice, 39 ans, commerçante, L’Aigle (Orne)</figcaption>

On peut supposer que l’entrée dans des espaces numériques propres au mouvement va les exposer à la circulation de publications issues d’automédias et ainsi leur en faire prendre connaissance.

Une autre partie des enquêtés était déjà familière des médias alternatifs, qu’elle a commencé à consulter généralement bien avant le mouvement des Gilets jaunes, et concerne des profils politisés de plus longue date. Par exemple, Christophe a bénéficié de son adoption précoce d’internet pour consulter des informations alternatives dès la fin des années 1990&#8239;:

> «&#8239;Du fait que j’ai été très rapidement sur Internet. En 96-97, j’ai eu accès très vite à toutes sortes d’informations, quelques fois loufoques, quelquefois très intéressantes, et le monde était beaucoup plus resserré à l’époque. Il y avait beaucoup moins de distractions et il y avait pas mal de personnes qui essayaient de passer des documents, des informations, etc. Et j’étais tombé sur des document un peu spéciaux, qui parlaient du groupe de Bilderberg et j’étais au courant parmi les premiers en France de l’existence de ce truc là et alors c’était mélangé avec tout un fatras de choses dignes d’un X-files. Démêler le vrai du faux, c’était compliqué, mais ça, ça avait l’air... les documents que j’avais étaient assez crédibles et du coup, je me suis dit : «&#8239;tiens, je vais rechercher&#8239;» du coup j’ai mené des recherches dans mon coin, à temps perdu&#8239;»  <figcaption>Christophe, 45 ans, vidéaste, Montpellier (Occitanie)</figcaption>

L’habitude à consulter des productions médiatiques alternatives concerne généralement les profils ayant fait davantage d’études supérieures et surtout politisés depuis plus longtemps. Christophe m’a par exemple cité Indymedia qui fut un des acteurs centraux de l’écosystème médiatique du mouvement altermondialiste (Kidd 2021). À la fin des années 2000, les mouvements sociaux se tournèrent vers les RSN pour assurer le support informationnel de leur activité ce qui a entrainé le déclin des production telles qu’Indymedia (Lievrouw 2011). Ce basculement leur permit ainsi de toucher un public bien plus large, en profitant du caractère grand public de ces plateformes, qui sont maintenant populaires jusque dans les couches paupérisées de la France rurale (Pasquier 2018). Le mouvement des Gilets jaunes a donc créé des espaces numériques d’échanges au sein desquels des individus issus d’horizons politiques multiples vont pouvoir se rencontrer.

Le succès de médias telles que Cerveaux non disponibles ou Vécu va donc reposer sur leur capacité à proposer des contenus en mesure de satisfaire les attentes d’un public finalement diversifié, en les rassemblant autour de valeurs communes. En effet, le rôle des médias alternatifs pour les mouvements sociaux ne saurait se limiter à leur fonction logistique mais concerne également leur capacité de construction de l’identité militante (Della Porta et Mosca 2005). C’est ainsi que le contenu éditorial des automédias doit promouvoir l’engagement citoyen en rassemblant autour de valeurs communes, c’est-à-dire en offrant le «&#8239;sentiment *a minima* de posséder quelque chose en commun \[…\] le sentiment d’une appartenance à une même entité sociale et politique, en dépit des différences&#8239;» (Dahlgren 2003, p.&#8239;157).

# Les automédias, ressort de l’engagement politique&#8239;?

Si les enquêtés font part d’un rejet prononcé des médias dominants, c’est avant tout car ces derniers ne parviennent pas à se conformer à leurs attentes, comme dans le cas du mouvement des Gilets jaunes qui s’est heurté à une couverture médiatique adverse, en particulier à partir de décembre quand les revendications initiales commencèrent à laisser place à une montée en généralité de leur discours politique (Siroux 2020). Au fil des actes, les dégradations lors des manifestations vont devenir l’un des angles récurrents du traitement médiatique (Moualek 2022), au grand dam de Denise par exemple, qui dénonce cette focale&#8239;:

> «&#8239;Je qualifierais d’odieux, de détestable, de gros menteurs mais ce qui s’est passé c’est ignoble parce que toutes ces personnes avec des mains arrachées, ces personnes éborgnées, les coups de matraques qu’on a vu sur place et qu’après on discrédite les manifestants en disant «&#8239;voilà la violence des manifestants&#8239;». Quand ils filment vous voyez l’angle d’où ils filment une poubelle brulée vous en faites tout un cinéma, ça dépend de l’angle d’où vous filmez. Et tout ça, c’est déformé que ce soit Le Dauphiné, Le Messager il s’est passé quelque chose au niveau des manifestations, quand ils ont retranscrit c’était pas du tout la vérité. Depuis je n’ai plus aucune confiance.&#8239;»<figcaption>Denise, 49 ans, employée administrative en reconversion, Thonon-les-Bains (Haute-Savoie)</figcaption>

Ce propos condense l’impératif pour les mouvements sociaux de garder la main sur leur couverture médiatique (Neveu 2019; Granjon 2020), qui demeure «&#8239;un point de passage obligé pour être \[perçu\] par le champ politique&#8239;» (Champagne 1984, p.&#8239;28), à plus forte raison pour les Gilets jaunes qui ne peuvent a priori s’appuyer sur des relais institutionnels comme savent le faire les syndicats (Le Bart 2020). Cette vulnérabilité symbolique va conduire le mouvement a investir les deux volets du médiactivisme que sont la dénonciation contre-hégémonique et l’autonomisation expressiviste (Cardon et Granjon 2013). La première configuration va consister à formuler une critique de l’activité médiatique tandis que la seconde va promouvoir la production indépendante de l’information. Ces deux approches se sont complétées dans le travail des automédias, ce qui leur a permis de trouver leur public.

Dans un premier temps, les Gilets jaunes ont pu trouver chez les automédias une dénonciation des médias dominants en continuité avec la leur. Cette dénonciation peut se jouer aussi bien sur le mode explicite qu’implicite. Dans le premier cas, les automédias peuvent effectivement tenir un discours qui va rappeler la critique des médias qui s’était développée dans le sillage des manifestations de 1995 avec la création de l’observatoire Acrimed ou les interventions de Pierre Bourdieu (1996), soit une dénonciation des mécanismes économiques, institutionnels ou symboliques de la production de l’information (Ouardi 2010). On voit par exemple dans cette publication parue sur la page du média Vécu l’utilisation d’une cartographie de l’actionnariat médiatique établie par deux acteurs historiques de la critique des médias en France, Acrimed et Le Monde diplomatique.

![Cartographie des Media français](/../../assets/media/publications/005-media_francais.webp)

Cette thématique de la connivence entre médias et pouvoirs économiques revient fréquemment dans les propos des Gilets jaunes, et les automédias ont pu contribuer à cette prise de conscience, comme dans le cas de Karine, à qui je demande comment elle s’est rendue compte des liens entre les journalistes et les pouvoirs politiques et économiques&#8239;:

> «&#8239;Avec les *Gilets jaunes constituants*, etc. qui eux faisaient un travail journalistique aussi, c’est-à-dire qu’ils \[…\] récupéraient des sources, et ils disaient «&#8239;allez là, vous allez lire ça, il y a ça, il y a ça&#8239;». Il y a le «&#8239;Diner du Siècle&#8239;» où ils vont souvent devant, c’est des choses où c’est pas dit, pourquoi c’est pas dit dans les médias&#8239;? on pourrait être informés de ça en tant que citoyens, on nous dit pas certaines choses et on enflamme d’autres choses dont on se fout complètement.&#8239;»<figcaption>Karine, 40 ans, photographe en reconversion, Saint-Maur-des-Fossés (Val-de-Marne)</figcaption>

Ce travail qui rapproche la vision journalistique de ces automédias de celle du journalisme d’investigation va leur permettre de se distinguer des reportages des chaînes d’information en continu qui sont les plus décriés par les enquêtés. Elle rejoint ainsi l’ambition de dévoilement de la critique traditionnelle des médias, en la rendant accessibles à public populaire&#8239;:

> «&#8239;C’est une information qui me parait juste, qui est faite pour les gens, pour les ouvriers, on ne leur ment pas, les choses ne sont pas détournées. Au contraire, elles sont révélées, ce qui dans d’autres médias va être caché, ou au moins un peu détourné ou minimisé, pour ne pas éveiller les consciences, ou même anesthésier les consciences&#8239;!&#8239;»<figcaption>Nadine, 62 ans, employée administrative, Scionzier (Haute-Savoie)</figcaption>

On voit ainsi comment l’automédia, par sa proximité avec un mouvement social et son autonomie vis-à-vis de référents traditionnels désavoués, va nouer un lien de confiance avec son public. Cette confiance dans des médias propres au mouvement favorisera une culture commune qui servira de ressource pour les Gilets jaunes afin de créer du lien. Non seulement l’activité se trouve en conformité avec les attentes envers l’information, mais la révélation se faisant aux dépens d’institutions rejetées va amplifier la résonance entre cette culture et les automédias.

Néanmoins, la dénonciation explicite des institutions dominantes est associée à une dénonciation implicite mettant en échec le discours médiatique par la circulation d’une contre-information de celles des grands médias. Par exemple, Ludo me raconte comment les images de blessés lors des manifestations, qui ont massivement été diffusées sur les RSN, ont suscité chez lui l’indignation tant pour leur contenu très violent, que pour leur absence prolongée dans la couverture médiatique&#8239;:

> «&#8239;Depuis le 1<sup>er</sup> décembre \[2018, NDLR\], tous les jours, je faisais le tour des médias à la télé donc LCI, BFM, CNEWS, principalement ces médias, et puis France 2 et TF1 que j’ai fait un petit peu. Et tous les jours j’étais choqué parce qu’on parlait pas de violences policières. On voyait des choses hallucinantes sur internet, sur Facebook on voyait tout ce qui se passait en violences policières à ce moment-là, et pas un mot des médias sur ce qui se passait et ça, ça a duré 2 mois.&#8239;»<figcaption>Ludo, 46 ans, maître-nageur, Ivry-sur-Seine (Val-de-Marne)</figcaption>

Au début du mouvement, celui-ci s’astreignait donc à une veille médiatique quotidienne lors de laquelle il pouvait constater l’écart entre les images circulant dans les espaces numériques des Gilets jaunes et ce qui pouvait se dire à la télévision. En effet, les RSN ont servi à remettre en question les cadrages médiatiques officiels en permettant le partage d’images contradictoires, comme cela a pu s’observer à propos de la question des violences policières lors du mouvement (Gunthert 2020; Bouté et Mabi 2020). En s’impliquant dans cette lutte sur le cadrage médiatique, les automédias ont donc gagné la confiance des Gilets jaunes, puisqu’ils ont prolongé en ligne leur vécu des manifestations hors-ligne.

Au-delà de la critique contre-hégémonique, la dimension expressiviste de ces productions médiatiques déborde le rôle de dénonciation des médias dominants. Cette autonomisation a également permis d’offrir des espaces de circulation des discours qui ont permis une liberté dans la prise de parole&#8239;: «&#8239;les dispositifs GJ de publication en ligne, grâce à leur grande diversité, font, *volens nolens*, exister un modèle de communication plus démocratique, en phase avec les objectifs d’émancipation sociale et de réappropriation, par les acteurs du mouvement, des outils de représentation&#8239;» (Granjon 2022, p.&#8239;289). Or, l’existence d’espaces d’expression consacrés aux publics subalternes en marge de l’espace public traditionnel fait partie des conditions indispensables à l’engagement politique de ces derniers (Fraser 1993), il n’est dès lors nullement étonnant que le mouvement des Gilets jaunes, qui nourrit un sentiment d’exclusion vis-à-vis de son manque de représentation politique et médiatique, ait investi de tels espaces où il a pu construire son propre discours.

Lors d’un entretien, Antigone me faisait part de son désarroi face à une couverture médiatique qui semblait ne rien entendre aux revendications martelées par le mouvement&#8239;:

> «&#8239;On réclamait, on en voulait trop et on voulait pas travailler, on avait qu’à vendre une voiture si on arrivait pas à payer l’essence. C’était absurde alors que nos revendications c’était toujours les mêmes on avait fait la synthèse de nos revendications&#8239;: justice fiscale, justice sociale et la démocratie directe, le RIC on était tous d’accord là-dessus. Comment au bout de 3 ans on peut toujours répéter la même chose&#8239;?&#8239;»<figcaption>Antigone, 51 ans, ancienne comptable, Nice (Alpes-Maritimes)</figcaption>

C’est en palliant cette absence d’écoute que les RSN ont laissé le mouvement investir son aspiration à la démocratie directe par la discussion et la délibération au sein de cet espace de discussion. Le mouvement a pu y construire son identité politique autour de ses propres revendications notamment la cause du Référendum d’initiative citoyenne (RIC) structurante au sein du mouvement. Comme le notent Souillard et al. (2020)&#8239;: «&#8239;le «&#8239;RIC&#8239;» apparaît dès lors comme une revendication structurante massive et argumentée de souveraineté populaire. Il est pensé, d’une part, comme un dispositif politique permettant une participation plus directe des citoyens à la décision politique \[…\] Ce pôle de revendication atteste ainsi d’un fort attachement à l’imaginaire démocratique.&#8239;» C’est donc un couplage de la théorie (ce que le RIC est censé apporter à la démocratie) à la pratique (la discussion délibérative comme horizon de la participation politique) qui va donner corps à la revendication, éprouvée par l’expérience lors des discussions en ligne.

# Conclusion

Le public des automédias Gilets jaunes se caractérise par une diversité dans ses pratiques médiatiques. On constate premièrement une relation ambivalente aux médias dominants, vacillant entre recul critique et rejet radical, souvent motivé par l’expérience du mouvement. La tradition contre-hégémonique de la critique des médias a ainsi pu se voir investie par un public éloigné de ses premiers avatars qui concernait surtout des «&#8239;professionnels de la critique&#8239;» incarnés par des figures universitaires connue (Pierre Bourdieu, Noam Chomsky). À rebours de ces profils, les automédias Gilets jaunes ont poursuivi cette tradition en se la réappropriant, et en la diffusant dans des espaces éloignés des cercles militants traditionnels. C’est donc bel et bien l’alliage des versants contre-hégémonique et expressiviste qui est à l’œuvre sur les RSN, la question médiatique des mouvements sociaux ayant elle aussi été investie «&#8239;par le bas&#8239;» (Lefebvre 2019) par les Gilets jaunes.

Les médias numériques, dont l’importance pour le mouvement n’est plus à préciser, se sont trouvés utilisés de manière transversale, tant dans les supports que dans les pratiques. Si l’ordinateur n’a pas disparu, la place du smartphone prend une importance décisive tant dans la production que dans la réception de l’information, étapes dont la frontière se révèle de plus en plus ténue. Les médias comme Vécu, qui se sont distingués par leur immersion dans les manifestations afin de couvrir les violences policières appartiennent à une tendance contemporaine du *cop-watching* (Bock 2016; Hermida et Hernández-Santaolalla 2018; Bouté 2021) qui sont entrées en continuité avec l’expérience des Gilets jaunes sur le terrain. Mais les effets de ce type de discours ne se caractérisent pas uniquement par leurs contenus, mais aussi par le mode de consultation qui se fait à rythme individualisé, ce qui s’accompagne d’une distanciation prise vis-à-vis des modes de légitimation médiatiques (Boczkowski, Mitchelstein et Matassi 2018).

De manière plus générale, le public des automédias Gilets jaunes a pu retrouver une conception agonistique de la pratique politique. Mouvement s’étant construit «&#8239;contre&#8239;» une loi, puis un gouvernement, puis même contre un monde (rappelant le slogan lors de Nuit debout&#8239;: «&#8239;contre la Loi travail et son monde&#8239;»), sa dimension agonistique le traverse jusque dans son investissement médiatique. L’existence d’un espace public agonistique (Dahlberg 2007) a pu jouer à plein son rôle de promoteurs de l’engagement civique des Gilets jaunes en contribuant à ses différentes dimensions (Dahlgren 2009). Alors qu’il y a une décennie environ la thèse de la différenciation des effets d’internet sur la participation politique était émise (Oser, Hooghe et Marien 2013; Mabi et Theviot 2014) et semblait exclure les classes populaire, il semble que «&#8239;l’internet des familles modestes&#8239;» (Pasquier 2018) ait pu prendre son essor politique avec le mouvement des Gilets jaunes.