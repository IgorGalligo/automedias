---
title: Pour une fabrique populaire de l’information à l’époque de la post&#8209;vérité
subtitle: n°6 Appel à contribution

flag: "[Cahiers COSTECH ](http://www.costech.utc.fr/CahiersCOSTECH/)"

type: article
locale: fr

author: 
  - Igor Galligo
  - Ludovic Duhem
  - Édouard Bouté
description:
date: 2025-12-13
tags:
  - populaire
  - journalisme
  - automédia
  - post-vérité
  - régime de vérité
  - médiactiviste
  - médiartivisme
  - individuation
  - design participatif/contributif
  - communauté médiatique
  - capitalisme numérique

linked_publications:
  - 002
  - 003
  - 004
  - 005
  - 006

# Bibliographie de l'article
# La bibliographie est une liste. Chaque entrée est introduit par un retrait (deux espaces) et un tiret.
# Il est possible de segmenter la bibliographie par catégorie. Il convient alors de renseigner un titre (- title: MON TITRE). L'ensemble des références placer à la ligne et respectant un retrait supplémentaire (deux espaces) seront inclut dans ce sous-groupe
# Si les références contiennent des doubles points (ou colons), il convient remplacer ce symbole par l'expression &#58; ou de placer la référence entre "guillemet"
bibliography: 
  - title: Sur les automédias, médias populaires, médias indépendants
    references:
      -  Arquembourg, J. (2011). L'événement et les médias. Les récits médiatiques des tsunamis et les débats publics (1755-2004). Paris, Éd. des Archives contemporaines.

      -  Bourdieu, P. (1992(1998). Les règles de l’art. Genèse et structure du champ littéraire. Paris, Ed. du Seuil.

      -  Cardon, D., Granjon, F. (2013). Médiactivistes. Paris, Presses de Sciences Po.

      -  Ferron, B. (2010). Des médias de mouvements aux mouvements de médias&#58; Retour sur la genèse du «&#8239;Réseau Intercontinental de Communication Alternative&#8239;» (1996-1999). Mouvements, 61(1), 107-120. <https://doi.org/10.3917/mouv.061.0107>  

      -  Lévêque, S., Ruellan, D. (2010). Journalistes engagés. Presses universitaires de Rennes, séries «&#8239;Res Publica&#8239;».

      -  Souillard, N., Sebbah, B., Loubère, L., Thiong-Kay, L., Smyrnaios, N. (2020). Les Gilets jaunes, étude d'un mouvement social au prisme de ses arènes médiatiques. Terminal, "Les groupes minoritaires et / ou marginalisés à l'ère numérique &#58; pratiques de mobilisation, changements sociopolitiques et transformations identitaires URL &#58; <http://journals.openedition.org/terminal/5671>&#8239;; DOI &#58; <https://doi.org/10.4000/terminal.5671>  

      -  Thiong-Kay, L. (2020, à paraître). L'automédia, objet de luttes symboliques et figure controversée. Le cas de la médiatisation de la lutte contre le barrage de Sivens (2012-2015)&#8239;», Le Temps des Médias.

      -  Carlino, V., (2020). «&#8239;Vidéo en ligne et contestation politique radicale&#58; Entre intégration aux pratiques militantes et critique des plateformes&#8239;». Terminal, no 127. <https://doi.org/10.4000/terminal.5806>

      -  Fuchs, C. (2014). OccupyMedia \!&#58; the occupy movement and social media in crisis capitalism. Winchester, UK, Royaume-Uni de Grande-Bretagne et d’Irlande du Nord&#58; Zero Books.

      - Mabi, C., (2016). «&#8239;Luttes sociales et environnementales à l’épreuve du numérique &#58; radicalité politique et circulation des discours&#8239;». Études de communication, no 47&#58; 111-30. <https://doi.org/10.4000/edc.6659>

      - Milan, S., (2015). «&#8239;When Algorithms Shape Collective Action&#58; Social Media and the Dynamics of Cloud Protesting&#8239;». Social Media + Society 1 (2)&#58;

      - Nez, H., (2015). «&#8239;Des « informateurs citoyens ». Usages des images par les Indignés espagnols&#8239;». Sciences de la société, no 94&#58; 139-54. <https://doi.org/10.4000/sds.2507>

      - Riboni, U.L.. (2015). «&#8239;Filmer et rendre visible les quartiers populaires dans la Tunisie en révolution&#8239;». Sciences de la société, no 94&#58; 121-36. <https://doi.org/10.4000/sds.2487>

      - Bortzmeyer, G. (2016) «&#8239;Révolution, touche replay&#8239;», Vacarme, vol. 77, no. 4, pp. 74-83.

      - Lovink G., Garcia D. (1997), On the ABC of Tactical Media & Workspace Manifesto [https://networkcultures.org/geert/2015/03/17/on-the-abc-of-tactical-media-workspace-manifesto-1997/?pdf=1112](https://networkcultures.org/geert/2015/03/17/on-the-abc-of-tactical-media-workspace-manifesto-1997/?pdf=1112)

      - Lovink, G. (2005) Tactical Media, the Second Decade, Brazilian Submidialogia

      - Lovink, G. (2007) Zero Comments&#58; Blogging and Critical Internet Culture, Routledge, London and New York

  - title: Sur la crise du journalisme et des médias d’information
    references:
      - Jost, F. (2020). Médias &#58; sortir de la haine. Paris, CNRS Editions.

      - Bronner, G. (2020). La démocratie des crédules. Paris, PUF.

      - Accardo, A. (2017). Pour une socianalyse du journalisme, considéré comme une fraction emblématique de la nouvelle petite bourgeoisie intellectuelle. Paris, Agone.

      - Kovach, B., Rosenstiel, T. (2004). Principes du journalisme, ce que les journalistes doivent savoir, ce que le public doit exiger. Trad. Monique Berry, Paris, Gallimard.

      - Ramonet, I. (2011). L'Explosion du journalisme. Des médias de masse à la masse de médias. Paris, [Éditions Galilée](https://fr.wikipedia.org/wiki/%25C3%2589ditions_Galil%25C3%25A9e).

      - Plenel, E. (2018). La valeur de l'information. Paris, Don Quichotte,.

      - Plenel, E. (2020). La sauvegarde du peuple. Presse, liberté et démocratie. Paris, [La Découverte](https://fr.wikipedia.org/wiki/La_D%25C3%25A9couverte).

      - Frau-Meigs, D. (2019). Faut-il avoir peur des fake news&#8239;? Paris, La documentation française.

      - Zuckerman, E. (2019). Média polarization, «&#8239;à la française&#8239;»&#8239;? Comparing the French and American ecosystems, rapport juin pour l’Institut Montaigne. <https://www.institutmontaigne.org/publications/media-polarization-la-francaise>

      - Charron, J.-M. (2017). Pour combattre la post-vérité, les médias condamnés à innover, INA, la revue des médias, 21 avril – Mis à jour le 12 mars 2019. <https://larevuedesmedias.ina.fr/pour-combattre-la-post-verite-les-medias-condamnes-innover>

      - Charron, J.-M. et Papet. J. (dir.) (2016). Le Journalisme en questions &#58; Nouvelles frontières des médias et du journalisme. Paris, L’Harmattan.

      - Dewey J., (2010). Le public et ses problèmes, Paris. Gallimard, collection Folio Essais.

      - Castells, M. (1998, 1998, 1999, réédition en 2001)). L'Ère de l'information. Vol. 1, 2 et 3, Paris, Fayard

  - title: Sur le complotisme et la post-vérité 
    references:
      - Lordon, F. (2015). Vous avez dit «&#8239;complot&#8239;», le symptôme d’une dépossession, Le Monde diplomatique, juin.

      - <https://www.monde-diplomatique.fr/2015/06/LORDON/53070>

      - Lordon, F. (2017). Le complotisme de l’anticomplotisme, Le Monde diplomatique, octobre. <https://www.monde-diplomatique.fr/2017/10/LORDON/57960>

      - Lordon, F. (2020). Paniques anticomplotistes, Le Monde diplomatique, La pompe à phynance, blog du 25 novembre, <https://blog.mondediplo.net/paniques-anticomplotistes>

      - Watzlawick, P. (1978). La réalité de la réalité, confusion, désinformation, communication. Trad. Edgar Roskis, Paris, Seuil.

      - Stiegler, B. (2018). Qu’appelle-t-on panser? 1.L’Immense Régression. Paris, Les liens qui Libèrent.

      - Zuckerman, E. (à paraître). [Mistrust&#58; Why Losing Faith in Institutions Provides the Tools to Transform Them](https://www.amazon.fr/Mistrust-Institutions-Provides-Transform-English-ebook/dp/B0858G5YN8/ref=sr_1_1?dchild=1&qid=1608737386&refinements=p_27%253AEthan+Zuckerman&s=books&sr=1-1), W.W.Norton & Company.

      - Arendt, H. (1954). Vérité et politique, in La Crise de la culture. Trad. C. Dupont et A. Huraud, Paris, Gallimard, 1972.

      - Cassin, B. (2018). Quand dire, c’est vraiment faire, Homère, Gorgias et le peuple arc-en-ciel. Paris, Fayard.

      - Revault d’Allonnes, M. (2018). La faiblesse du vrai, ce que la post-vérité fait à notre monde commun. Paris, Seuil.

      - Williams, B. (2006).Vérité et véracité. trad. Jean Lelaidier, Paris, Gallimard.

      - Origgi, G. (2008). Qu’est-ce que la confiance&#8239;? Paris, Vrin.

      - Origgi, G. (2012). Democracy and trust in the Age of The Social Web, Séminaire II Seminario di Teoria Politica Aosta, 28-30 June&#58; [http://gloriaoriggi.blogspot.com/2012/06/epistemic-democracy-in-age-of-internet.html](http://gloriaoriggi.blogspot.com/2012/06/epistemic-democracy-in-age-of-internet.html)

      - Whitehead, A. N. (1995). Procès et réalité. Essai de cosmologie. Traduit de l'anglais par D. Charles, M. Elie, M. Fuchs, J.-L. Gautero, D. Janicaud, R. Sasso, A. Villani, Paris, Gallimard.

      - Stengers, I. (2002). Penser avec Whitehead. Une libre et sauvage création de concepts, Paris, Seuil.

      - Stengers, I. (2020). Réactiver le sens commun, lecture de Whitehead en temps de débâcle, Paris, La découverte, coll. Les empêcheurs de penser en rond.

      - Lamotte, M.&#8239;; Le Caisne L.&#8239;; Le Courant S. (2019). Fake news, mensonges & vérités, Le monde commun, des anthropologues dans la cité, Clamecy, PUF, Humensis.

      - Foucault, M. (2009). Le courage de la vérité, le gouvernement de soi et des autres 2, Cours au Collège de France. 1984, Paris, EHESS, Gallimard, Seuil. 

      - Gros, F. Michel Foucault, une philosophie de la vérité En ligne. URL &#58; <http://1libertaire.free.fr/IntroPhiloFoucault.html>

      - Foucault, M. (1979-1980, réed. 2012) &#58; Du gouvernement des vivants – Cours au Collège de France, Gallimard, Seuil, coll. «&#8239;Hautes études&#8239;», Paris.

  - title: Sur la démocratie numérique 
    references:
      - Allard, L. (2018). Des mobiles et des apps &#58; 50 nuances d'image, Paris, Armand Colin.

      - Nova, N. (2020). Smartphones &#58; une enquête anthropologique, Genève.

      - Cardon, D. (2015). À quoi rêvent les algorithmes. Nos vies à l'heure des big data. Paris, Seuil, La République des idées.

      - Smyrnaios, N. (2017). Les GAFAM contre l'internet, Paris, Ina éditions,.

      - Ertzscheid, O. (2017). L'appétit des géants. Pouvoir des algorithmes, ambition des plateformes, Caen, C\&F Editions, mai.

      - Tufecki, Z. (2019). Internet et les gaz lacrimogènes, Forces et fragilités de la contestation connectée, Paris, C\&F éditions.

      - Turner, F. (2013). Aux sources de l'utopie numérique &#58; De la contre-culture à la cyberculture. Trad. de l'anglais, Caen, C\&F Editions.

      - Stiegler, B. (2012). États de choc - Bêtise et savoir au XXIe siècle, Paris, Fayard/Mille et une nuits.

      - Srnicek, N. (2018). Capitalisme de plateforme, l’hégémonie de l’économie numérique, Montréal, LUX.

      - Rouvroy, A.&#8239;; Berns, T. (2013). Gouvernementalité algorithmique et perspectives d'émancipation. Le disparate comme condition d'individuation par la relation&#8239;? Réseau n°177, p. 163 à 196 <https://www.cairn.info/revue-reseaux-2013-1-page-163.htm>

      - Lovink G. Rossiter N., (2018) Organization after social media, Minor Compositions, Colchester, NewYork.

  - title: Sur l’individuation, la subjectivation et la contribution
    references:
      - Stiegler, B. (2013). Pharmacologie du Front National. Paris, Flammarion.

      - Stiegler, B. (2013). De la misère symbolique. Paris, Flammarion.

      - Stiegler, B. (2015). La Société automatique &#58; 1. L'avenir du travail. Paris, Fayard.

      - Stiegler, B. (2016). Dans la disruption, comment ne pas devenir fou&#8239;? Paris. Les liens qui Libèrent.

      - Stiegler, B. (2020) (dir.) Bifurquer, il n’y a pas d’alternative. Paris, Les liens qui Libèrent.

      - Barthélémy J-H. (2005), Penser l’individuation, Paris, L’Harmattan.

      - Simondon, G. (2012). Du mode d’existence des objets techniques. Paris, Flammarion.

      - Simondon, G. (1989). L'individuation psychique et collective. Paris, Aubier. 

      - Simondon, G. (2013). L'individuation à la lumière des notions de formes et d'information. Paris, Jérôme Million.

      - Foucault, M. (1994). Dits et écrits, volume III, Paris, Gallimard.

      - Agamben, G. (2006). Qu’est-ce qu’un dispositif&#8239;? Paris, Payot et Rivages.

      - Lordon, F. (2015). Imperium, Structures et affects des corps politiques, Paris, La fabrique.

      - Bourdieu, P. (1992). Les règles de l’art, genèse et structure du champ littéraire, Paris, Seuil.

      - Castoriadis, C. (1975). L'Institution imaginaire de la société&#8239;; Paris, Seuil.

      - Beuys, J., (1998), Par la présente je n’appartiens plus à l’art, l’Arche, Paris

  - title: Sur le design participatif et les sciences des médias 
    references:
      - Huyghe, P. D. (2014). A quoi le tient le design&#8239;? Paris, De l’incidence éditeur.

      - Citton, Y. (2014). Pour une écologie de l’attention. Paris, Seuil.

      - Citton, Y. (2017). Médiarchie, Paris, Seuil.

      - Flusser, V. (2006). La civilisation des médias, Trad. Claude Maillard, Belval, Circé.

      - Mersch, D. (2018). Théorie des médias, une introduction, Les presses du réel.

      - Citton, Y. et Doudet, E. (dir.) (2019). Ecologies de l’attention et archéologie des médias . Grenoble, UGA éditions,.

      - Turner, F. (2016). Le Cercle démocratique. Le design multimédia, de la Seconde Guerre mondiale aux années psychédéliques, C\&F Editions.

      - Van der Berg, K.&#8239;; Jordan, Cara M.&#8239;; Kleinmichel, Philipp. (2019). The art of Direct Action Social Sculpture and Beyond&#8239;; Berlin, Sternberg Press.

      - McCarthy J., Wright P., (2015) Taking (a)part. The politics and aesthetics of participation in experience-centered design, MIT Press

      - Manzini E., (2015) Design. When Everybody Designs, MIT Press

      - Zask J., (2011) Participer, Essai sur les formes démocratiques de la participation, Paris, La Découverte.

      - Godbout T. J., (2014) La participation contre la démocratie, Montréal, Liber

      - Duhem L., Rabin K., (2018) Design écosocial. Convivialité, pratiques situées, nouveaux communs, Fauconey-et-la-mer,

      - Duhem L., Pereira de Moura R., (2020) Design des territoires. L’enseignement de la biorégion, Paris, Eterotopia.

      - Stiegler B., (2008) Le design de nos existences à l’époque de l’innovation ascendante, Paris, Mille et une nuits/Centre Pompidou

      - Banz C., Krohn M., (2018) Social Design &#58; Participation and Empowerment, Zurich, Lars Muller Publishers.

      - Bishop C. (2012) ARTIFICIAL HELLS, Participatory Art and the Politics of Spectatorship, Verso, London

      - Lovink, G. (2019), Sad by design, on Platform Nihilism, Pluto press, London

      - Eude, E. Maire V. (sous la direction de) (2018) La Fabrique a Ecosysteme&#58; Design, territoire et innovation sociale, Loco, Paris.

  - title: Sur la sociologie des classes populaires
    references:
      - Siblot, Y., Cartier, M., Coutant, I., Masclet, O. et Nicolas Renahy, N. (2015). Sociologie des classes populaires contemporaines&#8239;; Paris, Armand Colin

      - Schwartz, O. et Masclet, O., (2019) «&#8239;Les classes populaires, c'est les pauvres&#8239;», dans Olivier Masclet, Séverine Misset et Tristan Poullaouec, La France d'en bas&#8239;? &#58; idées reçues sur les classes populaires, [Éditions du Cavalier bleu](https://fr.wikipedia.org/wiki/Éditions_du_Cavalier_bleu).

      - Schwartz, O., (1998) La notion de «&#8239;classes populaires&#8239;», habilitation à diriger des recherches en sociologie, [Université de Versailles-Saint-Quentin-en-Yvelines](https://fr.wikipedia.org/wiki/Université_de_Versailles-Saint-Quentin-en-Yvelines).

      - Bourdieu, P., (1979) [La Distinction. Critique sociale du jugement](https://fr.wikipedia.org/wiki/La_Distinction._Critique_sociale_du_jugement), Paris, [Les Éditions de Minuit](https://fr.wikipedia.org/wiki/Les_Éditions_de_Minuit).

      - Pasquier, D., (2018) [L'Internet des familles modestes. Enquête dans la France rurale](https://journals.openedition.org/lectures/17015), Paris, Presses des Mines, coll. «&#8239;Sciences sociales&#8239;».


# Cette section permet de publier un contenu complémentaire à l'article, sous la forme de sections disctincte en fin de page. 
# le contennu de ces section peut être rédigé en markdown
aside:
  - title: Comité scientifique
    content: "
      - **Yves Citton** - Professeur de Littérature et Média, Université Paris 8 Vincennes-Saint-Denis, cofondateur de la revue Multitudes.

      - **Dominique Cardon** - Directeur du MédiaLab Sciences Po, Professeur de Sociologie du Numérique, Membre du comité de prospective de la CNIL.

      - **Joëlle le Marec** - Professeure en Sciences de l'Information et de la Communication au Muséum d'Histoire Naturelle, laboratoire PALOC (Patrimoines Locaux, Environnement et Globalisation).

      - **Geert Lovink** - Directeur de l'Institute of Network Cultures, Professor of Interactive Media à la Hogeschool van Amsterdam University of Applied Sciences, mediactiviste et cofondateur du concept de *tactical media*

      - **Olivier Fillieule** - Directeur de recherche au CNRS, Institut politique de l’Université de Lausanne, CRAPUL (Centre de Recherche sur l'Action Politique), Professeur de Sociologie Politique.

      - **Michelle Christensen** – Professeure invitée à la chaire «&#8239;Open science&#8239;», Technische Universität Berlin et Einstein Center Digital Future. Co-directrice avec Florian Conradi du groupe de recherche «&#8239;Critical Maker Culture&#8239;» à la Universitat der Kunst de Berlin et à l'Institut Weizenbaum. Professeur de design et de sociologie.

      - **Clément Mabi** - Directeur adjoint de COSTECH, laboratoire de recherche de l'Université Technologique de Compiègne, Maître de conférences en Sciences de l'Information et de la Communication.

      - **Noël Fitzpatrick** - Doyen de la GradCAM (Graduate School of Creative Arts and Media), TU Dublin, directeur de EUT Lab (European University of Technology Laboratory, Dublin), Professeur de Philosophie.

      - **Laurence Allard** - Maîtresse de conférences en Sciences de l'Information et de la Communication à l'Université de Lille, laboratoire IRCAV (Institut de Recherche sur le Cinéma et l'Audiovisuel) à l'Université Paris 3 Sorbonne

      - **Jamie Allen** - Senior Researcher au Critical Media Lab, University of Applied Sciences and Arts Northwestern Switzerland, Artiste.
    "

  - title: Coordination du projet
    content: "
      - **Porteur du projet&#160;:   
        Igor Galligo** (ArTeC, UTC Costech, Technische Universität)

      - **Responsable axe 1&#8239;:   
        Zakaria Bendali** (CRAPUL)&#8239;: [Contact](mailto:Zakaria.Bendali@unil.ch)

      - **Responsable axe 2&#8239;:   
        Edouard Bouté** (UTC Costech)&#160;: [Contact](mailto:edouard.boute@utc.fr)
      
      - **Responsable axe 3&#160;:   
        Igor Galligo**&#160;: [Contact](mailto:democratienumeriquepopulaire@protonmail.fr)

      - **Responsable axe 4&#8239;:   
        Ludovic Duhem** (ÉSAD Valenciennes)&#8239;: [Contact](mailto:ludovic.duhem@esad-valenciennes.fr)
    "

  - title: Contributions au dossier
    content: "
      Trois types de contribution écrite sont attendues pour ce dossier de recherche qui sera publié dans le numéro 6 des [Cahiers Costech](https://www.costech.utc.fr/CahiersCOSTECH/), la revue numérique scientifique de COSTECH, le laboratoire de recherche de [l’Université Technologique de Compiègne](http://www.costech.utc.fr/)&#8239;:

      1.  Des articles scientifiques portant sur les problématiques du dossier et correspondant à un ou plusieurs des 4 axes de recherche présentés.   

        *Entre 15 et 20 articles attendus entre 25000 et 40000 signes, espaces compris.*

      2.  Des entretiens avec des automédias correspondant au premier axe présenté.   

        *Entre 10 et 15 entretiens attendus, d’une taille moyenne de 25 000 signes, espaces compris.*

      3.  Dans un deuxième temps, nous envisageons de proposer aux chercheur.es.s et aux automédias qui le souhaitent, de coécrire ensemble des articles, en fonction des affinités et intérêts mutuels qui pourront apparaître.   

        *4, 5 articles attendus entre 25000 et 40000 signes, espaces compris*
    "

  - title: Candidatures 
    content: "
              Les candidatures de type 1 et 2 seront sélectionnées au mois de Janvier 2022 par l’équipe de coordination ainsi que par le comité scientifique du dossier.

              Pour postuler il est demandé aux candidats d’envoyer au responsable de l’axe concerné par la contribution **un document de 3000 signes maximum** (espaces compris) présentant **la problématique** et **l’objectif** de la contribution ainsi que **le prénom, le nom et le statut du/de la candidat.e.**
    "

  - title: Calendrier
    content: "
      1.  Date limite de réception des candidatures pour les **contributions 1 et 2**&#160;:   
      **23 janvier 2022**

      2.  Date de remise des textes sélectionnés pour les **contributions 1 et 2**&#160;:    
      **fin octobre 2022**
      
      3.  Date de remise des textes pour les **contributions 3**&#160;:   
      **fin octobre 2022**   


    Avant cette publication, un colloque et des ateliers seront organisés pendant 3 jours les 22, 23 et 24 juin 2022 à la [ Maison des Sciences de l’Homme Paris-Nord ](https://www.mshparisnord.fr/) pour rencontrer l’ensemble des contributeurs, chercheurs et Automédias et discuter des différentes contributions. Cela permettra en outre d’amorcer la production des contributions de type 3. Enfin, dans un troisième temps, une publication papier est envisagée pour l’année 2023 à partir des meilleures contributions correspondant aux 3 types.      
    "
---

# Argumentaire

Si la fabrique institutionnalisée de l’information et sa médiatisation restent principalement l’affaire de professionnels appelés «&#8239;journalistes&#8239;», qui répondent à des valeurs, des critères et des protocoles enseignés dans des écoles de journalisme et consignés dans des chartes (ce qui ne contredit pas un constat sur la diversité des genres et des pratiques journalistiques), il semble que la démocratisation de la production et de la communication de l’information transforme aujourd’hui leurs valeurs et fabriques. Cette démocratisation est produite par le développement et l’usage conjoints des technologies numériques de l’information et de la communication (TNIC), des GAFAM, de réseaux sociaux numériques alternatifs (Discord, Mastodon, Telegram, etc.), mais aussi en France par l’avènement de mouvements socio-politiques populaires réclamant davantage de démocratie – et ainsi de pluralisme idéologique et sociologique (Cardon et Granjon, 2013).

C’est ce qu’illustre de manière exemplaire le mouvement populaire des Gilets Jaunes en France (Ertzscheid, 2019) – dont on retrouve plusieurs caractéristiques médiatico-politiques ces quinze dernières années dans d’autres pays, tels que le mouvement «&#8239;5 étoiles&#8239;» en 2009 en Italie, le «&#8239;Printemps arabe&#8239;» en 2010, le mouvement de l’«&#8239;Alt-Right&#8239;» aux États-Unis en 2016 (concomitant avec la victoire de Donald Trump) ou encore le soulèvement insurrectionnel Hong Kongkais contre le pouvoir de Pekin en 2019-2020. Le mouvement des Gilets Jaunes a conduit à la résurgence et au développement sur Internet de médias dits «&#8239;populaires&#8239;»[^1] qui ont réalisé de nombreuses expérimentations médiatico-politiques, individuelles ou collectives, indépendantes d'une formation professionnelle, d'une corporation ou d'une institution de représentation et de légitimation. La fonction vidéo qui équipe les smartphones, associée à l’usage des réseaux sociaux numériques (RSN), a favorisé la démocratisation de la production médiatique (Nova, 2020), en permettant à tout individu de révéler, relayer et rendre public sur Internet des informations à caractère politique, prises sur le vif, comme en témoigne le film de David Dufresne «&#8239;Un pays qui se tient sage&#8239;»[^2], qui compile des dizaines de séquences vidéo réalisées avec des smartphones lors de manifestations des Gilets Jaunes.

Avec le mouvement des Gilets Jaunes, un acteur politique est apparu avec force dans le champ des actions et des expériences démocratiques françaises&#8239;: l’*automédia*. Que ce soit le *geste auto-médiatique* produit à la volée par un seul individu avec un smartphone, ou *l’entreprise collective automédiatique* déjà aperçue dans le champ des luttes démocratiques (Thiong-Kay, 2020) qui réinvente les formes des *médias* *tactiques* (Garcia et Lovink, 1997) et des *médiactivistes* (Cardon et Granjon, 2013), l’*automédiation* désigne l’autoproduction et l’autodiffusion de l’information à caractère politique par l’usage ou la réinvention des appareils et circuits de communication numériques. Le geste amateur et le cadre initialement communautaire de la fabrique automédiatique constitue ainsi une extension de la *culture maker* au domaine de la fabrique médiatique.

Cependant, le déploiement des *automédias* se réalise aujourd’hui au sein d’un contexte techno-économique de *post-vérité*[^3]*,* qui est apparu avec l’avènement d’un *capitalisme informationnel* (Castells, 1998, 1999a, 1999b) et d’une numérisation de l’information sur l’Internet. Cette évolution techno-économique détermine de nouvelles *infrastructures numériques de l’information* qui tendent à valoriser davantage une information pour le *capital attentionnel numérique* (Citton, 2017) qu’elle détient que pour sa valeur de vérité&#8239;; ce qui perturbe fortement *le régime de vérité*[^4] *journalistique*, et avec *la fonction démocratique* à laquelle il doit être consacré (Arendt, 1954 (1972)&#8239;; Revault d’Allonnes, 2018). Selon Bernard Stiegler (2018), *le capitalisme numérique néo-computationnel*, en *disruptant* les circuits et traitements traditionnels de l’information qui reposaient sur des processus de *véridiction, certification et vérification*, altère aujourd’hui tout autant la production de l’information par les organisations journalistiques que sa réception par des publics variés, en provoquant des réactions de défiance, de méfiance, voire de haine envers les institutions journalistiques, alors accusées de trahir leur déontologie (Jost, 2020).

En conséquence, les individus accordent (parfois) davantage de crédit à une information délivrée par un·e proche – tel un automédia avec qui des affects communs sont partagés – que par une information produite et vérifiée *(«&#8239;fact-checked&#8239;»)* par une institution journalistique[^5]. Il en résulte une crise des institutions et organisations journalistiques ainsi qu’une substitution des productions journalistiques par celles des automédias en tant que sources d’information, notamment pour *les publics appartenant aux classes populaires*, mais sans que ceux-ci ne formulent et ne se réfèrent à ce que nous pourrions nommer&#8239;: un *régime de vérité automédiatique*. Ainsi, le contexte techno-économique de *post-vérité,* sous le règne des GAFAM, favorise aujourd’hui l’explosion du genre automédiatique (en particulier sur YouTube), en faisant fi de toute *critériologie de droit* *et de toute domination du droit sur le fait* (Stiegler, 2018)&#8239;; ce qui le constitue en retour – et souvent malgré lui – comme un acteur de la *post-vérité.*

Or, il importe de ne pas réduire le genre automédiatique aux formes «&#8239;gafamisées&#8239;» à partir desquelles se développent aujourd’hui les designs et circuits techno-économiques de production et de réception de l’information, et dans lesquels se trouvent aujourd’hui enfermés un grand nombre d’automédias. Bien que la figure dominante du *YouTuber* cristallise cette subjectivation techno-économique du genre automédiatique, et tandis que certains détracteurs du genre, qui se revendiquent d’une éthique journalistique, opposent le genre automédiatique et la production de vérité, nous postulons au contraire que cette opposition n’est pas valide si nous distinguons le genre automédiatique du contexte techno-économique au sein duquel il émerge aujourd’hui.

En effet, la conquête de *l’autonomie des automédias* ne repose pas seulement sur celles de leurs libertés d’actions (ou d’entreprises) et de leurs indépendances financières – problèmes dont il a déjà largement été discuté au sein du milieu journalistique (Ramonet, 2011&#8239;; Plenel, 2018, 2020, etc.) – mais aussi sur leurs capacités à réinventer leurs infrastructures technologiques et économiques ainsi que les valeurs, les normes, et les protocoles de production de l’information afin d’*établir leurs nécessités et légitimités démocratiques et scientifiques*. Il ne s’agit donc pas seulement de faire un usage politique d’une technologie commercialisée sur smartphone, mais de concevoir et de produire de nouvelles *puissances* (Lordon, 2015) *techno-politiques* à travers des projets collectifs à la fois technologiques, politiques et médiatiques. La catégorie des *médias indépendants* est donc appelée à évoluer vers celle des *automédias,* au sens littéral et exhaustif du terme «&#8239;auto-&#8239;», qui signifie «&#8239;par soi-même&#8239;», incluant dans une perspective *médiartiviste* (Citton, 2017) *l’auto-design des circuits de production et de communication d’information*.

Si comme l’affirme Jean-Marie Charon, «&#8239;la reconquête de la confiance ne passe pas que par l’exercice de vérification[^6]»*,* nous souhaitons interroger dans ce dossier l’hypothèse que la production d’une information vraie et les conditions de sa crédibilité auprès d’un public appartenant aux classes populaires peuvent être réagencés dans un design numérique et économique nouveau par le genre automédiatique. Ce dossier aura pour enjeu de proposer une réflexion sur les fondements et modèles épistémologique, économique, politique et technologique d’un nouveau *régime de vérité automédiatique,* qui favorise non seulement la démocratisation de la production et de la communication de l’information, mais en outre la réinvention de la valeur de *vérité informationnelle*, ainsi que celle de *crédibilité informationnelle*, *à partir* et *auprès* de publics populaires – c’est-à-dire la réinvention des rapports entre médias, vérité et démocratie par le genre automédiatique.

Comment les infrastructures numériques ont-elles transformé les pratiques *makers* des *médias tactiques* et du *médiactivisme* pour donner naissance au *«&#8239;tournant participatif&#8239;»* puis *au genre automédiatique*&#8239;? À quels *dispositifs de pouvoir* (Foucault, 1994) techno-économique les automédias sont-ils aujourd’hui assujettis et contraints dans l’expérimentation de nouvelles individuations automédiatiques&#8239;? Quelles sont les nouvelles valeurs, normes et protocoles médiatico-politiques portées et fabriqués par les automédias&#8239;? Comment redesigner la fabrique de l’information par des processus et circuits participatifs/contributifs pour *produire confiance et vérité en l’information au sein de milieux populaires*&#8239;?

Afin de travailler ces questionnements, nous invitons les contributeur·trices à s’inscrire dans l’un des quatre axes d’étude suivants&#8239;:

## 1/ Automédias&#8239;: descriptions et typologies

*Comment les infrastructures numériques ont-elles transformé les pratiques makers des medias tactiques et du médiactivisme pour donner naissance au «&#8239;tournant participatif&#8239;» puis au genre automédiatique&#8239;?*

Les travaux s’inscrivant dans ce premier axe contribueront à proposer un panorama empirique et phénoménologique des automédias à partir d’une analyse des (infra-)structures, des individualités et des organisations automédiatiques existantes en France. Les contributions s’appuieront sur des entretiens réalisés avec des automédias pour nourrir ce premier axe de recherche. Ils auront pour enjeu d’approfondir une réflexion sur leurs processus d’individuation à partir des potentialités offertes par les infrastructures numériques, et notamment du *«&#8239;tournant participatif&#8239;». Les sciences de l’information et de la communication, les sciences politiques et la sociologie des médias* pourront être convoquées pour cet *objectif descriptif et typologique.* Les infrastructures numériques de communication sur lesquels se développent les automédias permettent aujourd’hui une grande diversité des pratiques automédiatiques. Ils constitueront les matériaux de base nécessaires à l’élaboration de ce panorama. Les facteurs distinctifs suivants pourront être considérés&#8239;:

  - Le matériel numérique de captation (sonore, vidéo)

  - Le matériel de création numérique pour l’édition textuelle, sonore ou vidéo.

  - Les plateformes numériques ou réseaux sociaux utilisés (YouTube, Discord, Twitch, Facebook, Twitter, Mastodon, Peer-Tube, etc.)

  - Le mode d’expression (textuel, vidéo, sonore)

  - Le format d’expression (edito, zapping, interview, enquête, analyse&#8239;; direct ou différé&#8239;; improvisé ou lecture d’un prompteur, etc.)

  - Le contexte d’enregistrement (en studio ou en extérieur)

  - Le mode de financement (autofinancement, accès payant aux usagers, commission sur publicité, crowdfunding (quels outils numériques&#8239;?), financement public, etc.)

  - L’échelle d’investigation (locale, nationale, etc.)

  - Le rapport à un territoire ou à une communauté (revendication d’une échelle ou d’une culture territoriale ou communautaire.)

  - Le rapport à une idéologie politique

  - L’initiative du projet (individuelle ou collective)

  - Le rapport à la création collective ou participative

  - Le processus de création d’information (production d’information à partir d’interview ou d’enquêtes de terrain brutes, ou d’analyses de textes, de vidéos, d’enquêtes journalistiques).

  - Le profil sociologique des automédias

  - Le profil sociologique du public

  - Les trajectoires personnelles des automédias

  - Les relations d’interdépendance

  - Les relations d’influence

  - Les cartographies de l’offre automédiatique établie par les automédias eux-mêmes

  - Les rapports d’opposition ou d’influence avec le journalisme

  - Les interactions avec le public
  {.minor .duo}

## 2/ Les automédias face au capitalisme numérique&#160;: analyse d’un rapport de force techno-économique.

*À quels dispositifs de pouvoir techno-économique les automédias sont-ils aujourd’hui assujettis et contraints dans l’expérimentation de nouvelles individuations automédiatiques&#8239;?*

Nous invitons les travaux s’inscrivant dans cet axe à décrire et analyser les rapports de forces techno-politiques et dispositifs technologiques qui contraignent les *individuations automédiatiques*[^7] dans leurs processus d’expression et de communication. Celles-ci désignent le processus d’individuation et de singularisation des automédias, en ce que l’automédia est toujours constitué par un individu ou un collectif d’individus en relation avec un public auquel il s’adresse et avec lequel il communique, et des dispositifs, appareils ou prothèses techniques qui lui permettent d’enregistrer et de diffuser des flux sonores et vidéos. Cependant, ces *individuations* *automédiatiques* sont prises dans des *dispositifs de pouvoir* (Foucault, 1994) qui cadrent, orientent ou font obstacle à leurs projets afin de constituer des *subjectivations* *automédiatiques*[^8]. En effet, les automédias sont aujourd’hui *saisis* dans des rapports de forces, parfois guerriers[^9], qui travaillent et agissent en profondeur sur leur dynamique d’individuation*. Ces dispositifs de pouvoir* les transforment alors en dynamiques de subjectivation, c’est-à-dire en médias dont l’individuation est *assujettie* aux acteurs technologiques et économiques dominants. Dans la perspective de notre réflexion sur *les infrastructures technologiques de la post-vérité*, nous souhaitons interroger un nouveau rapport de forces qui confronte les individuations automédiatiques aux grandes infrastructures technologiques privées qui leurs servent de supports d’expression et de communication. En soumettant celles-ci à différents règlements, designs et algorithmes édictés et produits par ces plateformes numériques (YouTube, Twitter, Facebook, etc.) – qui répondent notamment aux valeurs et logiques du *capitalisme numérique* (Srnicek, 2018), et conséquemment aux mécanismes de la *grammatisation* (Citton, 2014), du *néo-computationnalisme* (Stiegler, 2013, 2018) et de la *gouvernementalité algorithmique* (Rouvroy, Berns, 2013) – les automédias sont confrontés à une lutte contre ces infrastructures pour la défense de leurs libertés d’expression et de communication des informations qu’ils produisent, mais aussi à une guerre attentionnelle pour révéler certaines informations, ainsi qu’ à des luttes pour en garantir la vérité et la crédibilité. L’enjeu de ce deuxième axe de recherche consistera à décrire les mécanismes et contraintes technologiques exercées par les grandes plateformes du capitalisme numérique sur les défis d’attention, de liberté, de vérité et de crédibilité des automédias.

## 3/ Les nouvelles valeurs, normes et protocoles automédiatiques

*Quelles sont les nouvelles valeurs, normes et protocoles mediatico-politiques portés et fabriqués par les automédias&#8239;?*

Le contexte de la *post-vérité,* se consolidant dès la seconde moitié des années 2010, semble aujourd’hui renouveler certaines distinctions entre journalisme et automédias, sur fond de conflits parfois violents entre leurs membres. En effet, nombre d’institutions journalistiques accusent les automédias d’être les auteurs ou les relais de thèses *complotistes.* Certaines de ces thèses dénoncent le contrôle des productions des organisations journalistiques par les sociétés capitalistes qui les financent, et en déduisent des processus de corruption de la déontologie journalistique qui masquent notamment les choix d’orientations idéologiques des informations ou enquêtes journalistiques fabriquées[^10]. L’accusation de complotisme portée par certaines institutions journalistiques contre la production de *fake news automédiatiques* ou la prétendue «&#8239;débilité&#8239;» des analyses produites par certains automédias jette en retour un discrédit sur l’ensemble du genre automédiatique, accusé de manquer d’objectivité, d’impartialité, ou encore de distinction entre *un* *fait* et *une opinion*. Cela engendre en réaction une lutte interne chez certains automédias pour *convaincre de la vérité et de la crédibilité des informations qu’ils produisent ou transmettent auprès de leurs publics*. Rappelons que sur le plan de la normativité démocratique, *l’énonciation de* *vérités* *de fait* est considérée comme une valeur nécessaire à l’information de *l’opinion publique et de l’instruction publique* (Arendt, 1954 (1972)&#8239;; Revault d’Allonnes, 2018). Les automédias, en tant qu’acteurs nouveaux du combat démocratique, sont donc sommés d’en garantir la prégnance dans leurs productions. Cette lutte entre journalisme et automédia permet ainsi la réélaboration de valeurs médiatiques alternatives en même temps que la constitution de nouvelles communautés médiatico-politiques. Portés par des ambitions démocratiques et étouffés par le rapport de force techno-économique mentionné, cet axe vise à mettre en lumière l’effort de création normative des automédias sur les valeurs, les normes et les protocoles qui sous-tendent la production de leurs informations – distinctement de certains standards journalistiques. À l’instar des *outsiders* du champ littéraire – dont Pierre Bourdieu décrivit les logiques d’émancipation et d’autonomisation de la modernité littéraire dans son ouvrage *Les règles de l’art*, *genèse et structure du champ littéraire* (Bourdieu, 1992) – les automédias nous semblent aujourd’hui promis à (co-)écrire le récit de leur *subsistance*, de leur *existence* et de leur *consistance*[^11] (Stiegler, 2013) au travers d’un dialogue comparatif avec les principes, organisations et pratiques du journalisme.

## 4/ Design participatif, vers de nouveaux circuits de production de l’information pour les automédias

*Comment redesigner la fabrique de l’information par des processus participatifs pour produire confiance et vérité en l’information au sein de milieux populaires*&#8239;?

La question du *design* n’est pas une question périphérique ou superficielle pour les individuations automédiatiques. Le rapport de force techno-économique correspondant énoncé plus haut suppose une certaine *conception de l’information*, au quadruple sens de l’idée de ce qu’elle doit être, de la forme qu’elle doit prendre, de la manière de la produire et de l’usage que l’on peut en faire. Mais plus concrètement, à travers les plateformes numériques dominantes, c’est toute une conception standardisée (par une logique consumériste et calculatoire), une production automatisée (par la puissance algorithmique), une esthétique formatée (par un graphisme autoritaire et une interface fermée) et un usage contrôlé (par des restrictions, des interdictions et des captations de données) qui s’impose sans participation aux conditions de production et de communication de l’information.

Dans ce dernier axe, il s’agit alors de s’interroger non seulement sur le rôle du design de l’information sur l’information elle-même, c’est-à-dire sur l’effet que produit le “milieu technique” (Simondon, 2012) sur la nature, la perception, la signification et l’utilisation de l’information&#8239;; mais de comprendre surtout en quoi la participation au design de l’information est nécessaire pour répondre aux enjeux de vérité et de crédibilité associés à la lutte contre la *post-vérité*.

Au-delà des enjeux esthétiques comme des enjeux praxéologiques ou pragmatiques, le design participatif pose une question clairement *politique*, d’une part quant à la capacité des individus en démocratie à *délibérer* sur les moyens et les fins de la production de l’information (Zask, 2011)&#8239;; et d’autre part quant à la reconnaissance des *exclus* de la participation et à la prise en compte des opposants à la participation comme mot d’ordre à contester.

Le design participatif de l’information, en tant que fabrique démocratique, est à cet égard "pharmacologique" au sens de Stiegler, et nécessite par conséquent un examen poussé de sa *toxicité* pour les individuations automédiatiques, du côté des producteurs comme des récepteurs de l’information qui se *prolétarisent* en perdant leur savoirs réciproques (enregistrer, analyser, interpréter, critiquer) alors qu’elle devrait être le support et le modèle d’une individuation collective ou plus précisément le lieu d’un processus de *transindividuation*[^12]. Une critique qui peut être notamment formulée à travers les travaux de Claire Bishop sur *l’art participatif* (Bishop, 2012). Corrélativement, cette pharmacologie des automédias permettrait d’évaluer et d’expérimenter la pertinence du paradigme *contributif* – en différence avec le paradigme participatif – pour l’élaboration d’un design de l’information compris comme *une* *science et une fabrique populaires de l’information à l’époque de la post-vérité.* Il s’agirait ainsi de *co-concevoir* et d’expérimenter avec des automédias des *formes contributives* d’un *nouveau design territorialisé de l’information* luttant contre la défiance, voir la méfiance, issue de la prolétarisation et de la déterritorialisation (engendrée par le design algorithmique des plateformes médiatiques et automédiatiques). Dans ce but, les nouvelles technologies de l’Open Source Intelligence (OSINT) pourront être considérées. Cette lutte s’opère notamment par un processus d’appropriation des technologies par les habitant·e·s des territoires qui les transforment en retour à différentes échelles de la localité (par exemple du quartier au village, de la métropole à la région voire jusqu’à la biosphère). Les pratiques d’appropriation, de détournement, voire d’invention grâce à des formes nouvelles d’intelligence collective localisées ou reterritorialisées sont en ce sens importantes à étudier dans cet axe. Mais au-delà des enjeux démocratiques connus du design participatif, il s’agira avant tout dans cet axe d’évaluer la pertinence de son paradigme pour refonder vérité et confiance dans l’information, dès lors que l’on est invité, en tant que citoyen, à prendre part à son design.

[^1]: Siblot, Y., Cartier, M., Coutant, I., Masclet, O. et Nicolas Renahy, N. (2015). *Sociologie des classes populaires contemporaines*&#8239;; Paris, Armand Colin. Comme le montrent Yasmine Siblot et ses co-auteurs, une sociologie des classes populaires s’est développée en France depuis une vingtaine d’années. Prenant la suite d’une longue tradition de travaux centrés sur le groupe ouvrier, cette sociologie entend confronter une ambition réaffirmée d’analyse des classes sociales aux mutations profondes qui ont affecté la société depuis une trentaine d’années. Dans ce programme, suivant les réflexions d’Olivier Schwartz la notion de «&#8239;populaire&#8239;» peut être vue comme une héritière d’une sociologie d’orientation culturaliste popularisée en France dans les années 1970. Chacun à sa manière, les ouvrages de Richard Hoggart et Pierre Bourdieu marquent alors une rupture avec les approches ouvriéristes et industrialistes dominantes, et ouvrent l’analyse aux modes de vie du «&#8239;populaire&#8239;». Dans cette veine, la clarification proposée par Olivier Schwartz pose deux caractéristiques fortes des classes populaires&#8239;: d’une part une situation dominée dans l’espace social, d’autre part une relative autonomie culturelle.

[^2]: David Dufresne, *Un pays qui se tient sage*, film documentaire, 86 minutes, Le Bureau, Jour 2 fête, France, 2020

[^3]: Le mot de *post-vérité (post-truth*) a été défini en 2016 par le dictionnaire d’Oxford comme ce qui se rapporte&#8239;: «&#8239;aux circonstances dans lesquelles les faits objectifs ont moins d’influence sur l’opinion publique que ceux qui font appel à l’émotion ou aux croyances personnelles. L’émergence de la «&#8239;post-vérité&#8239;» dans le langage a été alimentée par la montée en puissance des réseaux sociaux en tant que source d’information et par la méfiance croissante vis-à-vis des faits présentés par l’establishment&#8239;». Oxford University Press, Word of the year 2016, Post-truth. [https://languages.oup.com/word-of-the-year/2016/](https://languages.oup.com/word-of-the-year/2016/)

[^4]: «&#8239;En gros, si vous voulez, un régime de vérité, c’est ce qui détermine les obligations des individus quant aux procédures de manifestation du vrai …. On parle de régime politique … pour désigner en somme l’ensemble des procédés et des institutions par lesquels les individus se trouvent engagés, d’une manière plus ou moins pressante …. On peut parler également de régime pénal, par exemple, pour désigner l’ensemble, là aussi, des procédés et institutions par lesquels les individus sont engagés, déterminés, contraints à se soumettre à des lois de portée générale. Alors, dans ces conditions, pourquoi en effet ne pas parler de régime de vérité pour désigner l’ensemble des procédés et des institutions par lesquels les individus sont engagés et contraints à poser, dans certaines conditions et avec certains effets, des actes bien définis de vérité&#8239;? … Le problème ce serait d’étudier les régimes de vérité, c’est-à-dire les types de relation qui lient les manifestations de vérité avec leurs procédures et les sujets qui en sont les opérateurs, les témoins ou éventuellement les objets …&#8239;» Foucault, M. (1979-1980) *&#160;: Du gouvernement des vivants – Cours au Collège de France* , Gallimard, Seuil, 2012, coll. «&#8239;Hautes études&#8239;», Paris, p. 91-92 et 98-99.

[^5]: Jost, F. (15.10.09) *Médias&#8239;: de la méfiance à la haine*, AOC, [https://aoc.media/opinion/2019/10/15/medias-de-la-mefiance-a-la-haine/](https://aoc.media/opinion/2019/10/15/medias-de-la-mefiance-a-la-haine/)

[^6]: Charon, J.M., *Pour combattre la post-vérité, les médias condamnés à innover,* INA, la revue des médias, 21 avril 2017 - Mis à jour le 12 mars 2019 [https://larevuedesmedias.ina.fr/pour-combattre-la-post-verite-les-medias-condamnes-innover](https://larevuedesmedias.ina.fr/pour-combattre-la-post-verite-les-medias-condamnes-innover)

[^7]: L’individuation est définie par Bernard Stiegler dans sa lecture de Simondon comme la «&#8239;… formation, à la fois biologique, psychologique et sociale, de l’individu toujours inachevé. L’individuation humaine est triple, c’est une individuation à trois brins, car elle est toujours à la fois psychique (‘je’), collective (‘nous’) et technique (ce milieu qui relie le ‘je’ au ‘nous’, milieu concret et effectif, supporté par des mnémotechniques). Cf. [https://arsindustrialis.org/individuation](https://arsindustrialis.org/individuation)

[^8]: Pour Giorgio Agamben, «&#8239;le dispositif est effectivement, avant tout, une machine qui produit des subjectivations et c’est par quoi il est aussi une machine de gouvernement&#8239;». Cf. Agamben, G., (2006), *Qu’est-ce qu’un dispositif&#8239;?* Payot et Rivages, Paris, p.42

[^9]: Rappelons que Jérôme Rodrigues, acteur majeur du mouvement des Gilets Jaunes, a été éborgné le 28 décembre 2019 par un tir de Lanceur de Balles de Défense (LBD) déclenché par un agent des forces de l’Ordre au moment où son attention quittait le théâtre des opérations physiques du boulevard Magenta à Paris, où il se trouvait, pour poster sur Twitter une séquence vidéo qu’il venait de réaliser avec son Smartphone, montrant l’ambiance guerrière de la situation.

[^10]: Lordon, F., (2020). *Paniques anticomplotistes,* Le Monde diplomatique, La pompe à phynance, blog du 25 novembre, [https://blog.mondediplo.net/paniques-anticomplotistes](https://blog.mondediplo.net/paniques-anticomplotistes)

[^11]: Ars Industrialis, Vocabulaire, *Subsister, Exister et Consister *: «&#8239;La subsistance, c’est l’ordre immuable des besoins et de leur satisfaction impérative, c’est l’impératif de la survivance. Lorsque la vie humaine est réduite à la pure nécessité subsister, elle est rabattue sur ses besoins et perd le sentiment d’exister. De tels besoins sont aujourd’hui artificiellement produits par le marketing. L’existence – le fait pour l’homme d’*ex-sistere*&#8239;: d’être projeté hors de soi, de se constituer au dehors et à venir – est ce qui constitue celui qui existe dans et par la relation qu’il entretient à ses objets non pas en tant qu’il en a besoin, mais en tant qu’il les désire. Ce désir est celui d’une singularité – et toute existence est singulière. La consistance désigne le processus par lequel l’existence humaine est mue et trans-formée par ses objets, où elle projette ce qui la dépasse, et qui n’existant pas cependant consiste – ainsi de l’objet de son désir, qui est par définition infini cependant que l’infini n’existe pas&#8239;: n’existe que ce qui est calculable dans l’espace et dans le temps, c’est à dire ce qui est fini. De telles infinités sont les objets de l’idéalisation sous toutes ses formes&#8239;: objets d’amour (mon amour), objets de justice (la justice à laquelle nul ne peut renoncer au prétexte qu’elle n’existe nulle part), objets de vérité (les idéalités mathématiques).» [http://arsindustrialis.org/vocabulaire-subsister-exister-consister](http://arsindustrialis.org/vocabulaire-subsister-exister-consister)

[^12]: Ars Industrialis, Vocabulaire, *Transindividuation* «&#8239;Chez Bernard Stiegler, le transindividuel est ce qui, à travers la co-individuation diachronisante des je, engendre la trans-individuation synchronisante d’un nous. Ce processus de transindividuation s’opère aux conditions de métastabilisation rendues possibles par ce que Simondon appelle le milieu préindividuel, qui est supposé par tout processus d’individuation et partagé par tous les individus psychiques. Ce milieu préindividuel est cependant, pour nous, intrinsèquement artefactuel, et la technique est ce dont le devenir métastabilise la co-individuation psychique et collective.&#8239;» [https://arsindustrialis.org/vocabulaire-ars-industrialis/transindividuation](https://arsindustrialis.org/vocabulaire-ars-industrialis/transindividuation)
