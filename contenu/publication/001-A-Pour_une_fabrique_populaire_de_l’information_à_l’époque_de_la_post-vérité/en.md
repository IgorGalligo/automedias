---
title: An information factory for the masses in the post-truth era
subtitle: n°6 Call for papers

flag: "[Cahiers COSTECH ](http://www.costech.utc.fr/CahiersCOSTECH/)"

type: article
locale: en

author: 
  - Igor Galligo
  - Ludovic Duhem
  - Édouard Bouté
description: 
date: 2025-12-13
tags:
  - masses/populaire
  - journalism
  - automedia
  - post-truth
  - truth regime
  - mediactivist
  - mediartivism
  - individuation
  - participatory/contributory design
  - media community
  - digital capitalism

linked_publications:
  - 002
  - 003
  - 004
  - 005
  - 006

# Bibliographie de l'article
# La bibliographie est une liste. Chaque entrée est introduit par un retrait (deux espaces) et un tiret.
# Il est possible de segmenter la bibliographie par catégorie. Il convient alors de renseigner un titre (- title: MON TITRE). L'ensemble des références placer à la ligne et respectant un retrait supplémentaire (deux espaces) seront inclut dans ce sous-groupe
# Si les références contiennent des doubles points (ou colons), il convient remplacer ce symbole par l'expression &#58; ou de placer la référence entre "guillemet"
bibliography: 
  - title: On automedia, populaire media or independent media
    references:
      -  Arquembourg, J. (2011). L'événement et les médias. Les récits médiatiques des tsunamis et les débats publics (1755-2004). Paris, Éd. des Archives contemporaines.

      -  Bourdieu, P. (1992(1998). Les règles de l’art. Genèse et structure du champ littéraire. Paris, Ed. du Seuil.

      -  Cardon, D., Granjon, F. (2013). Médiactivistes. Paris, Presses de Sciences Po.

      -  Ferron, B. (2010). Des médias de mouvements aux mouvements de médias&#58; Retour sur la genèse du «&#8239;Réseau Intercontinental de Communication Alternative&#8239;» (1996-1999). Mouvements, 61(1), 107-120. <https://doi.org/10.3917/mouv.061.0107>  

      -  Lévêque, S., Ruellan, D. (2010). Journalistes engagés. Presses universitaires de Rennes, séries «&#8239;Res Publica&#8239;».

      -  Souillard, N., Sebbah, B., Loubère, L., Thiong-Kay, L., Smyrnaios, N. (2020). Les Gilets jaunes, étude d'un mouvement social au prisme de ses arènes médiatiques. Terminal, "Les groupes minoritaires et / ou marginalisés à l'ère numérique &#58; pratiques de mobilisation, changements sociopolitiques et transformations identitaires URL &#58; <http://journals.openedition.org/terminal/5671>&#8239;; DOI &#58; <https://doi.org/10.4000/terminal.5671>  

      -  Thiong-Kay, L. (2020, à paraître). L'automédia, objet de luttes symboliques et figure controversée. Le cas de la médiatisation de la lutte contre le barrage de Sivens (2012-2015)&#8239;», Le Temps des Médias.

      -  Carlino, V., (2020). «&#8239;Vidéo en ligne et contestation politique radicale&#58; Entre intégration aux pratiques militantes et critique des plateformes&#8239;». Terminal, no 127. <https://doi.org/10.4000/terminal.5806>

      -  Fuchs, C. (2014). OccupyMedia \!&#58; the occupy movement and social media in crisis capitalism. Winchester, UK, Royaume-Uni de Grande-Bretagne et d’Irlande du Nord&#58; Zero Books.

      - Mabi, C., (2016). «&#8239;Luttes sociales et environnementales à l’épreuve du numérique &#58; radicalité politique et circulation des discours&#8239;». Études de communication, no 47&#58; 111-30. <https://doi.org/10.4000/edc.6659>

      - Milan, S., (2015). «&#8239;When Algorithms Shape Collective Action&#58; Social Media and the Dynamics of Cloud Protesting&#8239;». Social Media + Society 1 (2)&#58;

      - Nez, H., (2015). «&#8239;Des « informateurs citoyens ». Usages des images par les Indignés espagnols&#8239;». Sciences de la société, no 94&#58; 139-54. <https://doi.org/10.4000/sds.2507>

      - Riboni, U.L.. (2015). «&#8239;Filmer et rendre visible les quartiers populaires dans la Tunisie en révolution&#8239;». Sciences de la société, no 94&#58; 121-36. <https://doi.org/10.4000/sds.2487>

      - Bortzmeyer, G. (2016) «&#8239;Révolution, touche replay&#8239;», Vacarme, vol. 77, no. 4, pp. 74-83.

      - Lovink G., Garcia D. (1997), On the ABC of Tactical Media & Workspace Manifesto [https://networkcultures.org/geert/2015/03/17/on-the-abc-of-tactical-media-workspace-manifesto-1997/?pdf=1112](https://networkcultures.org/geert/2015/03/17/on-the-abc-of-tactical-media-workspace-manifesto-1997/?pdf=1112)

      - Lovink, G. (2005) Tactical Media, the Second Decade, Brazilian Submidialogia

      - Lovink, G. (2007) Zero Comments&#58; Blogging and Critical Internet Culture, Routledge, London and New York

  - title: On the crisis in journalism and news media
    references:
      - Jost, F. (2020). Médias &#58; sortir de la haine. Paris, CNRS Editions.

      - Bronner, G. (2020). La démocratie des crédules. Paris, PUF.

      - Accardo, A. (2017). Pour une socianalyse du journalisme, considéré comme une fraction emblématique de la nouvelle petite bourgeoisie intellectuelle. Paris, Agone.

      - Kovach, B., Rosenstiel, T. (2004). Principes du journalisme, ce que les journalistes doivent savoir, ce que le public doit exiger. Trad. Monique Berry, Paris, Gallimard.

      - Ramonet, I. (2011). L'Explosion du journalisme. Des médias de masse à la masse de médias. Paris, [Éditions Galilée](https://fr.wikipedia.org/wiki/%25C3%2589ditions_Galil%25C3%25A9e).

      - Plenel, E. (2018). La valeur de l'information. Paris, Don Quichotte,.

      - Plenel, E. (2020). La sauvegarde du peuple. Presse, liberté et démocratie. Paris, [La Découverte](https://fr.wikipedia.org/wiki/La_D%25C3%25A9couverte).

      - Frau-Meigs, D. (2019). Faut-il avoir peur des fake news&#8239;? Paris, La documentation française.

      - Zuckerman, E. (2019). Média polarization, «&#8239;à la française&#8239;»&#8239;? Comparing the French and American ecosystems, rapport juin pour l’Institut Montaigne. <https://www.institutmontaigne.org/publications/media-polarization-la-francaise>

      - Charron, J.-M. (2017). Pour combattre la post-vérité, les médias condamnés à innover, INA, la revue des médias, 21 avril – Mis à jour le 12 mars 2019. <https://larevuedesmedias.ina.fr/pour-combattre-la-post-verite-les-medias-condamnes-innover>

      - Charron, J.-M. et Papet. J. (dir.) (2016). Le Journalisme en questions &#58; Nouvelles frontières des médias et du journalisme. Paris, L’Harmattan.

      - Dewey J., (2010). Le public et ses problèmes, Paris. Gallimard, collection Folio Essais.

      - Castells, M. (1998, 1998, 1999, réédition en 2001)). L'Ère de l'information. Vol. 1, 2 et 3, Paris, Fayard

  - title: On conspiracy and post-truth 
    references:
      - Lordon, F. (2015). Vous avez dit «&#8239;complot&#8239;», le symptôme d’une dépossession, Le Monde diplomatique, juin.

      - <https://www.monde-diplomatique.fr/2015/06/LORDON/53070>

      - Lordon, F. (2017). Le complotisme de l’anticomplotisme, Le Monde diplomatique, octobre. <https://www.monde-diplomatique.fr/2017/10/LORDON/57960>

      - Lordon, F. (2020). Paniques anticomplotistes, Le Monde diplomatique, La pompe à phynance, blog du 25 novembre, <https://blog.mondediplo.net/paniques-anticomplotistes>

      - Watzlawick, P. (1978). La réalité de la réalité, confusion, désinformation, communication. Trad. Edgar Roskis, Paris, Seuil.

      - Stiegler, B. (2018). Qu’appelle-t-on panser? 1.L’Immense Régression. Paris, Les liens qui Libèrent.

      - Zuckerman, E. (à paraître). [Mistrust&#58; Why Losing Faith in Institutions Provides the Tools to Transform Them](https://www.amazon.fr/Mistrust-Institutions-Provides-Transform-English-ebook/dp/B0858G5YN8/ref=sr_1_1?dchild=1&qid=1608737386&refinements=p_27%253AEthan+Zuckerman&s=books&sr=1-1), W.W.Norton & Company.

      - Arendt, H. (1954). Vérité et politique, in La Crise de la culture. Trad. C. Dupont et A. Huraud, Paris, Gallimard, 1972.

      - Cassin, B. (2018). Quand dire, c’est vraiment faire, Homère, Gorgias et le peuple arc-en-ciel. Paris, Fayard.

      - Revault d’Allonnes, M. (2018). La faiblesse du vrai, ce que la post-vérité fait à notre monde commun. Paris, Seuil.

      - Williams, B. (2006).Vérité et véracité. trad. Jean Lelaidier, Paris, Gallimard.

      - Origgi, G. (2008). Qu’est-ce que la confiance&#8239;? Paris, Vrin.

      - Origgi, G. (2012). Democracy and trust in the Age of The Social Web, Séminaire II Seminario di Teoria Politica Aosta, 28-30 June&#58; [http://gloriaoriggi.blogspot.com/2012/06/epistemic-democracy-in-age-of-internet.html](http://gloriaoriggi.blogspot.com/2012/06/epistemic-democracy-in-age-of-internet.html)

      - Whitehead, A. N. (1995). Procès et réalité. Essai de cosmologie. Traduit de l'anglais par D. Charles, M. Elie, M. Fuchs, J.-L. Gautero, D. Janicaud, R. Sasso, A. Villani, Paris, Gallimard.

      - Stengers, I. (2002). Penser avec Whitehead. Une libre et sauvage création de concepts, Paris, Seuil.

      - Stengers, I. (2020). Réactiver le sens commun, lecture de Whitehead en temps de débâcle, Paris, La découverte, coll. Les empêcheurs de penser en rond.

      - Lamotte, M.&#8239;; Le Caisne L.&#8239;; Le Courant S. (2019). Fake news, mensonges & vérités, Le monde commun, des anthropologues dans la cité, Clamecy, PUF, Humensis.

      - Foucault, M. (2009). Le courage de la vérité, le gouvernement de soi et des autres 2, Cours au Collège de France. 1984, Paris, EHESS, Gallimard, Seuil. 

      - Gros, F. Michel Foucault, une philosophie de la vérité En ligne. URL &#58; <http://1libertaire.free.fr/IntroPhiloFoucault.html>

      - Foucault, M. (1979-1980, réed. 2012) &#58; Du gouvernement des vivants – Cours au Collège de France, Gallimard, Seuil, coll. «&#8239;Hautes études&#8239;», Paris.

  - title: On digital democracy 
    references:
      - Allard, L. (2018). Des mobiles et des apps &#58; 50 nuances d'image, Paris, Armand Colin.

      - Nova, N. (2020). Smartphones &#58; une enquête anthropologique, Genève.

      - Cardon, D. (2015). À quoi rêvent les algorithmes. Nos vies à l'heure des big data. Paris, Seuil, La République des idées.

      - Smyrnaios, N. (2017). Les GAFAM contre l'internet, Paris, Ina éditions,.

      - Ertzscheid, O. (2017). L'appétit des géants. Pouvoir des algorithmes, ambition des plateformes, Caen, C\&F Editions, mai.

      - Tufecki, Z. (2019). Internet et les gaz lacrimogènes, Forces et fragilités de la contestation connectée, Paris, C\&F éditions.

      - Turner, F. (2013). Aux sources de l'utopie numérique &#58; De la contre-culture à la cyberculture. Trad. de l'anglais, Caen, C\&F Editions.

      - Stiegler, B. (2012). États de choc - Bêtise et savoir au XXIe siècle, Paris, Fayard/Mille et une nuits.

      - Srnicek, N. (2018). Capitalisme de plateforme, l’hégémonie de l’économie numérique, Montréal, LUX.

      - Rouvroy, A.&#8239;; Berns, T. (2013). Gouvernementalité algorithmique et perspectives d'émancipation. Le disparate comme condition d'individuation par la relation&#8239;? Réseau n°177, p. 163 à 196 <https://www.cairn.info/revue-reseaux-2013-1-page-163.htm>

      - Lovink G. Rossiter N., (2018) Organization after social media, Minor Compositions, Colchester, NewYork.

  - title: On individuation, subjectification and contribution
    references:
      - Stiegler, B. (2013). Pharmacologie du Front National. Paris, Flammarion.

      - Stiegler, B. (2013). De la misère symbolique. Paris, Flammarion.

      - Stiegler, B. (2015). La Société automatique &#58; 1. L'avenir du travail. Paris, Fayard.

      - Stiegler, B. (2016). Dans la disruption, comment ne pas devenir fou&#8239;? Paris. Les liens qui Libèrent.

      - Stiegler, B. (2020) (dir.) Bifurquer, il n’y a pas d’alternative. Paris, Les liens qui Libèrent.

      - Barthélémy J-H. (2005), Penser l’individuation, Paris, L’Harmattan.

      - Simondon, G. (2012). Du mode d’existence des objets techniques. Paris, Flammarion.

      - Simondon, G. (1989). L'individuation psychique et collective. Paris, Aubier. 

      - Simondon, G. (2013). L'individuation à la lumière des notions de formes et d'information. Paris, Jérôme Million.

      - Foucault, M. (1994). Dits et écrits, volume III, Paris, Gallimard.

      - Agamben, G. (2006). Qu’est-ce qu’un dispositif&#8239;? Paris, Payot et Rivages.

      - Lordon, F. (2015). Imperium, Structures et affects des corps politiques, Paris, La fabrique.

      - Bourdieu, P. (1992). Les règles de l’art, genèse et structure du champ littéraire, Paris, Seuil.

      - Castoriadis, C. (1975). L'Institution imaginaire de la société&#8239;; Paris, Seuil.

      - Beuys, J., (1998), Par la présente je n’appartiens plus à l’art, l’Arche, Paris

  - title: On participatory design and media science 
    references:
      - Huyghe, P. D. (2014). A quoi le tient le design&#8239;? Paris, De l’incidence éditeur.

      - Citton, Y. (2014). Pour une écologie de l’attention. Paris, Seuil.

      - Citton, Y. (2017). Médiarchie, Paris, Seuil.

      - Flusser, V. (2006). La civilisation des médias, Trad. Claude Maillard, Belval, Circé.

      - Mersch, D. (2018). Théorie des médias, une introduction, Les presses du réel.

      - Citton, Y. et Doudet, E. (dir.) (2019). Ecologies de l’attention et archéologie des médias . Grenoble, UGA éditions,.

      - Turner, F. (2016). Le Cercle démocratique. Le design multimédia, de la Seconde Guerre mondiale aux années psychédéliques, C\&F Editions.

      - Van der Berg, K.&#8239;; Jordan, Cara M.&#8239;; Kleinmichel, Philipp. (2019). The art of Direct Action Social Sculpture and Beyond&#8239;; Berlin, Sternberg Press.

      - McCarthy J., Wright P., (2015) Taking (a)part. The politics and aesthetics of participation in experience-centered design, MIT Press

      - Manzini E., (2015) Design. When Everybody Designs, MIT Press

      - Zask J., (2011) Participer, Essai sur les formes démocratiques de la participation, Paris, La Découverte.

      - Godbout T. J., (2014) La participation contre la démocratie, Montréal, Liber

      - Duhem L., Rabin K., (2018) Design écosocial. Convivialité, pratiques situées, nouveaux communs, Fauconey-et-la-mer,

      - Duhem L., Pereira de Moura R., (2020) Design des territoires. L’enseignement de la biorégion, Paris, Eterotopia.

      - Stiegler B., (2008) Le design de nos existences à l’époque de l’innovation ascendante, Paris, Mille et une nuits/Centre Pompidou

      - Banz C., Krohn M., (2018) Social Design &#58; Participation and Empowerment, Zurich, Lars Muller Publishers.

      - Bishop C. (2012) ARTIFICIAL HELLS, Participatory Art and the Politics of Spectatorship, Verso, London

      - Lovink, G. (2019), Sad by design, on Platform Nihilism, Pluto press, London

      - Eude, E. Maire V. (sous la direction de) (2018) La Fabrique a Ecosysteme&#58; Design, territoire et innovation sociale, Loco, Paris.

  - title: On the sociology of the mass/classes populaires
    references:
      - Siblot, Y., Cartier, M., Coutant, I., Masclet, O. et Nicolas Renahy, N. (2015). Sociologie des classes populaires contemporaines&#8239;; Paris, Armand Colin

      - Schwartz, O. et Masclet, O., (2019) «&#8239;Les classes populaires, c'est les pauvres&#8239;», dans Olivier Masclet, Séverine Misset et Tristan Poullaouec, La France d'en bas&#8239;? &#58; idées reçues sur les classes populaires, [Éditions du Cavalier bleu](https://fr.wikipedia.org/wiki/Éditions_du_Cavalier_bleu).

      - Schwartz, O., (1998) La notion de «&#8239;classes populaires&#8239;», habilitation à diriger des recherches en sociologie, [Université de Versailles-Saint-Quentin-en-Yvelines](https://fr.wikipedia.org/wiki/Université_de_Versailles-Saint-Quentin-en-Yvelines).

      - Bourdieu, P., (1979) [La Distinction. Critique sociale du jugement](https://fr.wikipedia.org/wiki/La_Distinction._Critique_sociale_du_jugement), Paris, [Les Éditions de Minuit](https://fr.wikipedia.org/wiki/Les_Éditions_de_Minuit).

      - Pasquier, D., (2018) [L'Internet des familles modestes. Enquête dans la France rurale](https://journals.openedition.org/lectures/17015), Paris, Presses des Mines, coll. «&#8239;Sciences sociales&#8239;».


# Cette section permet de publier un contenu complémentaire à l'article, sous la forme de sections disctincte en fin de page. 
# le contennu de ces section peut être rédigé en markdown
aside:
  - title: Scientific Committee
    content: "
      - **Yves Citton** - Professor of Literature and Media, Université Paris 8 Vincennes-Saint Denis, co-director of the journal Multitudes.

      - **Dominique Cardon** - Director of the MediaLab Sciences Po Paris, Professor of Digital Sociology, Member of the prospective committee of the CNIL.

      - **Joëlle le Marec** - Professor of Information and Communication Sciences at the Natural History Museum in Paris, laboratory PALOC (Patrimoines Locaux, Environnement et Globalisation).

      - **Geert Lovink** - Director of the Institute of Network Cultures, Professor of Interactive Media à la Hogeschool van Amsterdam University of Applied Sciences, mediactivist and co-founder of the concept of *tactical medias.*

      - **Olivier Fillieule** - Director of Research at the CNRS, Political Institute of the University of Lausanne, CRAPUL (Centre de Recherche sur l'Action Politique), Professor of Political Sociology.

      - **Michelle Christensen** -- Visiting Professor for “Open science” chair at the Technische Universität Berlin and the Einstein Center Digital Future with Florian Conradi. Head of research group on “Critical Maker Culture” at the Universitat der Kunst in Berlin and the Weizenbaum Institute. Professor of design and sociology.

      - **Clément Mabi** - Deputy Director of COSTECH, the Research laboratory of Université Technologique de Compiègne, Lecturer in Information and Communication Sciences.

      - **Noël Fitzpatrick** - Dean of GradCAM (Graduate School of Creative Arts and Media), TU Dublin, Academic Lead of EUT Lab (European University of Technology Laboratory, Dublin), Professor of Philosophy.

      - **Laurence Allard** - Lecturer in Information and Communication Sciences at Université of Lille, Senior Researcher at IRCAV (Institut de Recherche sur le Cinéma et l'Audiovisuel), Université Paris 3 Sorbonne

      - **Jamie Allen** - Senior Researcher at Critical Media Lab, University of Applied Sciences and Arts Northwestern Switzerland, Basel, Artist.


    "

  - title: Project coordinator
    content: "
      - **Project leader&#160;:   
        Igor Galligo** (ArTeC, UTC Costech, Noödesign)

      - **Head of axis 1 : 
        Zakaria Bendali** (CRAPUL): 
        [Contact](Zakaria.Bendali@unil.ch)

      - **Head of axis 2 : 
        Edouard Bouté** (UTC Costech): 
        [Contact](edouard.boute@utc.fr)

      - **Head of axis 3 : 
        Igor Galligo**: 
        [Contact](democratienumeriquepopulaire@protonmail.fr)

      - **Head of axis 4 : 
        Ludovic Duhem** (ÉSAD Valenciennes): 
        [Contact](mailto:ludovic.duhem@esad-valenciennes.fr)
    "

  - title: File contributions
    content: " Three types of written contributions are expected for this research dossier, which will be published in issue 5 of [Cahiers Costech](https://www.costech.utc.fr/CahiersCOSTECH/), the digital scientific journal of COSTECH, [the research laboratory of the Université Technologique de Compiègne](http://www.costech.utc.fr):

      1.  Scientific articles dealing with the issues in the dossier and corresponding to one or more of the 4 research axes presented.

      *Between 15 and 20 articles are expected between 25,000 and 40,000 characters, spaces included.*

      2. Interviews with Automedia corresponding to the first axis presented.

      *Between 10 and 15 interviews are expected, with an average length of 25,000 characters, spaces included.*

      3.  In the second stage, we plan to offer researchers and Automedia the opportunity to co-write articles together, according to the mutual affinities and interests that may appear.

        *4, 5 articles are expected between 25,000 and 40,000 characters, spaces included*
        "

  - title: Applications 
    content: "
      Type 1 and 2 applications will be selected in January 2022 by the coordination team and the scientific committee.

      To apply, candidates are asked to send a document **of 3000 characters maximum** (including spaces) to the head of the axis responsible for the contribution, presenting **the problem** and **the objective** of the contribution as well as the **first name, the family name and the status of the candidate**.
    "

  - title: Calendar
    content: "
       1. Deadline for applications for **contributions 1 and 2**: 
       **January 23, 2022**

       2. Date of submission of texts for **contributions 1 and 2**: 
       **end of October 2022**
      
       3. Date of submission of texts for **contribution 3**: 
       **end of January 2022**


     Following this digital publication, a colloquium and workshops will be held over 3 days on 22, 23 and 24 June 2022 at the [Maison des Sciences de l'Homme Paris-Nord](https://www.mshparisnord.fr) to meet all the contributors, researchers and Automedia and to discuss the different contributions. This will also allow the production of type 3 contributions to begin. Finally, in a third phase, a paper publication is envisaged for the end of 2022** from the best contributions of all 3 types.   
    "
---

# Argument

If the institutionalized production of information and its mediatization remain primarily the work of professionals called "journalists", who respond to the values, criteria and protocols taught in journalism schools and set forth by journalistic charters (which does not preclude an observation on the diversity of journalistic genres and practices), it would appear that the democratization of the production and communication of information today is in the process of transforming their values ​​and mode of production. This is effected by the joint development and use of digital information and communication technologies (TNIC), GAFAMs, and alternative digital social networks (Discord, Mastodon, Telegram, etc.), as well as in France by the advent of mass socio-political movements calling for more democracy - along with ideological and sociological pluralism (Cardon and Granjon, 2013).

This shift is exemplified by the Yellow Vest movement in France (Ertzscheid, 2019) -- which shares many media-political characteristics with movements in other countries over the last fifteen years in other countries, such as the "5 Star" movement in 2009 in Italy, the "Arab Spring" in 2010, the "Alt-Right" movement in the United States in 2016 (concomitant with the victory of Donald Trump), or the insurrectionary uprising in Hong Kong against the Beijing government in 2019-2020. The Yellow Vest movement has led to the resurgence and development on the Internet of so-called *populaires*[^1] media through which numerous media-political experiments have been carried out -- whether individually or collectively -- independently of professional training, corporations or institutions of representation and legitimation. As evidenced by David Dufresne's film, "*Un pays qui se tient sage*"[^2], which compiles dozens of video sequences made by participants and spectators of the Yellow Vest demonstrations, the smartphone (and in particular, its video function) allows any individual to instantaneously reveal, relay, and publicize information of a political nature on the Internet. In this way, the smartphone in tandem with the use of digital social networks (RSN) has enabled the democratization of media production (Nova, 2020).

With the Yellow Vest movement, a powerful political actor has emerged in the field of democratic actions and experiences: the *automedia*. Whether it is the *self-media gesture* produced on the fly by a single individual with a smartphone, or the *collective auto-media enterprise* already seen in the field of democratic struggles and experiments (Thiong-Kay, 2020) which reinvents the forms of *tactical medias* (Garcia et Lovink, 1997) and of *mediactivists* (Cardon and Granjon, 2013), *automediation* refers to the self-production and self-communication of political information through the use or reinvention of digital communication devices and circuits. The amateur gesture, and the often community framework of automedia production, thus constitutes an extension of *maker culture* to the domain of media production.

However, the deployment of *automedia* is taking place today within a *post-truth*[^3] techno-economic context, which has appeared with the advent of *informational capitalism* (Castells, 1998, 1999a, 1999b) and the digitization of information on the Internet. This techno-economic development is determining new *digital information infrastructures* that tend to value information more for the *digital attentional capital* (Citton, 2017) that it holds than for its truth value; which strongly disrupts the *journalistic truth regime*[^4], along with the democratic function to which it should be devoted (Arendt, 1954 (1972); Revault d´Allonnes, 2018). According to Bernard Stiegler (2018), *neo-computational digital capitalism*, by *disrupting* the traditional circuits and processing of information, which were originally based on *veridiction, certification and verification*, today alters equally the production of information by journalistic organizations and its reception by various publics, provoking reactions of distrust, mistrust, and even hatred towards journalistic institutions, then accused of betraying their deontologie (Jost, 2020).

As a result, individuals (sometimes) give more credit to information provided by a loved one - such as an automedia creator with whom common affects are shared - than to information produced and verified (*"fact-checked*") by a journalistic institution[^5]. The result is a crisis in journalistic institutions and organizations as well as a substitution of journalistic productions by those of the automedia as a source of information, especially for *populaire publics*, but without them formulating and referring to what we might call: *an automedia truth regime*. Thus, the techno-economic context of *post-truth today*, under the reign of GAFAM, promote the explosive growth of the automedia genre (in particular on YouTube); by flouting any *criteriology of law and any domination of the law over the fact* (Stiegler, 2018); which in return, constitutes - and often in spite of itself - as an actor of *post-truth*.

It is crucial, however, not to reduce the automedia genre to "gafamized" forms out of which techno-economic designs and circuits for the production and reception of information are now developing, and in within which a large number of automedia are currently constrained. Although it is the dominant figure of the *YouTuber,* which today crystallizes the techno-economic subjectivation of the automedia genre, and while certain detractors, claiming loyalty to journalistic ethic, we postulate, on the contrary, that this binary is not valid if we distinguish the automedia genre from the techno-economic context within which it emerges today.

Indeed, the conquest of the *autonomy of automedia* depends not only on that of their freedoms of actions (or enterprises) and their financial independence - problems that have already been widely discussed within the journalistic community (Ramonet , 2011; Plenel, 2018, 2020, etc.) - but also on their ability to reinvent, through automedia itself, the technological and economic infrastructures upon which they depend, as well the values, norms, and protocols of information production, in order *to establish their democratic and scientific necessities and legitimacies.* It is therefore not only a question of making political use of a commercialized smartphone technology, but of conceiving and producing new techno-political *puissances* (Lordon, 2015) through collective projects in the realms of media, technology, and politics. The category of *independent media* is therefore likely to evolve into that of *automedia*, in the literal and exhaustive sense of the term -- "auto", meaning "by oneself" -- including from a *mediartivist* (Citton, 2017) perspective, *the* *auto-design of information production and communication circuits.*

If, as Jean-Marie Charon asserts, "the reclamation of trust does not occur only through the exercise of verification[^6]", we wish to test the hypothesis that the production of truth, and the conditions of its credibility with a mass ("*populaire"*) public, can be reconfigured in a new digital and economic design by the automedia genre. The aim of this dossier will be to propose a reflection on the epistemological, economic, political and technological foundations and models of a new *automedia truth regime*, which not only promotes the democratization of the production and communication of information, but also the reinvention of the value of *informational truth*, as well as that of *informational credibility*, *from* and *with* mass public - that is to say, the reinvention of the relationships between media, truth and democracy, through and by automedia genre.

How have *digital infrastructures* transformed the *maker* practices of *tactical media* and *mediactivism* to give birth to the *participatory turn* and then to the automedia genre? To what techno-economic *power* *dispositives* (Foucault, 1994) are automedia currently subjected and constrained, in the experimentation of new automedia individuations? What are the new media-political values, norms and protocols are carried and fabricated by automedia? How can we redesign the production of information through participatory / contributory processes and circuits *to produce trust and truth in information within the masses*?

In order to address these questions, we invite contributors to take part to one of the following four areas of study:

## 1/ Automedia: descriptions and typologies

*How have digital infrastructures transformed the maker practices of tactical media and mediactivism to give birth to the participatory turn, and then to the automedia genre?*

The work in this first axis of research will help to provide an empirical and phenomenological panorama of automedia based on an analysis of (infra-) structures, individualities and automedia organizations existing in France. The contributions will be based on interviews with automedia to feed this first axis of research. The goal will be to deepen a reflection on their process of individuation from the potentialities offered by digital infrastructures, and in particular the *"participatory turn"*. The fields of *Information and communication sciences, political sciences and media sociology* could be called upon for this *descriptive and typological objective*. The digital communication infrastructures upon which automedia are now developing allow for a great diversity of automedia practices and will constitute the basic materials necessary for the development of this panorama. The following distinctive factors may be considered:

-   Digital recording equipment (sound, video)

-   Digital creation material for text, sound or video editing.

-   The digital platforms or social networks used (YouTube, Discord, Twitch, Facebook, Twitter, Mastodon, Peer-Tube, etc.)

-   The mode of expression (text, video, sound)

-   The expression format (editorial, zapping, interview, survey, analysis; direct or deferred improvised or reading from a teleprompter, etc.)

-   The recording context (in the studio or outdoors)

-   The mode of financing (self-financing, paid access to users, commission on advertising, crowdfunding (using which digital tools?), public financing, etc.)

-   The scale of investigation (local, national, etc.)

-   The relationship to a territory or a community (claiming a scale or a territorial or community culture.)

-   The relationship to a political ideology

-   The initiative of the project (individual or collective)

-   The relationship to collective or participatory creation

-   The process of information creation (production of information from interviews or the field surveys, or analyzes of texts, videos, journalistic surveys).

-   Sociological profile of automedia

-   Sociological profile of the public

-   Personal trajectories of automedia

-   Relationships of interdependence

-   Relationships of influence

-   Mapping of the automedia built by the automedias themselves

-   Oppositional or influential relationships with journalism

-   Interactions with the public

## 2/ Automedia in the face of digital capitalism: an analysis of a techno-economic balance of power.

*To what techno-economic power dispositives are automedia today subject and constrained in the experimentation with new automedia individuations?*

We invite works within this axis to describe and analyze the techno-political power relations and technological dispositives that constrain automedia individuations[^7] in their expression and communication processes. These designate the process of individuation and singularization of automedia, in that automedia is always constituted by an individual or a collective of individuals in relation to a public to whom it is addressed and with whom it communicates, and by devices, apparatus or technical prostheses that allow it to record and broadcast sound and video streams. However, these *automedia individuations* are caught in *dispositives of power* (Foucault, 1994) that frame, orient or hinder their projects in order to constitute *automedia subjectivations*[^8]. Indeed, automedia today are caught up in power struggles, sometimes warlike[^9], which work and act in depth on their dynamics of individuation. These *dispositives* *of power* then transform them into dynamics of subjectivation, that is to say into media whose individuation is subject to dominant technological and economic actors. In the context of our reflection on *the technological infrastructures of post-truth*, we wish to interrogate a new relationship of forces that confronts the automedia individuations with the immense infrastructures of private technologies that supports their expression and communication. Through their subjection to various regulations, designs and algorithms enacted and produced by these digital platforms (YouTube, Twitter, Facebook, etc.) - which respond notably to the values ​​and logic of *digital capitalism* (Srnicek, 2018), and consequently to the mechanisms of *grammatization* (Citton, 2014), *neo-computationalism* (Stiegler, 2013, 2018) *and algorithmic governmentality* (Rouvroy, Berns, 2013) - automedia are confronted with both a struggle against these infrastructures, in defense of the free of expression and communication of the information they produce ; and with an attentional war to reveal certain (type of) information, along with struggles to guarantee its truth and credibility. The challenge of this second axis of research will consist in describing the technological mechanisms and constraints exerted by the major platforms of digital capitalism on the challenges of attention, freedom, truth and credibility of automedia.

## 3/ New values, norms and automedia protocols

*What are the new media-political values, norms and protocols carried and fabricated by automedia?*

The *post-truth* context, consolidating itself from the second half of the 2010s, now seems to renew certain distinctions between journalism and automedia, against the backdrop of sometimes violent conflicts between their members. Indeed, many journalistic institutions accuse automedia of being the authors or relays of *conspiratorial* theses. Some of these theses denounce the control of the productions of journalistic organizations by the capitalist companies that finance them, and deduce from that, different processes of corruption of journalistic ethics that in particular mask the choices of ideological orientations of the information or journalistic investigations[^10]. In return, the conspiracy accusations brought to bear, by certain journalistic institutions, upon the production of *automedia fake news,* or the alleged "debility" of the analyses produced by certain automedia cast in a discredit on the whole of the automedia genre as a whole, which is accused of lacking objectivity, impartiality, or even the distinction between *fact* and *opinion*. This attack in turn generates an internal struggle among some automedia to convince their critics of the truth and credibility of the information they produce or transmit to their publics. It should be remembered that in terms of democratic normativity, the enunciation of factual truths is considered a necessary value for informing *public opinion* and *public education*. (Arendt, 1954 (1972); Revault d´Allonnes, 2018). Automedia, as new actors in the democratic struggle, are therefore called upon to guarantee the importance of thruth in their output. This struggle between journalism and automedia thus allows the reworking of alternative media values ​​at the same time as the constitution of new media-political communities. Driven by democratic ambitions and stifled by the aforementioned techno-economic balance of power, this axis aims to highlight the effort of normative creation of automedia on the values, norms and protocols underlying the production of their information - distinctly from certain journalistic standards. Following the example of *outsiders* in the literary field -- described by Pierre Bourdieu in terms of the logics of emancipation and empowerment of literary modernity in his work "The rules of art, genesis and structure of the literary field" (Bourdieu, 1992) - automedia seem to us, today, destined to (co-) write the story of their *subsistence*, their *existence* and their *consistency*[^11] (Stiegler, 2013) through a comparative dialogue with the principles, organizations and practices of journalism.

## 4/ Participatory design, towards new information production circuits for automedia

*How to redesign the information factory through participatory/contributory processes to produce trust and truth in information within the masses?*

The question of *design* is not a peripheral or superficial question for automedia individuations. The corresponding techno-economic balance of power stated above supposes a certain conception of information, in the quadruple sense of the idea of ​​what it should be, of the form it should take, of the way of producing it, and of the use that can be made of it. But more concretely, through the dominant digital platforms, it is a whole standardized design (by a consumerist and computational logic), an automated production (by algorithmic power), a formatted aesthetic (by an authoritarian graphics and a closed interface) and controlled use (by restrictions, bans and data collection) which is necessary without participation in the conditions of production and communication of information. This last axis, is therefore concerned not only questioning the effect of the information design upon the information itself -- that is to say, the effect produced by the *technical milieu* (Simondon, 2012) upon the nature, perception, meaning and use of information -- but above all, with understanding why participation in information design is necessary to meet the challenges of truth and credibility associated with the fight against post-truth.

Beyond aesthetic issues as well as praxeological or pragmatic issues, participatory design poses a clearly *political* question : on the one hand regarding the capacity of individuals in a democracy to deliberate upon the means and ends of the production of information (Zask, 2011); and on the other hand, with regard to the recognition of those *excluded* from participation, and to the inclusion of opponents of participation as a watchword to challenge.

The participatory design of information, as a democratic production, is in this respect "pharmacological" in the sense of Stiegler, and consequently requires an in-depth examination of its *toxicity* for automedia individuations, on the side of producers as receptors of the media information that *proletarizes* by losing their reciprocal knowledge (recording, analyzing, interpreting, criticizing) whereas it should be the support and the model of a collective individuation or more precisely the place of a *process of transindividuation*[^12]. A criticism that can be made in particular through the work of Claire Bishop on 'participatory art' (Bishop, 2012). Correlatively, this pharmacology of automedias would make it possible to assess and test the relevance of the *contributory* paradigm -- as opposed to the participatory paradigm -- for the development of an information design understood as a "*populaire" science and factory of information in the post-truth era*. It would thus be a question of *co-designing* and experimenting with automedia *contributory forms* of a new *territorialized design of information* fighting against distrust - even mistrust - resulting from proletarization and deterritorialization (engendered by the algorithmic design of media and automedia platforms). For this purpose, the new technologies of Open Source Intelligence (OSINT) may be considered. This fight takes place in particular through a process of appropriation of technologies by the inhabitants of the territories who transform them in return at different scales of the locality (for example from the district to the village, from the metropolis to the region or even to the biosphere.). The practices of appropriation, diversion, even invention thanks to new forms of localized or reterritorialized collective intelligence are in this sense important to study in this area. But beyond the known democratic stakes of participatory design, the key issue of this axis will above all evaluate the relevance of its paradigm to rebuild truth and confidence in information, when we are invited, as a citizen, to take part in its design.

[^1]: Siblot, Y., Cartier, M., Coutant, I., Masclet, O. and Nicolas Renahy, N. (2015). *Sociologie des classes populaires contemporaines* ; Paris, Armand Colin. As Yasmine Siblot and her co-authors show, a sociology of the "*populaires"* classes has developed in France over the past twenty years. Following on from a long tradition of work focused on the workers' group, this sociology intends to confront a reaffirmed ambition of analyzing social classes with the profound changes that have affected society for the past thirty years. In this program, following the reflections of Olivier Schwartz, the notion of *"populaire"* can be seen as an heir to a sociology of culturalist orientation popularized in France in the 1970s. Each in its own way, the works of Richard Hoggart and Pierre Bourdieu then marked a break with the dominant workerist and industrialist approaches, and opened the analysis to the lifestyles of the *"populaire".* In this vein, the clarification proposed by Olivier Schwartz poses two strong characteristics of the "*populaires"* classes: on the one hand a dominated situation in social space, on the other hand a relative cultural autonomy.

[^2]: David Dufresne*, Un pays qui se tient sage*, documentary film, 86 minutes, Le Bureau, Jour 2 fête, France, 2020

[^3]: The word *post-truth* was defined in 2016 by the Oxford Dictionary to mean:

    "To circumstances in which objective facts have less influence on public opinion than those which appeal to emotion or personal beliefs. \[The emergence of 'post-truth' in language has been\] fueled by the rise of social media as a source of information and by the growing distrust of the facts presented by the establishment ". Oxford University Press, Word of the year 2016, Post-truth : [[https://languages.oup.com/word-of-the-year/2016/]{.underline}](https://languages.oup.com/word-of-the-year/2016/)

[^4]: "Basically, if you will, a regime of truth is what determines the obligations of individuals with regard to the procedures for manifesting the truth \[\...\]. We speak of political regime \[...\] to designate in short all the processes and institutions by which individuals find themselves engaged, in a more or less pressing manner \[...\]. We can \[also\] speak of a penal regime, for example, to designate the set, again, of the processes and institutions by which individuals are engaged, determined, and forced to submit to laws of general scope. So, in these conditions, why indeed not speak of a regime of truth to designate all the procedures and institutions by which individuals are engaged and forced to perform, under certain conditions and with certain effects, well-defined acts of truth ? \[...\] The problem would be to study the regimes of truth, that is to say the types of relation which link the manifestations of truth with their procedures and the subjects who are the operators, the witnesses or possibly the objects. \[...\] " Foucault, M. (*1979-1980) : Du gouvernement des vivants -- Cours au Collège de France*, Gallimard, Seuil, 2012, coll. « Hautes études », Paris, p. 91-92 et 98-99.

[^5]: Jost, F. (15.10.09) *Médias : de la méfiance à la haine*, AOC, [[https://aoc.media/opinion/2019/10/15/medias-de-la-mefiance-a-la-haine/]{.underline}](https://aoc.media/opinion/2019/10/15/medias-de-la-mefiance-a-la-haine/)

[^6]: Charon, J.M., *Pour combattre la post-vérité, les médias condamnés à innover,* INA, la revue des médias, 21 avril 2017 - Mis à jour le 12 mars 2019 [[https://larevuedesmedias.ina.fr/pour-combattre-la-post-verite-les-medias-condamnes-innover]{.underline}](https://larevuedesmedias.ina.fr/pour-combattre-la-post-verite-les-medias-condamnes-innover)

[^7]: **Individuation** is defined by Bernard Stiegler in his reading of Simondon as "\[\...\] the formation, at the same time biological, psychological and social, of the individual which is still incomplete. Human individuation is threefold, it is an individuation with three strands, because it is always at the same time psychic ('I'), collective ('us') and technical (this milieu which links the 'I' to the 'us'). ', concrete and effective milieu, supported by mnemonics).

    See <https://arsindustrialis.org/individuation>

[^8]: For Giorgio Agamben, "the device is \[indeed\], above all, a machine that produces subjectivations and that is why it is also a machine of government". See Agamben, G., (2006), *Qu'est-ce qu'un dispositif ?* Payot et Rivages, Paris, p.42

[^9]: Recall that Jérôme Rodrigues, a major actor in the Yellow Vest movement, was blinded on December 28, 2019 by a shot from Defense Ball Launcher (LBD) triggered by an agent of the forces of Order when his attention left the theater physical operations on Boulevard Magenta in Paris, where he was, to post on Twitter a video footage he had just taken with his smartphone, showing the warlike atmosphere of the situation.

[^10]: Lordon, F., (2020). *Paniques anticomplotistes,* Le Monde diplomatique, La pompe à phynance, blog of November 25, <https://blog.mondediplo.net/paniques-anticomplotistes>

[^11]: Ars Industrialis, Vocabulaire, *Subsister, Exister* et *Consister*: "**Subsistence** is the immutable order of needs and their imperative satisfaction, it is the imperative of survival. When human life is reduced to the pure necessity of subsistence, it is reduced to its needs and loses the feeling of existing. Such needs are now artificially produced by marketing. **Existence** - the fact for man of ex-sistere: of being projected out of himself, of constituting himself outside and to come - is what constitutes that which exists in and through the relation he maintains to his objects not in so far as he needs them, but in so far as he desires them. This desire is for a singularity - and all existence is singular. **Consistency** designates the process by which human existence is moved and transformed by its objects, where it projects what exceeds it, and which does not exist however, consists - thus of the object of its desire, which is by infinite definition however that the infinite does not exist: exists only what is calculable in space and in time, that is to say what is finite. Such infinities are the objects of idealization in all its forms: objects of love (my love), objects of justice (justice which no one can renounce on the pretext that it exists nowhere), objects of truth (mathematical idealities)." <http://arsindustrialis.org/vocabulaire-subsister-exister-consister>

[^12]: Ars Industrialis, Vocabulaire, *Transindividuation* "With Bernard Stiegler, the transindividual is what, through the diachronizing co-individuation of the I's, generates the synchronizing trans-individuation of an us. This process of transindividuation takes place under the conditions of metastabilization made possible by what Simondon calls the preindividual environment, which is assumed by any process of individuation and shared by all psychic individuals. This pre-individual environment is however, for us, intrinsically artefactual, and technique is what becoming metastabilizes psychic and collective co-individuation." <Https://arsindustrialis.org/vocabulaire-ars-industrialis/transindividuation>
