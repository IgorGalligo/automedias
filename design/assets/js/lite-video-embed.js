/*
* This Library ehence progressively <a data-video-embed > elements. 
* Once clicked, linked video is embded.
* No Tiers JS loaded otherwise.
* 
* Inspiration : 
* https://gomakethings.com/how-to-lazy-load-youtube-videos-with-vanilla-javascript/
* https://github.com/paulirish/lite-youtube-embed (But accessible without JS)

**/


/**
 * Handle click events on the video thumbnails
 * @param  {Event} event The event object
 */
function peertubeClickHandler(event) {


  const parser 			= new DOMParser(),
  			dataIframe	= event.getAttribute('data-iframe'),
  		 	iframe 			= parser.parseFromString(dataIframe, "text/html"),
  			sandbox			= iframe.querySelector("iframe").sandbox
	let 	src					= new URL (iframe.querySelector("iframe").src)

  // Force autoplay
  src.searchParams.set("autoplay", 1)

  // Peertube : Prevent disgracefull warning Message
  src.searchParams.set("warningTitle", 0)

  // Youtube : Force No Cookie 
  src.origin == 'https://www.youtube.com' ? src = new URL(`https://www.youtube-nocookie.com/${src.pathname}${src.search}`) : null

  // Compose a clean new Iframe element
  _iframe = `<iframe 
  						allow="	accelerometer; 
  										autoplay; 
  										encrypted-media; 
  										gyroscope; 
  										picture-in-picture" 
  						allowfullscreen 
  						src="${src}" 
  						width="560" 
  						height="315"
  						${sandbox} 
  						frameborder="0">
  					</iframe>`

  // Inject the player into the UI
  event.innerHTML = _iframe;

  // Set as activated
  event.classList.add('activated');
}

/**
 * Ehance peertube link without loading heavy iframe
 */
function ehancePeertubeLink(){
  let videos = document.querySelectorAll('[data-video-embed]')
  for (let video of videos) {
    video.setAttribute('role', 'button');
    video.setAttribute('onclick', 'peertubeClickHandler(this);return false;')
    video.setAttribute("href", "javascript:void(0);")
    video.removeAttribute("target")
  }
}

ehancePeertubeLink()


/* Allow SPA */
const observer = new MutationObserver(() => {
		ehancePeertubeLink()
});
observer.observe(document.querySelector("body"), {subtree: true, childList: true});



