let body      = document.querySelector('body'),
    main      = document.querySelector('main'),
    url       = window.location.href,
    y         = 0,
    hueStart,
    hue       = 0, 
    lightest  = 70,
    autoMotion;

const isReduced = window.matchMedia(`(prefers-reduced-motion: reduce)`) === true || window.matchMedia(`(prefers-reduced-motion: reduce)`).matches === true;



//
// Function
//

function reset(){
  main.classList.remove('open');
}

// Function to sort Data
function SortItem(el) {

  if(document.querySelector("button.focus")){
    (document.querySelector("button.focus")).classList.remove("focus")
  }
  el.classList.add("focus")
  
  let attribute = el.getAttribute('data-value'),
    items = document.querySelectorAll("main.open .list > li"),
    itemArray = Array.from(items),
    list = document.querySelector("main.open .list");

  list.setAttribute("data-sort-by", attribute)

  function sorter(a,b) {
    // dirty
    let va, vb;
    if(attribute == 'date'){
      va = a.dataset.date;
      vb = b.dataset.date;
    }else if(attribute == 'title'){
      va = a.dataset.title;
      vb = b.dataset.title;
    }else if(attribute == 'author'){
      va = a.dataset.author;
      vb = b.dataset.author;
    }

    if(attribute == 'type'){
      if(va >vb) return -1;
      if(va < vb) return 1;
      return 0;
    } else {
      if(va < vb) return -1;
      if(va > vb) return 1;
      return 0;
    }
  }

  let sorted = itemArray.sort(sorter);

  sorted.forEach(e =>
    document.querySelector("main > section > .list").appendChild(e));
}

function resetHue(){
  //console.log('resetHue')
  hueStart = Math.round(Math.random() * (360 - 0));
  body.style.setProperty('--hue', hueStart + hue);
  ajustLuminosity()
}

function updateLightest(){
  body.style.setProperty('--lightest', lightest + "%");
}

function hsl360(){
  // Check if Hue value is in acceptable range
  if(hueStart + hue > 360){
    hue = -hueStart;}
  else if (hueStart + hue < 0){
    hue = hueStart;
  }
}

// Slightly update luminosity for blue and violet colors
function ajustLuminosity(){
  let lmin  = 70,
      lmax  = 85,
      h     = 20,
      hmin  = 210 - hueStart,
      hmax  = 310 - hueStart;
  // if hue is in danger zone (blue / violet color can't provide sufisant contrast)
  if(hue >= hmin && hue <= hmax ){
    if(hue <= hmin + h ){
      // if in the bottom on his zone
      lightest = Math.round( lmin + ( (hue - hmin) / 2) )
    } else if( hue >= hmax - h){
      // if in the top on his zone
      lightest = Math.round( lmin + ( (hmax - hue) / 2) )
    } else{
      lightest = lmax
    }
    updateLightest()
  } else if( lightest != lmin){
    // if not in danger zone but luminosity isn't coherent, reset to minimal value
    lightest = lmin;
    updateLightest()
  }
}

function killAutoMotion(){
    if(autoMotion){
      clearInterval(autoMotion);
    }
}

function killScrollMotion(){
  window.removeEventListener("scroll", scrollColor)
}

function killMotion(){
  killAutoMotion()
  killScrollMotion()
}

function killVideo(){
  var videos = document.querySelectorAll('.ltv-activated');
  videos.forEach( video => {
    video.classList.remove('ltv-activated')
    let iframe = video.querySelector('iframe')
    iframe.remove();
  });
}

function jumpHue(value){
  
  function repeat(){
    value -= step
    hue += step
    updateHue()

    if( value >= step) {
      setTimeout( function() {
        repeat()
      }, 20 );
    }
  }

  let step = 3;
  repeat()
}

function updateHue(){
  hsl360()
  ajustLuminosity();
  body.style.setProperty('--hue', hueStart + hue);
}

function scrollColor(){

  // Prevent Scroll on transition
  if(typeof initiated !== 'undefined' && !initiated){

    const brake = 100;

    let _y = 1 + (window.scrollY || window.pageYOffset);
    _y = Math.round(_y / brake);

    if (y != _y){
      if (y < _y) {
        hue++
      } else if (y > _y) {
        hue--
      }
      y = _y;
      updateHue()
      hsl360();
      ajustLuminosity();
    }
  }
}

 
function colorMotion(){ 
  // Brake : higher is slower
  if (!isReduced) {
    // clear any AutoMotion
    killAutoMotion()
    window.addEventListener('scroll', scrollColor)
  } 
}

function colorAutoMotion(t=150){
  if (!isReduced) {
    // clear any Scroll Motion
    killScrollMotion()
    autoMotion = setInterval(function() {
      hue++
      hsl360()
      ajustLuminosity()
      updateHue()
    }, t);    
  }
}

function obf(){
  let as = document.querySelectorAll(".obf")
  as.forEach( a => {
    let value = a.getAttribute('data-obf'),
        protocol = a.getAttribute('aria-label')

    if(protocol == "mail"){protocol = "mailto:"}
    else if(protocol == "telephone"){protocol = "phone:"}
      
    value = value.split("").reverse().join("");
    a.setAttribute("href", protocol + value)


    if(!a.classList.contains('humanize')){
      a.innerHTML = value // else let inner as it is
    }

    a.removeAttribute('data-obf')
    a.classList.remove('obf')

  })
}


let clearPin = () => new Promise( (resolutionFunc,rejectionFunc) => {
  //console.log('clearPin on')
  let pinned = document.querySelector('body > .pinned')
  if(pinned){ 
    pinned.remove()
  }
  return 
});


function clonePin(){
  const parent = document,
        sibling = document.querySelector('main')
  if(parent.querySelector('main:not(.previous) article .pinned')){
    //console.log('We found a a pinnable element. Clone it')
    let toClone = parent.querySelector('article .pinned')
    let clone = toClone.cloneNode(true)
    sibling.after(clone)
    toClone.classList.add('copied')
    draggable(parent.querySelector('body > .pinned'))
  }
}

function redirectEnglishUser(){
  if(!localStorage.getItem('locale')){
    localStorage.setItem('locale', navigator.language);
    if(!navigator.language.includes('fr')){
      console.log('English version might fit you best')
      window.location.replace("/en");
    }
  }
}

/**
 * Makes an element draggable.
 *
 * @param {HTMLElement} element - The element.
 */
function draggable(element) {
    var isMouseDown = false;

    // initial mouse X and Y for `mousedown`
    var mouseX;
    var mouseY;

    // element X and Y before and after move
    var elementX = 0;
    var elementY = 0;

    // mouse button down over the element
    element.addEventListener('mousedown', onMouseDown);

    /**
     * Listens to `mousedown` event.
     *
     * @param {Object} event - The event.
     */
    function onMouseDown(event) {
        mouseX = event.clientX;
        mouseY = event.clientY;
        isMouseDown = true;
    }

    // mouse button released
    document.addEventListener('mouseup', onMouseUp);

    /**
     * Listens to `mouseup` event.
     *
     * @param {Object} event - The event.
     */
    function onMouseUp(event) {
        isMouseDown = false;
        elementX = parseInt(element.style.left) || 0;
        elementY = parseInt(element.style.top) || 0;
    }

    // need to attach to the entire document
    // in order to take full width and height
    // this ensures the element keeps up with the mouse
    document.addEventListener('mousemove', onMouseMove);

    /**
     * Listens to `mousemove` event.
     *
     * @param {Object} event - The event.
     */
    function onMouseMove(event) {
        if (!isMouseDown) return;
        var deltaX = event.clientX - mouseX;
        var deltaY = event.clientY - mouseY;

        element.style.transition = 'all 0s linear'
        element.style.setProperty('--x', deltaX + 'px');
        //element.style.setProperty('--y', deltaY + 'px');
    }
}


function printOnDemand(){

  if(url.includes('?print') ) {
    body.classList = 'print';
    main.classList = 'print';
    if(url.includes('&screen') ) {
          body.classList.add('forscreen');
          main.classList.add('forscreen');
    }
    if(main.getAttribute('data-printed-color')){
      body.style.setProperty('--hue', main.getAttribute('data-printed-color'));
      body.style.setProperty('--lightest', 60 + "%");
    }


    // Dirty but necessary hack. listed section block pagedJs rendering. Why ?
    let undesiredSection = ['#teasers', '#videos', '#interview', '#guests', '[data-section="Ressources"]']
    undesiredSection.forEach( id => {
      if(document.querySelector(id)){
        document.querySelector(id).remove()
      }
    })



    let scripts = ['/assets/lib/paged.js']

    // load cover fallback if needed
    if(document.querySelector('#coverFallback')){
      let lazy = document.querySelectorAll('.lazy');
      lazy.forEach( el => {
        let src   = el.getAttribute('data-src')
        el.setAttribute('src', src)
      })
    }

    //setTimeout(() => {
      // load pagedJs script
      scripts.forEach( function(path, i){
        let script = document.createElement('script');
          script.type = 'text/javascript';
          script.src = window.location.origin + path;


        document.getElementsByTagName('body')[0].appendChild(script);
      });
    //}, 0)

    killMotion()
  }
}

function panorama(enable){
  if(enable){
    document.querySelector('#panorama').setAttribute("aria-hidden", false);
    document.querySelector('#panFallback ').removeAttribute('hidden')
    let a = document.querySelectorAll('#panorama a') 
    a.forEach(el => el.removeAttribute("tabindex") )    
  } else {
    document.querySelector('#panorama').setAttribute("aria-hidden", true);
    let a = document.querySelectorAll('#panorama a') 
    a.forEach(el => el.setAttribute("tabindex", -1) )
    setTimeout( function() {
      if(document.querySelector('#panFallback')){
        document.querySelector('#panFallback').hidden = true
      }
    }, 1000);  
  }
}

function isDarkMode(){
  if (window.matchMedia && window.matchMedia('(prefers-color-scheme: dark)').matches) {  
    body.classList.add("darkMode");
  }

  window.matchMedia('(prefers-color-scheme: dark)').addEventListener('change', event => {
      if(event.matches){
        body.classList.add("darkMode")
      } else {
        body.classList.remove("darkMode")
      }
  });
}

//
// Event Listener
//

function indexPageEvent(){
  colorAutoMotion()
  redirectEnglishUser()
}

function allPagesButIndexEvent(){
  let actionClose = document.querySelector(".action-close"),
    actionColorMode = document.querySelector("main:not(.previous) .action-colorMode"),
    sections = document.querySelectorAll("section");

  colorMotion()


  actionClose.addEventListener("click", () => {
   reset()
  }, false);

  if(actionColorMode){actionColorMode.classList.add('bite')}
  actionColorMode.addEventListener("click", () => {
   body.classList.toggle("darkMode");
  }, false);

  sections.forEach(function(section){
    let toggle = section.querySelector('section > h1:first-child')
    if(toggle){
      toggle.addEventListener("click", () => {
        section.classList.toggle("closed");
      }, false);
    }
  });

  // hide panorama for screen reader

  obf()
  // reset Share Button
  ShareButtons.update()
  printOnDemand()

}

function eventPageEvent(){
  let guestButtons = document.querySelectorAll("#guests button");

  guestButtons.forEach(function(guestButton){
    guestButton.addEventListener("click", () => {
      syncGuest(guestButton)
    }, false);
    guestButton.addEventListener("mouseover", () => {
      syncGuest(guestButton)
    }, false);

  });

  function syncGuest(guestButton){
    let n      = guestButton.getAttribute("data-item"),
      buttonShowed = document.querySelector("#guests ul.artefact li.show"),
      buttonShow  = guestButton.parentNode,
      bioShowed  = document.querySelector("#guests ul:not([aria-hidden='true']) li.show"),
      bioShow   = document.querySelector("#guests ul:not([aria-hidden='true']) li[data-item='"+ n +"']")

    if(buttonShowed){buttonShowed.classList.remove("show");}
    if(bioShowed){bioShowed.classList.remove("show");}
    buttonShow.classList.add("show");
    bioShow.classList.add("show");
  }
}


//
// On init
//

resetHue()
isDarkMode()

if(body.getAttribute('data-serve') != 'index'){
  allPagesButIndexEvent()
  panorama(false)
  if(body.getAttribute('data-serve') == 'event'){
    eventPageEvent();
    clonePin()
  }
} else{
  indexPageEvent()
  panorama(true)
}

// That's all for me. Thank you !
console.log("%cDesign + Code: \nhttps://bnjm.eu", "font-family: monospace; font-size: 1.5em;")

