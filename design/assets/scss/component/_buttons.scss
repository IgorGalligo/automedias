#sideAction{

	position: absolute;
	@include flex();
	@include flex-direction(row);
	gap: 0; // 1px;
	top: $base*-22;
	left: calc($gut * -1);

	[class^="action"]{
		width: $baseline*2;
		height: $baseline*2;
		cursor: pointer;
		line-height: 50%;
		color: currentColor;
		background: var(--bg);
		padding: math.div($unite, 4);
		border-color: currentColor;
		border-style: solid;
		border-width: 0 1px 1px 0;
		box-sizing: border-box;
		flex-shrink: 0;
		transition: height .25s ease-in-out;

		&:is(div){
			border-right-width:0;
		}
			

		.stroke{stroke: var(--cc);}
		.fill{fill: var(--cc);}

		&:hover a:not(:hover:not(:active)) {
			background: var(--cc);
			color: var(--bg);
			.stroke{stroke: var(--bg);}
			.fill{fill: var(--bg);}
		}	
		a{display: inline-block;}

	}

	.action-close{font-size: $font-size*4;}
	.action-rss{
		border-right-width:0;
		svg{width: $base*10; padding: $base*1.5;}
	}


	.action-share{
		overflow: hidden;
		@include flex();
		@include flex-direction(row);
		@include justify-content(flex-start);
		padding: 0;

		> *{
			transition: .25s ease;
			left: 0;
			top: 0
		}

		&:hover{
			width: $baseline*6;
			height: $baseline*2;
			> *{ left: $baseline*-2; top: 0;}
		}

		svg, a{
			width: $baseline*2;
			height: $baseline*2;
			flex-shrink: 0;
		}

		svg{
			padding: math.div($unite, 4);
			box-sizing: border-box;
		}

		.share-btn{
			@include flex();
			@include flex-direction(row);
			@include justify-content(space-around);
			//margin: math.div($unite,2) 0 0 math.div($unite,2);
			a{
				display: inline-block;
			}
		}
	}

	@include for-size(desktop){
		top: $baseline*-3.5;
	}

	@include before-size(desktop-1200){
		.action-share .share-btn{
			[data-id="fb"]{display: none;}
			[data-id="copy"]{display: none;}
		}
	}
	@include for-size(desktop-1200){
		position: sticky;
		top: 20vh;
		left: 100%;
		width: calc($gut*2);
		height: 0;
		@include flex-direction(column);
		[class^="action"]{
			border-width: 0 0 1px 1px;
			top: $baseline*-2.5;
			right: calc($gut*-3);

			&:is(div){
				border-bottom-width:0;
			}
		}
			
		.action-rss{
			border-bottom-width:0;
		}

		.action-share{
			@include flex-direction(column	);
			&:hover{
				width: $baseline*2;
				height: $baseline*6;
				> *{ top: $baseline*-2; left: 0;}
			}
			.share-btn{
				@include flex-direction(column	);
				[data-id="tg"]{display: none;}
				[data-id="wa"]{display: none;}
			}

		}
	}
}

main[data-type="publications_list"],
main[data-type="events_list"]{
	#sideAction .action-share{
		display: none;
	}
} 

#backTop{ display: none;}
@media screen and (max-width: 450px) {
	main[data-type="event"],
	main[data-type="article"],
	main[data-type="audio"],
	main[data-type="member"],
	main[data-type="video"]{
		~ #backTop{
			display: inline-block;
			position: fixed;
			bottom: 0;
			right: 0;
			padding: $base*2 $base*4;
			background: var(--bg);
			@include f1(1);
			cursor: pointer;
			z-index: 6;
			border-top: 1px solid currentColor;
			border-left: 1px solid currentColor;
		}
	}
}