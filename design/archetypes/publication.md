---
title: --REPLACE_TITLE--
subtitle: REPLACE SUBTITLE

flag: flag title
layout: publication
type: --REPLACE_TYPE--
locale: fr

draft: false

author: 
  - REPLACE AUTHOR
author_contact:
  - type: mail
    link: REPLACE@MAIL

description: REPLACE Description
date: 2000-01-01

videos:
  title: --REPLACE TITLE--
  plateform: youtube
  id: 6xckBwPdo1c

audio:
  plateform: soundcloud
  id: 1314683398

# Bibliographie de l'article
# La bibliographie est une liste. Chaque entrée est introduit par un retrait (deux espaces) et un tiret.
# Il est possible de segmenter la bibliographie par catégorie. Il convient alors de renseigner un titre (- title: MON TITRE). L'ensemble des références placer à la ligne et respectant un retrait supplémentaire (deux espaces) seront inclut dans ce sous-groupe
# Si les références contiennent des doubles points (ou colons), il convient remplacer ce symbole par l'expression &#58; ou de placer la référence entre "guillemet"
bibliography: 
  - title: Catégorie bibliographique
    references:
      -  "Title"

 

# Cette section permet de publier un contenu complémentaire à l'article, sous la forme de sections disctincte en fin de page. 
# le contennu de ces section peut être rédigé en markdown
aside:
  - title: Title
    content: "Content inside"
---

# Introduction

Deserunt ad commodo id veniam ullamco eiusmod nostrud laborum deserunt excepteur esse esse nisi[^1].

[^1]: Note de bas de page

