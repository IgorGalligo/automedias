---
title: --REPLACE_TITLE--
subtitle: REPLACE SUBTITLE

date: 2000-01-01
event_date: 
  - 2000-01-01
place: 

author: 
  - Igor Galligo

organizers: 
  - Automedias.org

layout: event

locale: fr

#
# Programme
#

programme:

  # Jour 1
  - date: 2000-01-01
    title: Journée 1 __TITRE JOURNEE__
    schedule:
      - session: REPLACE SESSION
        moderator: NOM PRENOM
        isit: 
          - NOM PRENOM
        guests: 
          - NOM PRENOM          
        speakers:
          - NOM PRENOM          
        start_time: 9:45
        end_time: 11:25
        debate: 
          start_time: 11:00
          duration: 25

      - break: true
        start_time: 13:00
        end_time: 14:30

---

# Présentation

