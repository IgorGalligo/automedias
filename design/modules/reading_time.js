// Time to read
function readingTime(text, lang="fr"){

  let regFn = new RegExp(/<section class=\"footnotes\".*?<\/section>/);
  text = ( text.replace(/\n/g, '') ).replace(regFn, '')

  // get entire post content element
  let wordCount = `${text}`.match(/\b[-?(\w+)?]+\b/gi).length;
  //calculate time in munites based on average reading time
  let timeInMinutes = (wordCount / 300)
  //validation as we don't want it to show 0 if time is under 30 seconds
  let output;
  if(timeInMinutes <= 0.5) {
    output = 1;
  } else {
    //round to nearest minute
    output = Math.round(timeInMinutes);
  }
  if(output > 1){
    if(lang =="en"){
      return `${output}` + ' minutes reading';
    } else{
      return `${output}` + ' minutes de lectures';
    }
  } else{
    return "";
  }
}

module.exports = readingTime
