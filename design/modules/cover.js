// cover
const { promises } = require('fs'),
      fs = require('fs'),
      { join, dirname } = require('path'),
      { registerFont, createCanvas } = require('canvas');     

  registerFont('./design/assets/font/f1/Epilogue-Light.ttf', { family: 'e200' });
  registerFont('./design/assets/font/f1/Epilogue-SemiBold.ttf', { family: 'e600' });

//
// Cover
//
const wrapText = function(ctx, text, x, y, maxWidth, lineHeight) {
    // First, split the words by spaces
    let words = text.split(' ');
    // Then we'll make a few variables to store info about our line
    let line = '';
    let testLine = '';
    // wordArray is what we'l' return, which will hold info on 
    // the line text, along with its x and y starting position
    let wordArray = [];
    // totalLineHeight will hold info on the line height
    let totalLineHeight = 0;

    // Next we iterate over each word
    for(var n = 0; n < words.length; n++) {
        // And test out its length
        testLine += `${words[n]} `;
        var metrics = ctx.measureText(testLine);
        var testWidth = metrics.width;
        // If it's too long, then we start a new line
        if (testWidth > maxWidth && n > 0) {
            wordArray.push([line, x, y]);
            y += lineHeight;
            totalLineHeight += lineHeight;
            line = `${words[n]} `;
            testLine = `${words[n]} `;
        }
        else {
            // Otherwise we only have one line!
            line += `${words[n]} `;
        }
        // Whenever all the words are done, we push whatever is left
        if(n === words.length - 1) {
            wordArray.push([line, x, y]);
        }
    }

    // And return the words in array, along with the total line height
    // which will be (totalLines - 1) * lineHeight
    return [ wordArray, totalLineHeight ];
}

function clean(text){
  if(text){
    return text.replace('&nbsp;',' ').replace('&#58;',': ').replace('&thinsp;',' ').replace('&thinsp;',' ').replace('&#8209;','-')
  } else {
    return null
  }
}

module.exports = async function createCover(publications) {


  publications.forEach( async publication => {
    let postTitle = clean(publication.data.title),
        subTitle = clean(publication.data.subtitle),
        filename = publication.fileSlug,
        hue = Math.random()*360,
        w = 1280,
        h = 628,
        baseline = h/20


    const canvas = createCanvas(w, h)
    const ctx = canvas.getContext('2d')

    let darkColor = 'hsl('+ hue +',65%,15%)',
        lightColor = 'hsl('+ hue +',100%,70%)'
    
    ctx.fillStyle = darkColor;
    ctx.fillRect(0, 0, w, h);

    ctx.fillStyle = lightColor;
    ctx.fillRect(w*.1, baseline*1, w*.8, h - baseline);

    ctx.fillStyle = darkColor

    // sitename
    ctx.font = baseline + 'px e200';
    ctx.textAlign = "center";
    ctx.fillText('automedias', w/2, baseline*6);

    // title
    ctx.font = baseline*2 + 'px e600';
    ctx.textAlign = "center";
    let wrappedText = wrapText(ctx, postTitle, w*.5, baseline*8, w*.75,  baseline*2.25);

    wrappedText[0].forEach(function(item, i) {
        ctx.fillText(item[0], item[1], item[2]);
    })

    // subtitle
    if(subTitle){
      ctx.font = baseline + 'px e200';
      ctx.textAlign = "center";
      let wrappedSubtitle = wrapText(ctx, subTitle, w*.5, baseline*8 + wrappedText[1] + baseline*2, w*.6,  baseline);

      wrappedSubtitle[0].forEach(function(item, i) {
          ctx.fillText(item[0], item[1], item[2]);
      })

    }





    // test if the output directory already exists, if not, create
    const outputDir = dirname('./public/assets/social');
    if (!fs.existsSync(outputDir))
        fs.mkdirSync(outputDir, { recursive: true });

    // write the output image
    const stream = fs.createWriteStream('./public/assets/social/'+ filename.substring(0,5) + '.jpg');
    //stream.on('finish', resolve);
    //stream.on('error', reject);
    canvas
        .createJPEGStream({
            quailty: 0.8,
        })
        .pipe(stream);


  });

}