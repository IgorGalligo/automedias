// Nunjuck date
let monthsEn = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
let monthsFr = ["janvier", "février", "mars", "avril", "mai", "juin", "juillet", "août", "septembre", "octobre", "novembre", "décembre"];

const nth = function(d) {
    if (d > 3 && d < 21) return 'th';
    switch (d % 10) {
        case 1:  return "st";
        case 2:  return "nd";
        case 3:  return "rd";
        default: return "th";
    }
}

function getDate(date, lang){
  let d

  let dt = new Date (date)
      dt = dt.toISOString()
  
  if( lang == 'fr'){
    d = '<time lang="fr" datetime="' + dt + '">'
          + '<span class="sDay">' + (date.getDate()) + '</span> ' 
          + '<span class="sMonth">' + monthsFr[date.getMonth()]  +' </span> '
          + '<span class="sYear">' + date.getFullYear()  +'</span></time>'

  } else {
    d = '<time lang="en" datetime="' + dt + '">'
          + '<span class="sMonth">' + monthsEn[date.getMonth()]  +' </span>'
          + '<span class="sDay">' + (date.getDate())  
          + '<sup>' + nth(date.getDate() + 1) + '</sup>' + '</span>'
          + '<span class="sComma">, </span>'
          + '<span class="sYear">' + date.getFullYear()  +'</span></time>';

  }

  return d
}


function dateCompare(date, lang){

  date = Array.from(date)
  
  let options   = {
    year: "numeric",
    month: "long",
    weekday: "long",
    hour: "numeric",
    minute: "numeric",
  },
  formatDate    = []


  if(date.length == 1){
    if(lang == "en"){
    formatDate.push([
          '<span class="sMonth">' + monthsEn[date[0].getMonth()] + '</span> ' 
          + '<span class="sDay">' + date[0].getDate() + '</span> ' 
          + '<sup>' + nth(date[0].getDate())  + '</sup>'
          + ', ' 
          + '<span class="sYear">' + date[0].getFullYear() + '</span> ' 
          , date])
    } else {
      formatDate.push([
            '<span class="sDay">' + date[0].getDate() + '</span> '
            + '<span class="sMonth">' + monthsFr[date[0].getMonth()] + '</span> ' 
            + '<span class="sYear">' + date[0].getFullYear() + '</span>'
          , date])
    }

  } else{

      let date1 = date[0]
      let date2 = date[date.length - 1]

      if( date1.getFullYear() == date2.getFullYear()){
        // if same year
        if( date1.getMonth() == date2.getMonth()){

          // 1 → 1/2/2022
          if(lang == "en"){
            formatDate.push([
                        '<span class="sMonth">' + monthsEn[date1.getMonth()] + '</span> '
                          + '<span class="sDay">'   + date1.getDate()   + '</span>'
                          + '<sup>' + nth(date1.getDate()) + '</sup>'
                        ,  date1])
            
            formatDate.push([ 
                          '<span class="sDay">'   + date2.getDate()   + '</span>'
                          + '<sup>' + nth(date2.getDate()) + '</sup>'
                          + ', ' 
                          + '<span class="sYear">'  +  date2.getFullYear() + '</span>'
                        ,  date2])
          } else {

          formatDate.push([
                      '<span class="sDay">'   + date1.getDate()   + '</span>'
                      , date1])

          formatDate.push([
                        '<span class="sDay">'   + date2.getDate()   + '</span> '
                        + '<span class="sMonth">' + monthsFr[date2.getMonth()] + '</span> ' 
                        + '<span class="sYear">'  +  date2.getFullYear() + '</span>'
                      , date2])
          }
        } else {
            // 1/1 → 1/2/2022
            if(lang == "en"){
            formatDate.push([
                        '<span class="sMonth">' + monthsEn[date1.getMonth()] + '</span> '
                          + '<span class="sDay">'   + date1.getDate()   + '</span>'
                          + '<sup>' + nth(date1.getDate()) + '</sup>'
                        , date1])

            formatDate.push([
                          '<span class="sMonth">' + monthsEn[date2.getMonth()] + '</span> '
                          + '<span class="sDay">'   + date2.getDate()   + '</span>'
                          + '<sup>' + nth(date2.getDate()) + '</sup>'
                          + ', ' 
                          + '<span class="sYear">'  +  date2.getFullYear() + '</span>'
                        , date2])
          } else{

            formatDate.push([
                        '<span class="sDay">'   + date1.getDate()   + '</span> '
                          + '<span class="sMonth">' + monthsFr[date1.getMonth()] + '</span> '
                        , date1])

            formatDate.push([
                          '<span class="sDay">'   + date2.getDate()   + '</span> '
                          + '<span class="sMonth">' + monthsFr[date2.getMonth()] + '</span> ' 
                          + '<span class="sYear">'  +  date2.getFullYear() + '</span>'
                        , date2])
          }
        }
      } else {        
          // 1/1/2021 → 1/1/2022
          if(lang == "en"){
            formatDate.push([
                        '<span class="sMonth">' + monthsEn[date1.getMonth()] + '</span> ' 
                          + '<span class="sDay">'   + date1.getDate() + '</span>, '
                          + '<sup>' + nth(date1.getDate()) + '</sup>'
                          + '<span class="sYear">'  + date1.getFullYear() + '</span>' 
                        , date1])
            formatDate.push([
                          '<span class="sMonth">' + monthsEn[date2.getMonth()] + '</span> ' 
                          + '<span class="sDay">'   + date2.getDate() + '</span>, '
                          + '<sup>' + nth(date1.getDate()) + '</sup>'
                          + '<span class="sYear">'  + date2.getFullYear() + '</span>' 
                        , date2])
          } else{

            formatDate.push([
                        '<span class="sDay">'   +   date1.getDate()           + '</span> ' 
                          + '<span class="sMonth">' + monthsFr[date1.getMonth()]  + '</span> ' 
                          + '<span class="sYear">'  +  date1.getFullYear() + '</span>'
                        , date1])
            formatDate.push([
                          '<span class="sDay">'   +   date2.getDate()           + '</span> ' 
                          + '<span class="sMonth">' + monthsFr[date2.getMonth()]  + '</span> ' 
                          + '<span class="sYear">'  +  date2.getFullYear()        + '</span>'                     
                        , date2])
          }
      }
  }

  let result = []

  formatDate.forEach( (fdate, j ) => {
    let dt = new Date (fdate[1])
    dt = dt.toISOString()

    let timeElem  = `<time lang="${lang}" datetime="${dt}">${fdate[0]}</time>`
    result.push(timeElem)
  })
  return result.join(' → ')

}

function time(time){
  if(typeof(time) == 'string'){
    return time
  } 
  var hours = (time / 60);
  var rhours = Math.floor(hours);
  var minutes = (hours - rhours) * 60;
  var rminutes = Math.round(minutes);
  rminutes = rminutes.toString().padStart(2, '0');

  return `<time datetime="${rhours}:${rminutes}:00 GMT+0200 (CEST)">${rhours}:${rminutes}</time>`
}

// is date is past
function dateIsPast(date) {
  const today = new Date()
  if(Array.isArray(date)){ date = date[date.length - 1] }
  return date < today;
}


module.exports = {getDate, dateCompare, time, dateIsPast}