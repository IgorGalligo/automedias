const { minify } = require("html-minifier-terser")

function minHTML(content, outputPath) {
  if (outputPath.endsWith(".html")) {
    let minified = minify(content, {
      useShortDoctype: true,
      removeComments: true,
      collapseWhitespace: true
    });
    return minified;
  }
  return content;
}

module.exports = minHTML;
