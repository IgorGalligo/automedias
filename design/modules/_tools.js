// shuffle array
function shuffle(values) {
  return values.sort((a, b) => 0.5 - Math.random());
}

// clear double value
function uniq(a) {
    a = a.flat()
    return a.sort().filter(function(item, pos, ary) {
        return !pos || item != ary[pos - 1];
    });
}

module.exports = {shuffle, uniq}