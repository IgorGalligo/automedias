// https://gitlab.com/JulieBlanc/typesetting-tools/-/blob/master/regex-typo.js
let rulesFr = [
  {
      // oe
      reg: /oe/g, 
      repl: 'œ' 
  },
  {
      // XIème = XIe 
      reg: /(X|I|V)ème/g, 
      repl: '$1e' 
  },

 /* {
      // french open quotes 
      reg: /\"([A-Za-zÀ-ÖØ-öø-ÿœŒ])/g, 
      repl: '«' 
  },
  {
      // french close quotes 
      reg: /([A-Za-zÀ-ÖØ-öø-ÿœŒ])\"/g, 
      repl: '»' 
  },*/
  {
      // real apostrophe
      reg: /\'/g, 
      repl: '’' 
  },
  {
      // real suspension points
      reg: /\.+\.+\./g, 
      repl: '\u2026'
  },
  {
      // delete all spaces before punctuation !?;:»›”)].,
      reg: /(\s)+([!?;:»›”)\]\.\,])/g, 
      repl: '$2'
  },
  {
      // add narrow no break space before !?;»›
      reg: /([!?;»›])/g, 
      repl: '&#8239;$1'
  },
  {
      // add narrow no break space before :
      reg: /(?<!http)(?<!https)(:)/g, 
      repl: '&#8239;$1'
  },
  {
      // delete all spaces after «‹“[(
      reg: /([«‹“\[(])(\s)+/g, 
      repl: '$1'
  },
  {
      // add narrow no break space after «‹
      reg: /([«‹])(?!&#8239;)/g, 
      repl: '$1&#8239;'
  },
  {
      // no break space after some two letter words 
      reg: /\s(le|la|un|une|ce|ces|il|on|les|de|des|du|ils)\s+/g, 
      repl: ' $1&nbsp;'
  },
  {
     // no break space after successive two letter words 
     reg: /\s+([a-zØ-öø-ÿœ]{2})\s+([A-Za-zÀ-ÖØ-öø-ÿœŒ]{2})\s+/g, 
     repl: ' $1 $2&nbsp;'
 },
  {
      // no break space after one letter words
      reg: /\s+([a-zà])\s+/gi, 
      repl: ' $1&nbsp;'
  },
  {
      // no break space after first word (2-5 letter) of the sentence
      reg: /\.\s([A-ZÀ-Ö])([A-Za-zÀ-ÖØ-öø-ÿœŒ]{1,5})\s+/g, 
      repl: '. $1$2&nbsp;'
  },
  {
      // no break space into names
      reg: /([A-ZÀ-ÖØŒ])([A-Za-zÀ-ÖØ-öø-ÿœŒ]+)\s+([A-ZÀ-ÖØŒ])([A-Za-zÀ-ÖØ-öø-ÿœŒ]+)/g, 
      repl: '$1$2&nbsp;$3$4'
  },
  {
      // no break space before Caps + .
      reg: /\s([A-ZÀ-ÖØŒ])\./g, 
      repl: '&nbsp;$1. '
  },
  {
      // no break space before 'siècles'
      reg: /(X|I|V)(er|e)\s+siècle/g, 
      repl: '$1$2&nbsp;siècles'
  },
  {
      // no break space after figures table page chapitre ect. + number
      reg: /(figures?|tables?|planches?|chapitres?|pages?|parties?|sections?|volumes?|vol\.)\s+(\d|I|X|V)/g, 
      repl: '$1&nbsp;$2'
  },

  {
      // numéros
      reg: /\sno\.?\s?(\d+)/g, 
      repl: ' n<sup>o</sup>&nbsp;$1' 
  },
  {
      // siècles 
      reg: /(X|I|V)(er|e)/g, 
      repl: '$1<sup>$2</sup>' 
  },
]

module.exports = function(content, locale){

    if(locale =='fr'){
      rulesFr.forEach( function(rule){
        content = content.replaceAll(rule['reg'], rule['repl'])
      })
    }

    return content;

}; 

