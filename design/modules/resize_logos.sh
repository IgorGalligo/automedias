#!/bin/bash
FOLDER="contenu/_media/partners_logos/*"
for image_file in `find $FOLDER -type f -name "*.jpg" -o -name "*.jpeg" -o -name "*.png"  -o -name "*.gif"`;  
do
	convert $image_file  -resize 200x200\> -quality 60 -define webp:lossless=false "${image_file%.*}.webp"
done