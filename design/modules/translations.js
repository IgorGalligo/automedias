// https://github.com/adamduncan/eleventy-plugin-i18n

module.exports = {
  common: {
    by:{
      'en': "by " ,
      'fr': "par "
    },
    on:{
      'en': "on " ,
      'fr': "sur "
    }
  },
  category: {
    events: {
      'en': 'Events',
      'fr': 'Événements'
    },
    publications: {
      'en': 'Publications',
      'fr': 'Publications'
    },
    event: {
      'en': 'Event',
      'fr': 'Événement'
    },
    publication: {
      'en': 'Publication',
      'fr': 'Publication'
    },
    members: {
      'en': 'Members',
      'fr': 'Membres'
    },
    info: {
      'en': 'Info',
      'fr': 'Info'
    },
  },
  list: {
    last_event:{
      'en': "Last Event" ,
      'fr': "Dernier événement"
    },
    event:{
      'en': "Event" ,
      'fr': "Événement"
    },
    next_event:{
      'en': "Next event" ,
      'fr': "Prochain évènement"
    },
    all_past_event:{
      'en': "All past events" ,
      'fr': "Tous les événements passés"
    },
    call:{
      'en': "Call to papers" ,
      'fr': "Appel à contribution"
    },
    all_publications:{
      'en': "All publications" ,
      'fr': "Toutes les publications"
    },
    linked: {
      en: "Linked publication",
      fr: "Publications en lien"
    }
  },

  page:{
    partners:{
      en: "Partners",
      fr: "Partenaires"
    },
    pratical:{
      en: "Practical info",
      fr: "Infos pratiques"
    },
    guest:{
      en: "Guest",
      fr: "Invité"
    },
    guests:{
      en: "Guests",
      fr: "Invités"
    },
    interpretation:{
      en: "Simultaneous interpretation by",
      fr: "Interprétation simultanée par"
    },
    translation:{
      en: "Simultaneous translation by",
      fr: "Traduction simultanée par"
    },
    lunch:{
      en: "Lunch",
      fr: "Déjeuner"
    },
    break:{
      en: "Break",
      fr: "Pause"
    },
    program:{
      en: "Program",
      fr: "Programme"
    },
    video:{
      en: "Video",
      fr: "Vidéo"
    },
    moderator:{
      en: "Session moderated by",
      fr: "Session animée par"
    },
    day:{
      en: "Day",
      fr: "Journée"
    },
    linked_visuals:{
      en: "Linked visuals elements",
      fr: "Éléments visuels associés"
    },
    bibliography:{
      en: "Bibliography",
      fr: "Bibliographie"
    },
    talk_duration:{
      en: "minutes open talk.",
      fr: "minutes de discussion intervenants&#8209;public."
    },
    an_event:{
      en: "An&#32;{{ name }}&#32;'s event",
      fr: "Un évènement organisé par&#32;{{ name }}"
    },
    researcher:{
      en: "Researchers",
      fr: "Chercheurs.ses"
    },
    engineer_designer:{
      en: "Digital Engineers and Designers",
      fr: "Ingénieur.e.s et Designer.e.s numériques"
    },
    activist:{
      en: "(Media-)Activists and Citizens",
      fr: "(Média-)Activistes et Citoyens"
    },
    scientific:{
      en: "Scientific Committee",
      fr: "Comité scientifique"
    },
    admin:{
      en: "Administrative Committee",
      fr: "Comité administratif"
    },
    founder:{
      en: "Project Founder",
      fr: "Fondateur du Projet"
    },
    partners:{
      en: "",
      fr: ""
    },
    partners:{
      en: "",
      fr: ""
    },
    partners:{
      en: "",
      fr: ""
    },
    partners:{
      en: "",
      fr: ""
    },
    partners:{
      en: "",
      fr: ""
    },
  },

  interaction: {
    close_view:{
      en: "Close current page",
      fr: "Fermer cette page"
    },
    share:{
      en: "Share this page",
      fr: "Partager cette page"
    },
    dark_mode:{
      en: "Switch to dark/light Mode",
      fr: "Intervertir la gamme de couleur"
    },
    copy:{
      en: "Copy page link",
      fr: "Copier le lien de la page"
    },
    title:{
      en: "Title",
      fr: "Titre"
    },
    authors:{
      en: "Authors",
      fr: "Auteurs"
    },
    sort_by:{
      en: "Sort by",
      fr: "Trier par"
    },
    language_available:{
      en: "is also available in english",
      fr: "est également disponible en français"
    },
    langs:{
      en: "Français",
      fr: "English"
    },
    fr:{
      en: "french",
      fr: "français"
    },
    en:{
      en: "english",
      fr: "anglais"
    },
    switch_lang:{
      en: "Read this page in",
      fr: "Lire cette page en"
    },
    current_lang:{
      en: "This page is currently in",
      fr: "Cette page est actuellement en"
    },
    link_to:{
      en: "Go to page",
      fr: "Aller vers la page "
    },
    download_this:{
      en: "Download document",
      fr: "Télécharger ce document"
    },
    back_to:{
      en: "Back to ",
      fr: "Retour vers "
    },
    available_language:{
      en: "Choose language",
      fr: "Choisir la langue"
    },
    breadcrumb:{
      en: "breadcrumb",
      fr: "Fil d'ariane"
    },
    watch:{
      en: "Watch video",
      fr: "Regarder la video"
    },
    read_publication:{
      en: "Read the publication",
      fr: "Lire la publication"
    },
    access_event:{
      en: "Access event",
      fr: "Accéder à l'événement"
    },
    rss:{
      en: "Access RSS Feed",
      fr: "S'abonner au flux RSS"
    }
  },

  member: {
    general:{
      en: "Community",
      fr: "Acteurs"
    },
    adm:{
      en: "Administrative Committee",
      fr: "Comité administratif"
    },
    admin:{
      en: "Administrators",
      fr: "Administrateurs.ses"
    },
    partner:{
      en: "Partners",
      fr: "Partenaires"
    },
    researcher:{
      en: "Researchers",
      fr: "Chercheurs.ses"
    },
    engineer_designer:{
      en: "Digital Engineers and Designers",
      fr: "Ingénieur.e.s et Designer.e.s numériques"
    },
    activist:{
      en: "(Media-)Activists and Citizens",
      fr: "(Média-)Activistes et Citoyens"
    },
    scientific:{
      en: "Scientific Committee",
      fr: "Comité scientifique"
    },
    founder:{
      en: "Project Founder",
      fr: "Fondateur du Projet"
    }
  },
  colophon: {
    web_based:{
      en: "Composé, dans un navigateur web, avec PagedJs",
      fr: "Web based composition, with PagedJs"
    }
  }
};