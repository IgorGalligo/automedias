const Image           = require('@11ty/eleventy-img')

async function imageShortcode(src, alt = "", sizes="1280px") {
  
  let metadata = await Image('./contenu/media/'+src, {
    widths: [1280],
    formats: ["webp", "jpg"],
    outputDir: "./public/media/couverture",
    urlPath: "../media/couverture",
    filenameFormat: function (id, src, width, format, options) {
      const extension = path.extname(src);
      const name = path.basename(src, extension);
      return `${name}-${width}.${format}`;
    }    
  });

  let imageAttributes = {
    alt,
    sizes,
    loading: "lazy",
    decoding: "async"
  };
  return Image.generateHTML(metadata, imageAttributes, {
    whitespaceMode: "inline"
  });
}

// async function imageShortcode(src, alt = "", sizes="1280px") {
//   const inputPath   = this.page.inputPath,
//         imageSrc    = `${inputPath.substring(0, inputPath.lastIndexOf('/'))}/${src}`,
//         outputPath  = this.page.outputPath,
//         imageDest   = `${outputPath.substring(0, outputPath.lastIndexOf('/'))}/`;

//   console.log(outputPath, imageDest)
//   let metadata = await Image(imageSrc, {
//     widths: [1280],
//     formats: ["webp", "jpg"],
//     // Write processed images to the correct `outputPath`
//     outputDir: imageDest,
//     // Prepend the correct path to the image `src` value
//     urlPath: this.page.url,

//     /*filenameFormat: function (id, src, width, format, options) {
//       const extension = path.extname(src);
//       const name = path.basename(src, extension);
//       return `${name}-${width}.${format}`;
//     }*/

//   });

//   let imageAttributes = {
//     alt,
//     sizes,
//     loading: "lazy",
//     decoding: "async"
//   };
//   return Image.generateHTML(metadata, imageAttributes, {
//     whitespaceMode: "inline"
//   });
// }

module.exports = imageShortcode
