//requires
const fs   = require('fs'),
      pandoc = require('simple-pandoc'),
      yesno = require('yesno');

// check if type existe

// set type
const args  = process.argv.slice(2);
let type
if( args[0] == 'event'){ 
  type = 'event'
} else if( args[0] == 'article' || args[0] == 'interview' || args[0] == 'video' ){
  type = 'publication'
} else {
  console.log("\x1b[33m", 'Type d\'article non valide');
  return;
}

const langs = ['fr', 'en']

console.log("Working on a ", args[0])


// count existing item
  fs.promises.readdir('contenu/' + type + '/', (err, files) => {
  }).then( files => {
      createNew(files.length - 1) // File length minus json data file
    }
  )

function createNew(item_count){
  // Copy archetype
  let title, content = 'NO CONTENT'

  if(args[1]){
    title = args[1]
  } else {
    title = 'REPLACE_TITLE'
  }
  let three_digits_item_count = ('000' + (item_count + 1)).substr(-3)
  let pageName =  three_digits_item_count + 
                  '-' + args[0].substring(0,1).toUpperCase() + 
                  '-' + title

  /*fs.copyFile('design/archetype/'+ type +'.md', pageName + '.md', (err) => {
    //if (err) throw err;
    console.log( type + '/' +  pageName + '.md is create');
  });*/

  let data = fs.readFileSync('design/archetypes/'+ type +'.md',
              {encoding:'utf8', flag:'r'});
   
  // Display the file data
  let date = (new Date()).toISOString().split('T')[0];

  data =  data.replaceAll('2000-01-01', date)
              .replace('--REPLACE_TITLE--', title.replaceAll('_', ' '))
              .replace('--REPLACE_TYPE--', args[0])


  // TO FIX
  if(args[0] != 'interview'){
    data = data.replace('/audio:\n\s\splateform: soundcloud\n\s\sid: --REPLACE URL--/gm','')
  }
  if(args[0] != 'video'){
    data = data.replace('video:\n\s\stitle: --REPLACE TITLE--\n\s\splateform: youtube\n\s\sid: 6xckBwPdo1c','')
  }
  // End TO FIX

  // create parent directory for article
  if (!fs.existsSync('contenu/' + type + '/' +  pageName + '/')){
    fs.mkdirSync('contenu/' + type + '/' +  pageName + '/', { recursive: true });
  }

  // create Shared Data file
  fs.promises.writeFile('contenu/' + type + '/' +  pageName + '/' + pageName + '.yaml', '# Shared data here', function (err) {
    if (err) throw err
  }).then( () => {
    console.log('\x1b[33m%s\x1b[0m', 'Shared Data file is created successfully.');
  })


  langs.forEach( lang => {

    data =  data.replaceAll(`locale: fr`, `locale: ${lang}`)
    
    fs.promises.writeFile('contenu/' + type + '/' +  pageName + '/' + lang + '.md', data, function (err) {
      if (err) throw err
    }).then( () => {
        console.log('\x1b[32m%s\x1b[0m', `${lang}.md is successfully .`);
    }).then( () => {
        if(args[1]){
          appendContent(pageName)
        }
      }
    );
  });
}

async function deleteContent(docx){
  const ok = await yesno({
      question: 'Effacer ' + args[1] + '.docx du repertoire ? (y/n)'
  });         
  if(ok){
    fs.unlink(docx, function (err) {
        if (err) throw err;
        console.log('Adieu ' + args[1] + '.docx ...');
    });
  }  
}

function appendContent(pageName){
  const docx = args[1] + '.docx'
  try {
    if (fs.existsSync(docx)) {
      console.log("Find a docs document. Let's auto-convert it!");
      //file exists
      const htmlToMarkdown = pandoc('docx', 'markdown+hard_line_breaks');
      
      htmlToMarkdown(fs.readFileSync(docx))
      .then(md => {
        langs.forEach( lang => {
          fs.appendFileSync('contenu/' + type + '/' +  pageName + '/' + lang + '.md', md);
        })
        deleteContent(docx)
      });
    }
  } catch(err) {
    console.error(err)
  }
}