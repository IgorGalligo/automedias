const {getDate, dateCompare, time, dateIsPast} 
                      = require('./design/modules/date.js'),
      { EleventyI18nPlugin } 
                      = require("@11ty/eleventy"),
      i18n            = require('eleventy-plugin-i18n'),
      pluginRss       = require("@11ty/eleventy-plugin-rss"),
      translations    = require('./design/modules/translations.js'),
      typo            = require('./design/modules/typo.js'),
      yaml            = require("js-yaml"),
      { Transform }   = require('readable-stream'),
      {shuffle, uniq} = require('./design/modules/_tools.js'),
      readingTime     = require('./design/modules/reading_time.js'),
      videoShortcode  = require('./design/modules/video.js'),
      imageShortcode  = require('./design/modules/image.js'),
      markdownLibrary = require('./design/modules/markdownLibrary.js'),
      minHTML         = require('./design/modules/minHTML.js'),
      minJS           = require('./design/modules/minJS.js')


module.exports = config => {

  config.addFilter("markdown", content => markdownLibrary.render(content) );
  config.addFilter("typo", (content, locale) => typo(content, locale) );
  config.setLibrary("md", markdownLibrary);
  config.addFilter("debug", (content) => console.log(content));
  config.addFilter('shuffle', shuffle)
  config.addFilter('dateCompare', (date, lang) => dateCompare(date, lang));
  config.addFilter('isArr', something => Array.isArray(something));
  config.addFilter('isObj', something => _.isPlainObject(something))
  config.addFilter('uniq', uniq)
  config.addFilter('time', time)
  config.addFilter('isInThePast', dateIsPast)
  config.addFilter("head", (array, n, m) => { if (m) { return array.slice(n, m) } else {return array.slice(n); } });
  config.addFilter("escaP", content => content.replace('<p>', '').replace('</p>', ''));
  config.addFilter("split", (content, character) => { return content.split(character) });
  config.addFilter("findId", content => { return content.match(/\d{3}-[A-Z]/g)[0] });
  config.addFilter("findSerial", content => { return content.match(/\d{3}/g)[0] });
  
  config.addShortcode('date', (date, lang) => getDate(date, lang));
  config.addShortcode('dateCompare', (date, lang) => dateCompare(date, lang));
  config.addShortcode('readingTime', (content, lang) => readingTime(content, lang));

  config.addDataExtension("yaml", contents => yaml.load(contents));
  
  config.addNunjucksAsyncShortcode("image", imageShortcode);
  config.addNunjucksAsyncShortcode("video", videoShortcode);


  //
  // Collections
  // 
  config.addCollection("publications_fr", function(collection) {
      const now = new Date();
      const publishedPosts = (post) => !post.data.draft; // [1]
      let publications = collection.getFilteredByGlob("contenu/publication/**/fr.md").filter(publishedPosts);
      return publications;
  });

  config.addCollection("events_fr", function(collection) {
      let events = collection.getFilteredByGlob("contenu/event/**/fr.md")
      return events;
  });

  config.addCollection("publications_en", function(collection) {
      const now = new Date();
      const publishedPosts = (post) => !post.data.draft; // [1]
      let publications = collection.getFilteredByGlob("contenu/publication/**/en.md").filter(publishedPosts);
      return publications;
  });

  config.addCollection("events_en", function(collection) {
      let events = collection.getFilteredByGlob("contenu/event/**/en.md")
      return events;
  });


  // 
  // i18n
  // 
  config.addPlugin(i18n, {
    translations,
    fallbackLocales: {
      '*': 'fr'
    }
  });

  config.addPlugin(EleventyI18nPlugin , {
    defaultLanguage: "fr", 
    filters: {
      url: "locale_url",
      links: "locale_links",
    },
    errorMode: "never",
  });


  // 
  //rss
  // 
  config.addPlugin(pluginRss);


  // 
  // Sass Watch
  // 
  config.addWatchTarget("./design/assets/scss/");
  

  // 
  // pass through !
  // 
  config.addPassthroughCopy({'./design/assets/font/' : 'assets/font'});
  config.addPassthroughCopy({'./design/assets/svg/' : 'assets/svg'});
  config.addPassthroughCopy({'./design/assets/img/' : 'assets/img'});
  config.addPassthroughCopy({'./design/assets/lib/' : 'assets/lib'});
  config.addPassthroughCopy({'./design/assets/css/' : 'assets/css'});
  config.addPassthroughCopy({'./design/assets/social/' : 'assets/social/'});
  config.addPassthroughCopy({'./contenu/_media/' : 'assets/media/'});
  config.addPassthroughCopy({'./design/assets/root/' : '/'});
  config.addPassthroughCopy({'./design/assets/lib/' : 'assets/lib'});
  

  // 
  // Passthrough & minify JS
  // 
  if(process.env.NODE_ENV == 'development'){
  config.addPassthroughCopy({'./design/assets/js/' : 'assets/js'});
  } else {
      config.addPassthroughCopy(
      {'./design/assets/js/' : 'assets/js'}, 
      {
        transform: (src, dest, stats) => {
          return new Transform({
            transform(chunk, enc, done) {
              done(null, minJS(chunk.toString()));
            },
          });
        },
      }
    );
  }


  // 
  // Minify HTML
  // 
  config.addTransform("htmlmin", minHTML);
  

  return {
    markdownTemplateEngine: 'njk',
    dataTemplateEngine: 'njk',
    htmlTemplateEngine: 'njk',
    dir: {
      input: './contenu/',
      data: './_data/',
      includes: '../design/layouts',
      output: 'public'
    }

  };
};

