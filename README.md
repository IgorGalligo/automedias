# Automedias

Le site est un site statique généré par eleventy.

Les fichiers de travail du site sont synchronisés sur [un Gitlab](https://gitlab.com/IgorGalligo/automedias)
Le projet est hébergé directement sur Gitlab (cette solution gratuite et simple convient tant que le site reste de taille modeste)

L’action d’éditer le site repose sur un triptyque : Édition d’un fichier texte → Génération du site → Stockage des fichiers générés

En clair, une modification dans les fichiers textes induit la régénération des fichiers du site puis leurs transfert (automatique) vers le serveur hébergeant le site.

Un tutoriel complet est disponible [à cette adresse](https://gitlab.com/IgorGalligo/automedias/-/wikis/pages) 

***

Crédits

Automédias est designé et développé par [BenjmnG](benjmng.eu)

+ Dessin de couverture par [Lucile Kessil](https://www.instagram.com/lucilekessil_painting/)
+ Une partie de [Page To Print](https://github.com/esadpyrenees/PageTypeToPrint) pour la rédaction du tutoriel
+ [Les règles typographique éditée](https://gitlab.com/JulieBlanc/typesetting-tools/-/blob/master/regex-typo.js) par Julie Blanc
+ Paged.js pour l’impression et la génération de PDF © Adam Hyde, Julie Blanc, Fred Chasen & Julien Taquet + Nicolas Taffin !
+ La typographie [Epilogue](https://fonts.google.com/specimen/Epilogue) par Tyler Finck, ETC
